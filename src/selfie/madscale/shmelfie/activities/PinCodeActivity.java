package selfie.madscale.shmelfie.activities;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.utils.PreferenceKey;
import selfie.madscale.shmelfie.utils.PreferenceUtil;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

//import com.congratulations_gr.R;
//import com.congratulations_gr.broadcasts.StartingAlarmManagerBroadcast;
//import com.congratulations_gr.utils.PreferenceUtil;
//import com.congratulations_gr.values.PreferenceKey;

public class PinCodeActivity extends ActivityWithCheckData {

	/** Dialog identifier, {@code IDD_ENTER_PIN=1}. */
	private static final int IDD_ENTER_PIN = 1;
	// Dialog ids
	/** Dialog identifier, {@code IDD_PROTECTION=0}. */
	private static final int IDD_PROTECTION = 0;
	/** Dialog identifier */
	private static final int IDD_SETUP_PROGRESS = 2;
	private static final int IDD_WARNING = 3;
	protected static final String TAG = PinCodeActivity.class.getSimpleName();
	private SharedPreferences mSharedPreferences;
	private final Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			super.handleMessage(msg);
			switch (msg.arg1) {
			case 1:
				showDialog(IDD_SETUP_PROGRESS);
				break;
			case 2:
				dismissDialog(IDD_SETUP_PROGRESS);
				break;
//			case 3:
//				dismissDialog(IDD_WARNING);
//				break;
			}
		}
	};

	private AlertDialog askPinDialog() {

		AlertDialog.Builder builder = null;
		final Context context = this;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layoutDoAsk = inflater.inflate(
				R.layout.dlg_first_activity_ask_pin,
				(ViewGroup) findViewById(R.id.rlDlgFirstActivityAskPin_root));
		final CheckBox doNotAskAgain = (CheckBox) layoutDoAsk
				.findViewById(R.id.chbDlgFirstActivityAskPin_doNotAskAgain);
		builder = new AlertDialog.Builder(context);
		builder.setView(layoutDoAsk);
		builder.setInverseBackgroundForced(true);
		//builder.setMessage(R.string.ttlDlgFirstActivityAskPin_title)
		builder.setCancelable(false);
		Button btnAskYes = (Button) layoutDoAsk
				.findViewById(R.id.btnDlgFirstActivityAskPin_yes);
		btnAskYes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				PreferenceUtil.putBoolean(getBaseContext(),
						PreferenceKey.DO_ASK_PIN, true);
				PreferenceUtil.putBoolean(getBaseContext(),
						PreferenceKey.DO_NOT_SHOW_DIALOG_AGAIN,
						doNotAskAgain.isChecked());
				dismissDialog(IDD_PROTECTION);
				showDialog(IDD_ENTER_PIN);
			}
		});
		Button btnAskNo = (Button) layoutDoAsk
				.findViewById(R.id.btnDlgFirstActivityAskPin_no);
		btnAskNo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				PreferenceUtil.putBoolean(getBaseContext(),
						PreferenceKey.DO_ASK_PIN, false);
				PreferenceUtil.putBoolean(getBaseContext(),
						PreferenceKey.DO_NOT_SHOW_DIALOG_AGAIN,
						doNotAskAgain.isChecked());
				openList();
				dismissDialog(IDD_PROTECTION);
			}
		});
		return builder.create();
	}

	private AlertDialog enterPinDialog() {

		AlertDialog.Builder builder = null;
		final Context context = this;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dlg_first_activity_enter_pin,
				(ViewGroup) findViewById(R.id.rlDlgFirstActivityEnterPin_root));
		final EditText etPin = (EditText) layout
				.findViewById(R.id.etDlgFirstActivityEnterPin_pin);
		final TextView tvMessage = (TextView) layout
				.findViewById(R.id.tvDlgFirstActivityEnterPin_message);
		builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		// builder.setTitle(R.string.ttlDlgFirstActivityEnterPin_title);
		Button btnEnterOk = (Button) layout
				.findViewById(R.id.btnDlgFirstActivityEnterPin_ok);
		btnEnterOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String enteredPin = etPin.getText().toString();
				String savedPin = mSharedPreferences.getString(
						PreferenceKey.PIN_CODE, null);
				if (enteredPin.length() == 4) {
					if ((savedPin == null)
							|| savedPin
									.equals(getString(R.string.cbPreferencesActivity_removePinKey))) {
						PreferenceUtil.putString(context,
								PreferenceKey.PIN_CODE, enteredPin);
						PreferenceUtil.putBoolean(context,
								PreferenceKey.DO_NOT_SHOW_DIALOG_AGAIN, true);
						dismissDialog(IDD_ENTER_PIN);
						openList();
					} else {
						if (enteredPin.equals(savedPin)) {
							tvMessage.setText("");
							dismissDialog(IDD_ENTER_PIN);
							openList();
						} else {
							tvMessage
									.setText(R.string.errDlgFirstActivityEnterPin_IncorrectPin);
						}
					}
				} else {
					tvMessage
							.setText(R.string.errDlgFirstActivityEnterPin_IncorrectLength);
				}
			}
		});
		Button btnEnterCancel = (Button) layout
				.findViewById(R.id.btnDlgFirstActivityEnterPin_cancel);
		btnEnterCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dismissDialog(IDD_ENTER_PIN);
				finish();
			}
		});
		builder.setView(layout);
		return builder.create();
	}
	
	private AlertDialog warningDialog() {

		AlertDialog.Builder builder = null;
		final Context context = this;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layoutDoAsk = inflater.inflate(
				R.layout.dlg_first_activity_warning,
				(ViewGroup) findViewById(R.id.rlDlgFirstActivityWarning_root));
		final CheckBox doNotAskWarningAgain = (CheckBox) layoutDoAsk
				.findViewById(R.id.chbDlgFirstActivityWarning_doNotAskAgain);
		builder = new AlertDialog.Builder(context);
		builder.setView(layoutDoAsk);
		//builder.setMessage("Attention!")
		builder.setCancelable(false);
		builder.setInverseBackgroundForced(true);
		Button btnAskYes = (Button) layoutDoAsk
				.findViewById(R.id.btnDlgFirstActivityWarning_ok);
		btnAskYes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				PreferenceUtil.putBoolean(getBaseContext(),
						PreferenceKey.DO_NOT_SHOW_WARNING_AGAIN,
						doNotAskWarningAgain.isChecked());
				dismissDialog(IDD_WARNING);
				
				Intent intent = new Intent();
				intent.setClass(getBaseContext(), MainActivity.class);
				startActivity(intent);
				finish();
			}
		});
		
		return builder.create();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		//BugSenseHandler.closeSession(PinCodeActivity.this);
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		//BugSenseHandler.initAndStartSession(PinCodeActivity.this, "d5a0b647");
		setContentView(R.layout.ac_first_activity);
		removePin();
		mSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		
		if (!mSharedPreferences.getBoolean(
				PreferenceKey.DO_NOT_SHOW_DIALOG_AGAIN, false)) {
			showDialog(IDD_PROTECTION);
		} else if (mSharedPreferences
				.getBoolean(PreferenceKey.DO_ASK_PIN, true)) {
			showDialog(IDD_ENTER_PIN);
		} else {
			openList();
		}
		// Intent newIntent = new Intent(this,
		// StartingAlarmManagerBroadcast.class);
		// sendBroadcast(newIntent);
		// StartingAlarmManagerBroadcast.setHandler(handler);
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		AlertDialog alertDialog = null;
		switch (id) {
		case IDD_PROTECTION:
			alertDialog = askPinDialog();
			break;
		case IDD_ENTER_PIN:
			alertDialog = enterPinDialog();
			break;
		case IDD_SETUP_PROGRESS:
			ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setTitle(R.string.dlgProgressDialog_title);
			progressDialog
					.setMessage(getString(R.string.dlgProgressDialog_message));
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(false);
			alertDialog = progressDialog;
			break;
		case IDD_WARNING:
			alertDialog = warningDialog();
			break;
		default:
			alertDialog = null;
		}
		return alertDialog;
	}

	@Override
	public void onStop() {

		super.onStop();
	}

	/**
	 * The function invoke for start new activity with SMS
	 */
	private void openList() {
		if(PreferenceUtil.getBoolean(getBaseContext(),PreferenceKey.DO_NOT_SHOW_WARNING_AGAIN, false)){
			Intent intent = new Intent();
			intent.setClass(this, MainActivity.class);
			startActivity(intent);
			finish();
		}else{
		showDialog(IDD_WARNING);
		}
	}

	private void removePin() {

		if (PreferenceUtil.getBoolean(getBaseContext(),
				getString(R.string.cbPreferencesActivity_removePinKey), false)) {
			PreferenceUtil.putBoolean(getBaseContext(),
					PreferenceKey.DO_NOT_SHOW_DIALOG_AGAIN, false);
			PreferenceUtil.putBoolean(getBaseContext(),
					PreferenceKey.DO_ASK_PIN, true);
			PreferenceUtil.putString(getBaseContext(), PreferenceKey.PIN_CODE,
					getString(R.string.cbPreferencesActivity_removePinKey));
			PreferenceUtil.putBoolean(getBaseContext(),
					getString(R.string.cbPreferencesActivity_removePinKey),
					false);
		}
	}
}