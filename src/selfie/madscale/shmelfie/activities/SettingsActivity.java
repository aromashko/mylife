package selfie.madscale.shmelfie.activities;


import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.AdapterView;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.Spinner;
import org.holoeverywhere.widget.TimePicker;

import selfie.madscale.shmelfie.BuildConfig;
import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.database.DataManager;
import selfie.madscale.shmelfie.utils.DBBackup;
import selfie.madscale.shmelfie.utils.FileDialog;
import selfie.madscale.shmelfie.utils.PhotoExporter;
import selfie.madscale.shmelfie.utils.PreferenceUtil;
import selfie.madscale.shmelfie.utils.Supporting;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsense.trace.BugSenseHandler;


/**
 * Sloboda Studio 2013 User: oZBo Date: 14.03.13
 */
public class SettingsActivity extends Activity {

    private final Integer REQUEST_SAVE = 101;

    public static final String LOG_SETTINGS = "settingsLogs";
    private RadioGroup whenMakePhoto, whichCamera;
    private TimePicker tpFrom, tpTo;
    //private Spinner autoPerDay;
    private Spinner anglePhoto;
    private Spinner slideShowFrequency;
    private Spinner slideShowOrder;
    private Spinner dateDisplay;
    private Spinner fileName;
    private CheckBox checkBoxInterval, checkBoxgeotagging, checkBoxLightsensor,
            checkBoxPincode, checkBoxShutterClick, checkBoxLoopPic, checkBoxLoopMusic,
            checkBoxShowDate;
    public static SharedPreferences settings;
    private LinearLayout llForLightSensor;
    private RelativeLayout llForInterval;
    private View separatorLightSensor;
    private TextView lightSensorInfo;
    private Button btnMusicMaintenance, btnImportDB, btnExport;
    BackUpTask backUpTask;
    ProgressDialog pd;
    Handler h;
    public static final int HANDLER_STOP=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BugSenseHandler.initAndStartSession(SettingsActivity.this, "d5a0b647");
        setContentView(R.layout.settings);

        Log.d(LOG_SETTINGS, "-------------------------");
        settings = getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE);
        findIDforViews();
        spinnerInit();
        radioGroupInit();
        checkBoxesInit();
        timeInit();
        buttonInit();

    }

    @Override
    protected void onDestroy() {
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
        BugSenseHandler.closeSession(SettingsActivity.this);
        super.onDestroy();
    }


    private void findIDforViews() {
        // =======================spinners============================
        slideShowFrequency = (Spinner) findViewById(R.id.spinner_slideshow_frequency);
        slideShowOrder = (Spinner) findViewById(R.id.spinner_slideshow_order);
        dateDisplay = (Spinner) findViewById(R.id.spinner_display_date);
        anglePhoto = (Spinner) findViewById(R.id.spinner_camera_angle);
        // =======================check boxes=========================
        checkBoxInterval = (CheckBox) findViewById(R.id.checkBox_interval);
        checkBoxgeotagging = (CheckBox) findViewById(R.id.checkBox_geotagging);
        checkBoxLightsensor = (CheckBox) findViewById(R.id.checkBox_lightsensor);
        checkBoxPincode = (CheckBox) findViewById(R.id.checkBox_pincode);
        checkBoxShutterClick = (CheckBox) findViewById(R.id.checkBox_shutterClick);
        checkBoxLoopMusic = (CheckBox) findViewById(R.id.loopin_mis_chek);
        checkBoxLoopPic = (CheckBox) findViewById(R.id.loopin_pic_chek);
        checkBoxShowDate = (CheckBox) findViewById(R.id.chBox_showDate);
        // =======================radio buttons=======================
        whichCamera = (RadioGroup) findViewById(R.id.radio_which_camera);
        whenMakePhoto = (RadioGroup) findViewById(R.id.radio_gruop_how);
        // =======================radio buttons=======================
        tpFrom = (TimePicker) findViewById(R.id.from);
        tpTo = (TimePicker) findViewById(R.id.to);
        // =======================LinearLayouts=======================
        llForInterval = (RelativeLayout) findViewById(R.id.lltime);
        llForLightSensor = (LinearLayout) findViewById(R.id.lightSensorLL);
        // =======================Buttons=============================
        btnMusicMaintenance = (Button) findViewById(R.id.music_maintenance);
        btnExport = (Button) findViewById(R.id.button_export);
        //btnImport = (Button) findViewById(R.id.button_import);
        btnImportDB = (Button) findViewById(R.id.button_importdb);
        // =======================Separators==========================
        separatorLightSensor = (View) findViewById(R.id.separatorLightSensor);
        // =======================TextViews===========================
        lightSensorInfo = (TextView) findViewById(R.id.txtLightSensorInfo);
        
    }

    public synchronized void onActivityResult(final int requestCode,
                                              int resultCode, final Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_SAVE) {
                VAR.PATH = data.getStringExtra(FileDialog.RESULT_PATH);
                String name = VAR.PATH.substring(VAR.PATH.lastIndexOf("/") + 1,
                        VAR.PATH.length());
                saveSharedPreferences("path", VAR.PATH);
                saveSharedPreferences("name", name);
                Log.d(LOG_SETTINGS, "onActivityResult path = " + VAR.PATH);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            if (BuildConfig.DEBUG) {
            }
        }
    }

    private void buttonInit() {
        Log.d(LOG_SETTINGS, "onCreate path = " + VAR.PATH);

        btnImportDB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            	createDialogBackUp();
            }
        });

        /*btnImport.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View _v) {
                PhotoImporter importer = new PhotoImporter(getApplicationContext());
                importer.setDataPath(VAR.PATH);
                if (importer.doImport()) {
                    Toast.makeText(getApplicationContext(), getString(R.string.db_success_import), 300).show();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.db_error_import), 300).show();
                }
                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(new Intent(VAR.ACTION_IMPORT_UPDATE));
            }
        });*/
        btnExport.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                PhotoExporter exporter = new PhotoExporter(DataManager.getPhotoList(getApplicationContext()),
                        DataManager.getBestPhotoList(getApplicationContext()));
                exporter.setDataPath(VAR.PATH);
                try {
                    exporter.doExport();
                    Toast.makeText(getApplicationContext(), "Data successfully exported", 300).show();
                    
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Export error", 300).show();
                }
            }
        });

        btnMusicMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent music_chooser = new Intent(getApplicationContext(),
                        ChooseMusicActivity.class);
                startActivity(music_chooser);
            }
        });

    }

    public void timeInit() {
        if (VAR.TEMP_checkBoxInterval) {
            showLayoutForInterval();
        } else {
            hideLayoutForInterval();
        }
        VAR.TEMP_fromTime = settings.getInt(VAR.FROM_TIME, VAR.FROM_TIME_DEFAULT_VALUE);
        VAR.TEMP_toTime = settings.getInt(VAR.TO_TIME, VAR.TO_TIME_DEFAULT_VALUE);
        tpFrom.setIs24HourView(true);
        tpFrom.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
        tpTo.setIs24HourView(true);
        tpTo.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
        tpFrom.setCurrentHour(VAR.TEMP_fromTime / 60);
        tpFrom.setCurrentMinute(VAR.TEMP_fromTime % 60);
        tpTo.setCurrentHour(VAR.TEMP_toTime / 60);
        tpTo.setCurrentMinute(VAR.TEMP_toTime % 60);
        Log.d(LOG_SETTINGS, "time from = " + VAR.TEMP_fromTime);
        Log.d(LOG_SETTINGS, "time to = " + VAR.TEMP_toTime);
        tpFrom.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {

            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

                VAR.TEMP_fromTime = hourOfDay * 60 + minute;
                saveSharedPreferences(VAR.FROM_TIME, VAR.TEMP_fromTime);
            }
        });
        tpTo.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {

            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                VAR.TEMP_toTime = hourOfDay * 60 + minute;
                saveSharedPreferences(VAR.TO_TIME, VAR.TEMP_toTime);

            }
        });
 
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void checkBoxesInit() {
        switch (settings.getInt(VAR.CHECK_BOX_INTERVAL, 1)) {
            case 0:
                checkBoxInterval.setChecked(false);
                VAR.TEMP_checkBoxInterval = false;
                hideLayoutForInterval();
                break;
            case 1:
                checkBoxInterval.setChecked(true);
                VAR.TEMP_checkBoxInterval = true;
                showLayoutForInterval();
                break;
        }

        if (settings.getBoolean(VAR.SHUTTER_CLICK, false)) {
            checkBoxShutterClick.setChecked(true);
        }

        checkBoxInterval.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    saveSharedPreferences(VAR.CHECK_BOX_INTERVAL, 1);
                    VAR.TEMP_checkBoxInterval = true;
                    showLayoutForInterval();
                } else {
                    saveSharedPreferences(VAR.CHECK_BOX_INTERVAL, 0);
                    VAR.TEMP_checkBoxInterval = false;
                    hideLayoutForInterval();
                }
            }
        });


        checkBoxShutterClick
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton,
                                                 boolean isChecked) {
                        if (isChecked) {
                            saveSharedPreferences(VAR.SHUTTER_CLICK, true);
                        } else {
                            saveSharedPreferences(VAR.SHUTTER_CLICK, false);
                        }
                    }
                });

        if (settings.getInt(VAR.CHECK_BOX_GEOTAG, 0) == 1) {
            checkBoxgeotagging.setChecked(true);
            VAR.TEMP_checkBoxGeotag = true;
        }
        Log.d(LOG_SETTINGS,
                "checkBoxGeotag = " + checkBoxgeotagging.isChecked());
        checkBoxgeotagging
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton,
                                                 boolean isChecked) {
                        if (isChecked) {
                            saveSharedPreferences(VAR.CHECK_BOX_GEOTAG, 1);
                            VAR.TEMP_checkBoxGeotag = true;
                        } else {
                            saveSharedPreferences(VAR.CHECK_BOX_GEOTAG, 0);
                            VAR.TEMP_checkBoxGeotag = false;
                        }
                    }
                });

        if(settings.getBoolean(VAR.CHECK_BOX_LOOP_MISIC, VAR.CHECK_BOX_LOOP_MISIC_DEFAULT_VALUE)){
        	checkBoxLoopMusic.setChecked(true);
        }
        
        checkBoxLoopMusic.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					saveSharedPreferences(VAR.CHECK_BOX_LOOP_MISIC, true);
				}else{
					saveSharedPreferences(VAR.CHECK_BOX_LOOP_MISIC, false);
				}
				
				
			}
		});
        
        if(settings.getBoolean(VAR.CHECK_BOX_LOOP_PIC, VAR.CHECK_BOX_LOOP_PICTURE_DEFAULT_VALUE)){
        	checkBoxLoopPic.setChecked(true);
        }
        
        checkBoxLoopPic.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					saveSharedPreferences(VAR.CHECK_BOX_LOOP_PIC, true);
				}else{
					saveSharedPreferences(VAR.CHECK_BOX_LOOP_PIC, false);
				}
				
				
			}
		});
        
        
        if (settings.getInt(VAR.CHECK_BOX_LIGHTSENSOR, 0) == 1) {
            checkBoxLightsensor.setChecked(true);

        }
        Log.d(LOG_SETTINGS,
                "checkBoxLightsensor = " + checkBoxLightsensor.isChecked());
        checkBoxLightsensor
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton,
                                                 boolean isChecked) {
                        if (Supporting.haveLightSensor(SettingsActivity.this)) {

                            if (isChecked) {

                                saveSharedPreferences(
                                        VAR.CHECK_BOX_LIGHTSENSOR, 1);
                            } else {
                                saveSharedPreferences(
                                        VAR.CHECK_BOX_LIGHTSENSOR, 0);
                            }
                        } else {
                            Supporting
                                    .showToast(SettingsActivity.this,
                                            getString(R.string.no_light_sensor));
                            checkBoxLightsensor.setChecked(false);
                        }
                    }
                });

        if (settings.getInt(VAR.CHECK_BOX_PINCODE, 0) == 1) {
            checkBoxPincode.setChecked(true);
            VAR.TEMP_checkBoxPinCode = true;
        }
        checkBoxPincode
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton,
                                                 boolean isChecked) {
                        if (isChecked) {
                            saveSharedPreferences(VAR.CHECK_BOX_PINCODE, 1);
                            VAR.TEMP_checkBoxPinCode = true;
                        } else {
                            saveSharedPreferences(VAR.CHECK_BOX_PINCODE, 0);
                            VAR.TEMP_checkBoxPinCode = true;
                        }
                    }
                });

        checkBoxPincode
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton,
                                                 boolean isChecked) {
                        if (isChecked) {
                            PreferenceUtil
                                    .putBoolean(
                                            getBaseContext(),
                                            getString(R.string.cbPreferencesActivity_removePinKey),
                                            true);
                        } else {
                            PreferenceUtil
                                    .putBoolean(
                                            getBaseContext(),
                                            getString(R.string.cbPreferencesActivity_removePinKey),
                                            false);
                        }
                    }
                });
        if (settings.getBoolean(VAR.SHOW_DATE, VAR.SHOW_DATE_DEFAULT_VALUE)) {
        	checkBoxShowDate.setChecked(true);
        }
        checkBoxShowDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
					  saveSharedPreferences( VAR.SHOW_DATE, isChecked);
			}
        	
        });
    }

    private void radioGroupInit() {

        int whichCameraCheck = settings.getInt(VAR.WHICH_CAMERA, Supporting.getFrontCamera() ? VAR.CAMERA_FRONT : VAR.CAMERA_BACK);

        switch (whichCameraCheck) {
            case VAR.CAMERA_FRONT:
                whichCamera.check(R.id.radio_camera_front);
                break;
            case VAR.CAMERA_BACK:
                whichCamera.check(R.id.radio_camera_back);
                break;
        }

        whichCamera
                .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup,
                                                 int checkedId) {
                        switch (checkedId) {
                            case R.id.radio_camera_front:
                                if (Supporting.getFrontCamera()) {
                                    saveSharedPreferences(VAR.WHICH_CAMERA, VAR.CAMERA_FRONT);
                                } else {
                                    saveSharedPreferences(VAR.WHICH_CAMERA, VAR.CAMERA_BACK);
                                    whichCamera.check(R.id.radio_camera_back);
                                    Supporting
                                            .showToast(
                                                    SettingsActivity.this,
                                                    getString(R.string.warnings_noFrontCamera));
                                }
                                break;
                            case R.id.radio_camera_back:
                                saveSharedPreferences(VAR.WHICH_CAMERA, VAR.CAMERA_BACK);
                                break;
                        }
                    }
                });

        int whenMakePhotoCheck = settings.getInt(VAR.WHEN_MAKE_PHOTO, VAR.WHEN_MAKE_PHOTO_DEFAULT_VALUE);
        Log.d(LOG_SETTINGS, "When make photo= " +whenMakePhotoCheck);
        switch (whenMakePhotoCheck) {
            case 0:
                whenMakePhoto.check(R.id.radio_unlock);
//                hideLayoutForLightSensor();
                VAR.whenMakePhoto = 0;
                break;
            case 1:
//                showLayoutForLightSensor();
                VAR.whenMakePhoto = 1;
                break;
            case 2:
            	whenMakePhoto.check(R.id.radio_unlock_random);
//              showLayoutForLightSensor();
            	VAR.whenMakePhoto = 2;
        }
        showLayoutForLightSensor();

        whenMakePhoto
                .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup,
                                                 int checkedId) {
                        switch (checkedId) {
                            case R.id.radio_unlock:
                                saveSharedPreferences(VAR.WHEN_MAKE_PHOTO, 0);
//                                hideLayoutForLightSensor();
                                VAR.whenMakePhoto = 0;
                                break;
                            case R.id.radio_unlock_random:
                            	saveSharedPreferences(VAR.WHEN_MAKE_PHOTO, 2);
//                              hideLayoutForLightSensor();
                            		VAR.whenMakePhoto = 2;
                                	break;
                        }
                    }
                });
    }

    private void spinnerInit() {
        //Spinner angle
        ArrayAdapter<String> adapterAngle = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getResources()
                .getStringArray(R.array.settings_photo_angle_spinner));
        adapterAngle
                .setDropDownViewResource(R.drawable.spinner_dropdown_item);
        anglePhoto.setAdapter(adapterAngle);
        anglePhoto.setSelection(settings.getInt(VAR.ANGLE_CAMERA, VAR.ANGLE_DEFAULT_VALUE));
        VAR.TEMP_angleCamera = VAR.angleCamera[settings.getInt(VAR.ANGLE_CAMERA,
        		VAR.ANGLE_DEFAULT_VALUE)];
        Log.d(LOG_SETTINGS, "angleCamera = " + VAR.TEMP_angleCamera);
        anglePhoto
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        saveSharedPreferences(VAR.ANGLE_CAMERA, position);
                        VAR.TEMP_angleCamera = VAR.angleCamera[position];
                    }

                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });

        // Spinner for slideshow frequency
        ArrayAdapter<String> adapterSlideshowFrequency = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, getResources()
                .getStringArray(R.array.settings_frequency_spinner));
        adapterSlideshowFrequency
                .setDropDownViewResource(R.drawable.spinner_dropdown_item);
        slideShowFrequency.setAdapter(adapterSlideshowFrequency);
        slideShowFrequency.setSelection(settings.getInt(
                VAR.SLIDE_SHOW_FREQUENCY, 1));
        VAR.TEMP_slideShowFrequency = VAR.slideShowFrequency[settings.getInt(
                VAR.SLIDE_SHOW_FREQUENCY, 1)];
        Log.d(LOG_SETTINGS, "slideShowFrequency = "
                + VAR.TEMP_slideShowFrequency);
        slideShowFrequency
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        saveSharedPreferences(VAR.SLIDE_SHOW_FREQUENCY,
                                position);
                        VAR.TEMP_slideShowFrequency = VAR.slideShowFrequency[position];
                    }

                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });

        // Spinner for slideshow order
        ArrayAdapter<String> adapterSlideshowOrder = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, getResources()
                .getStringArray(R.array.settings_order_spinner));
        adapterSlideshowOrder
                .setDropDownViewResource(R.drawable.spinner_dropdown_item);
        slideShowOrder.setAdapter(adapterSlideshowOrder);
        slideShowOrder.setSelection(settings.getInt(VAR.SLIDE_SHOW_ORDER, VAR.SLIDE_SHOW_ORDER_DEFAULT_VALUE));
        VAR.TEMP_slideShowOrder = VAR.slideShowOrder[settings.getInt(
                VAR.SLIDE_SHOW_ORDER, VAR.SLIDE_SHOW_ORDER_DEFAULT_VALUE)];
        Log.d(LOG_SETTINGS, "slideShowOrder = " + VAR.TEMP_slideShowOrder);
        slideShowOrder
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        saveSharedPreferences(VAR.SLIDE_SHOW_ORDER, position);
                        VAR.TEMP_slideShowOrder = VAR.slideShowOrder[position];
                    }

                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });

        // Spinner for date display
        ArrayAdapter<String> adapterForDateDisplay = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, getResources()
                .getStringArray(R.array.settings_date_type));
        adapterForDateDisplay
                .setDropDownViewResource(R.drawable.spinner_dropdown_item);
        dateDisplay.setAdapter(adapterForDateDisplay);
        dateDisplay.setSelection(settings.getInt(VAR.DATE_DISPLAY, 0));
        VAR.TEMP_dateDisplay = VAR.dateDisplay[settings.getInt(
                VAR.DATE_DISPLAY, 0)];
        Log.d(LOG_SETTINGS, "dateDisplay = " + VAR.TEMP_dateDisplay);
        dateDisplay
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        saveSharedPreferences(VAR.DATE_DISPLAY, position);
                        VAR.TEMP_dateDisplay = VAR.dateDisplay[position];
                    }

                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });
    }

    private void hideLayoutForInterval() {
    	llForInterval.setVisibility(TimePicker.GONE);
    }

    private void showLayoutForInterval() {
    	llForInterval.setVisibility(TimePicker.VISIBLE);
    }

    private void hideLayoutForLightSensor() {
        llForLightSensor.setVisibility(LinearLayout.GONE);
        lightSensorInfo.setVisibility(TextView.GONE);
        separatorLightSensor.setVisibility(View.GONE);
    }

    private void showLayoutForLightSensor() {
        if (settings.getInt(VAR.CHECK_BOX_LIGHTSENSOR, 0) == 1) {
            checkBoxLightsensor.setChecked(true);
        } else {
            checkBoxLightsensor.setChecked(false);
        }
        llForLightSensor.setVisibility(LinearLayout.VISIBLE);
        lightSensorInfo.setVisibility(TextView.VISIBLE);
        separatorLightSensor.setVisibility(View.VISIBLE);
    }

    public void saveSharedPreferences(String key, Integer value) {

        SharedPreferences settings = getSharedPreferences(VAR.SETTINGS,
                MODE_PRIVATE);
        SharedPreferences.Editor ed = settings.edit();
        ed.putInt(key, value);
        ed.commit();
    }

    public void saveSharedPreferences(String key, Boolean value) {

        SharedPreferences settings = getSharedPreferences(VAR.SETTINGS,
                MODE_PRIVATE);
        SharedPreferences.Editor ed = settings.edit();
        ed.putBoolean(key, value);
        ed.commit();
    }

    public void saveSharedPreferences(String key, String value) {
        SharedPreferences settings = getSharedPreferences(VAR.SETTINGS,
                MODE_PRIVATE);
        SharedPreferences.Editor ed = settings.edit();
        ed.putString(key, value);
        ed.commit();
    }

    public void saveSharedPreferences(String key, Float value) {

        SharedPreferences settings = getSharedPreferences(VAR.SETTINGS,
                MODE_PRIVATE);
        SharedPreferences.Editor ed = settings.edit();
        ed.putFloat(key, value);
        ed.commit();
    }
    
    public void createDialogBackUp(){
    	if (DBBackup.getBackupCount() > 0) {
    		
    		LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
    		View layoutDoAsk = inflater.inflate(
    				R.layout.dlg_backup,
    				(ViewGroup) findViewById(R.id.rl_dlgBackUp_root));
            AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
            //builder.setMessage(R.string.apply_backup);
            builder.setView(layoutDoAsk);
            builder.setInverseBackgroundForced(true);
            Button btnAskYes = (Button) layoutDoAsk
    				.findViewById(R.id.btnDlgBackUp_yes);
            Button btnAskNo = (Button) layoutDoAsk
    				.findViewById(R.id.btnDlgBackUp_no);
            final AlertDialog dialog = builder.create();;
            btnAskYes.setOnClickListener(new OnClickListener() {

    			@Override
    			public void onClick(View v) {
    				dialog.dismiss();
                	 restoreFromBackUp();
                }
            });
            btnAskNo.setOnClickListener(new OnClickListener() {

    			@Override
    			public void onClick(View v) {
    				dialog.dismiss();
                	saveSharedPreferences(VAR.BACK_UP_ENDED, true);
                }
            });
            //dialog = builder.create();
            dialog.show();
    }
    }
    
    public void restoreFromBackUp(){
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    	pd = new ProgressDialog(SettingsActivity.this);
        pd.setTitle(getString(R.string.backUp));
        pd.setMessage(getString(R.string.please_wait));
        
        // ������ ����� �� ���������
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // �������� �������� ��������
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.show();
   	 	backUpTask = new BackUpTask();
   	 	backUpTask.execute();
    }
    
class BackUpTask extends AsyncTask<Void, Void, Void> {
        
        @Override
        protected void onPreExecute() {
          super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void...values) {
        	int backUpCount = DBBackup.getBackupCountAll(SettingsActivity.this);
        	
        	 // ������������� ��������
            pd.setMax(backUpCount);
            pd.setIndeterminate(false);
            publishProgress();
        	DBBackup.applyBackup(getApplicationContext());
        	
        	 
          return null;
        }


        @Override
        protected void onProgressUpdate(Void... values) {
          super.onProgressUpdate(values);
          h = new Handler() {
        		 Boolean stopHandler = false;
        	        public void handleMessage(Message msg) {
        	        	 switch (msg.what) {
        	        	 case HANDLER_STOP:
        	        		 stopHandler = true;
        	        		 break;
        	        	 case 0:
        	        	if (pd.getProgress() < pd.getMax()) {
        	                pd.setProgress(DBBackup.countRestoreBackUp);
        	                if (!stopHandler){
        	                h.sendEmptyMessageDelayed(0, 1500);
        	                }
        	              } else {
        	                pd.dismiss();
        	              }
        	        	break;
        	        	 }
        	        }
        	      };
        	      h.sendEmptyMessageDelayed(0, 1500);
        }

        @Override
        protected void onPostExecute(Void result) {
          super.onPostExecute(result);
          Log.e("AsyncTak","End");
          saveSharedPreferences(VAR.BACK_UP_ENDED, true);
          getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
          Log.e("Count Restore All", DBBackup.countRestoreBackUp+"");
          LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(new Intent(VAR.ACTION_IMPORT_UPDATE));
         /* loadDataToLists();
          openMonthAdapter();
          Collections.sort(listOfPhoto);*/
        }
      }

}
