package selfie.madscale.shmelfie.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import com.bugsense.trace.BugSenseHandler;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.models.PhotoObject;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 25.04.13
 */
public class SlideShowActivity extends ActivityWithCheckData {

    static Timer timer, timerMusic;
    TimerTask task, taskmusic;
    Handler handlerMusic;
    ImageView slideImage;
    private float period;
    public static Handler handlerSlideShow;
    private static Bitmap bitmap;
    private static int countOfImages;
    private static ArrayList<Integer> arrayOfRandomNumbers = new ArrayList<Integer>();
    public static final String LOG_TAG = "SlideShow";
    MediaPlayer mMediaPlayer;
    public static SharedPreferences settings;
    Runnable runnable;
    public static final String FULL_LIST_OF_PHOTO = "fullListOfPhoto";
    ArrayList<PhotoObject> listOfPhoto;
    TextView tv_date;
    Boolean showDate;

    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	BugSenseHandler.closeSession(SlideShowActivity.this);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BugSenseHandler.initAndStartSession(SlideShowActivity.this, "d5a0b647");
        setContentView(R.layout.slide_show);
        listOfPhoto = new ArrayList<PhotoObject>();
        Intent intent = getIntent();
        Boolean fullListOfPhoto = intent.getBooleanExtra(FULL_LIST_OF_PHOTO, true);
        if (fullListOfPhoto){
        	listOfPhoto.addAll(MainActivity.listOfPhoto);
        }
        else{
        	listOfPhoto.addAll(MainActivity.bestListOfPhoto);
        }
        mMediaPlayer = new MediaPlayer();
        settings = getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE);
        showDate = settings.getBoolean(VAR.SHOW_DATE, VAR.SHOW_DATE_DEFAULT_VALUE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        tv_date = (TextView) findViewById(R.id.tv_date);
        slideImage = (ImageView) findViewById(R.id.slideShowImage);
        slideImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                task.cancel();
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.reset();
                }
            }
        });
        period = VAR.TEMP_slideShowFrequency * 1000;
        timer = new Timer();
        timerMusic= new Timer();
        initOrderToShow();
        handlerInit();
        timerInit();
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	 task.cancel();
         if (mMediaPlayer.isPlaying()) {
             mMediaPlayer.reset();
         }
    }
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            task.cancel();
            timer.cancel();
            timer = null;
            if (handlerMusic != null){
            handlerMusic.removeCallbacks(runnable);}
            finish();
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.reset();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void handlerInit() {
        handlerSlideShow = new android.os.Handler() {
            @Override
            public void handleMessage(Message msg) {
                changeImage();
            }
        };
    }

    private void timerInit() {
        task = new TimerTask() {
            @Override
            public void run() {
                handlerSlideShow.sendEmptyMessage(0);
            }
        };
        taskmusic = new TimerTask() {
            @Override
            public void run() {
            	handlerMusic.sendEmptyMessage(0);
            }
        };
        timer.schedule(task, 1000, (int) period);
        String path_to_music = settings.getString(VAR.MUSIC_PATH, "default");

    		try {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.reset();
                }
                if(!path_to_music.equals("default")){
                mMediaPlayer.setDataSource(path_to_music);
                }
                else{
                	try {
            			AssetFileDescriptor afd = getAssets().openFd("Cloud_Patterns.mp3");
            			mMediaPlayer.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
            			
            		} catch (IOException e1) {
            			e1.printStackTrace();
            		}
                }
                if(settings.getBoolean(VAR.CHECK_BOX_LOOP_MISIC, VAR.CHECK_BOX_LOOP_MISIC_DEFAULT_VALUE)){
                	
                	 if(path_to_music.equals("default")){
                	startLoopingMusic();
                	 }
                	 else{
                		 mMediaPlayer.setLooping(true);
                		 mMediaPlayer.prepare();
                         mMediaPlayer.start();
                	 }	  

                }else{
                	mMediaPlayer.setLooping(false);
                	 mMediaPlayer.prepare();
                     mMediaPlayer.start();
                }
                
              
               
            } catch (Exception e) {
            	e.printStackTrace();
            	Toast.makeText(SlideShowActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }
    		
    	
    }
    
    public void startLoopingMusic(){
    	if (mMediaPlayer.isPlaying()) {
            mMediaPlayer = new MediaPlayer();
            try {
    			AssetFileDescriptor afd = getAssets().openFd("Cloud_Patterns.mp3");
    			mMediaPlayer.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
    			
    		} catch (IOException e1) {
    			e1.printStackTrace();
    		}
        }
		try {
			mMediaPlayer.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  mMediaPlayer.start();
         int duration = mMediaPlayer.getDuration();
         Log.e("Duration", duration+"");
        
         handlerMusic = new Handler();
         handlerMusic.postDelayed(runnable = new Runnable(){
         	  public void run(){
         		 startLoopingMusic();
         	  }
         	  }, duration-100);
    }

    private void changeImage() {
    	//if (countOfImages < 0) {countOfImages = listOfPhoto.size() - 1;}
    	//if (countOfImages > listOfPhoto.size() - 1) {countOfImages=0;}
    	
        switch (VAR.TEMP_slideShowOrder) {
            case 0: //forward
            	
            	
            	if(settings.getBoolean(VAR.CHECK_BOX_LOOP_PIC, VAR.CHECK_BOX_LOOP_PICTURE_DEFAULT_VALUE)){
            		if (!(countOfImages <= listOfPhoto.size() - 1)) {countOfImages=0;}
            		if (showDate){
            		long timeInMillis = Long.parseLong(listOfPhoto.get(countOfImages).getDateInMillis());
            		Calendar c = Calendar.getInstance();
            	    c.setTimeInMillis(timeInMillis);
            	    int mYear = c.get(Calendar.YEAR); 
            	    int mMonth = c.get(Calendar.MONTH)+1;
            	    int mDay = c.get(Calendar.DAY_OF_MONTH);
            	    String date = mYear+"." + mMonth+"." + mDay;
            		tv_date.setText(date);
            		}
            		slideImage.setImageBitmap(decodeFile(new File(listOfPhoto.get(countOfImages).getPath())));
            		
                    countOfImages++;
            	}else{
            		if (countOfImages <= listOfPhoto.size() - 1) {
            			if (showDate){
            			long timeInMillis = Long.parseLong(listOfPhoto.get(countOfImages).getDateInMillis());
                		Calendar c = Calendar.getInstance();
                	    c.setTimeInMillis(timeInMillis);
                	    int mYear = c.get(Calendar.YEAR); 
                	    int mMonth = c.get(Calendar.MONTH)+1;
                	    int mDay = c.get(Calendar.DAY_OF_MONTH);
                	    String date = mYear+"." + mMonth+"." + mDay;
            			tv_date.setText(date);
            			}
                    slideImage.setImageBitmap(decodeFile(new File(listOfPhoto.get(countOfImages).getPath())));
                    
                    countOfImages++;
                } else {
                	if(settings.getBoolean(VAR.CHECK_BOX_LOOP_MISIC, VAR.CHECK_BOX_LOOP_MISIC_DEFAULT_VALUE)){
	                		if (mMediaPlayer.isPlaying()) {
	                        
	                    }
	                		
                	}
                		task.cancel(); 
                	}
            	}
            	
               
                break;
            case 1: //back
            	if(settings.getBoolean(VAR.CHECK_BOX_LOOP_PIC, VAR.CHECK_BOX_LOOP_PICTURE_DEFAULT_VALUE)){
            		if (!(countOfImages >= 0)) {countOfImages = listOfPhoto.size() - 1;}
            		if (showDate){
            		long timeInMillis = Long.parseLong(listOfPhoto.get(countOfImages).getDateInMillis());
            		Calendar c = Calendar.getInstance();
            	    c.setTimeInMillis(timeInMillis);
            	    int mYear = c.get(Calendar.YEAR); 
            	    int mMonth = c.get(Calendar.MONTH)+1;
            	    int mDay = c.get(Calendar.DAY_OF_MONTH);
            	    String date = mYear+"." + mMonth+"." + mDay;
        			tv_date.setText(date);
            		}
            		slideImage.setImageBitmap(decodeFile(new File(listOfPhoto.get(countOfImages).getPath())));
                    countOfImages--;
            	}else{
            		if (countOfImages >= 0) {
            			if (showDate){
            			long timeInMillis = Long.parseLong(listOfPhoto.get(countOfImages).getDateInMillis());
                		Calendar c = Calendar.getInstance();
                	    c.setTimeInMillis(timeInMillis);
                	    int mYear = c.get(Calendar.YEAR); 
                	    int mMonth = c.get(Calendar.MONTH)+1;
                	    int mDay = c.get(Calendar.DAY_OF_MONTH);
                	    String date = mYear+"." + mMonth+"." + mDay;
            			tv_date.setText(date);
            			}
                    slideImage.setImageBitmap(decodeFile(new File(listOfPhoto.get(countOfImages).getPath())));
                    countOfImages--;
                } else {
                	if(settings.getBoolean(VAR.CHECK_BOX_LOOP_MISIC, VAR.CHECK_BOX_LOOP_MISIC_DEFAULT_VALUE)){
                		if (mMediaPlayer.isPlaying()) {
                			//mMediaPlayer.reset();
                    }
            	}
                    task.cancel();
                }
            	}
                
                break;
            case 2: //random
            	if(settings.getBoolean(VAR.CHECK_BOX_LOOP_PIC, VAR.CHECK_BOX_LOOP_PICTURE_DEFAULT_VALUE)){
            		if (!(countOfImages <= listOfPhoto.size() - 1)) {
            			arrayOfRandomNumbers.clear();
                    for (int i = 0; i < listOfPhoto.size(); i++) {
                        arrayOfRandomNumbers.add(i);
                        Log.i(LOG_TAG,""+arrayOfRandomNumbers.get(i));
                    }
                    Collections.shuffle(arrayOfRandomNumbers);
                    countOfImages = 0;
                    }
            		if (showDate){
            		long timeInMillis = Long.parseLong(listOfPhoto.get(arrayOfRandomNumbers.get(countOfImages)).getDateInMillis());
            		Calendar c = Calendar.getInstance();
            	    c.setTimeInMillis(timeInMillis);
            	    int mYear = c.get(Calendar.YEAR); 
            	    int mMonth = c.get(Calendar.MONTH)+1;
            	    int mDay = c.get(Calendar.DAY_OF_MONTH);
            	    String date = mYear+"." + mMonth+"." + mDay;
        			tv_date.setText(date);
            		}
            		slideImage.setImageBitmap(decodeFile(new File(listOfPhoto.get(arrayOfRandomNumbers.get(countOfImages)).getPath())));
                    countOfImages++;
            	}else{
            		if(countOfImages <= listOfPhoto.size()-1) {
            			if (showDate){
            			long timeInMillis = Long.parseLong(listOfPhoto.get(arrayOfRandomNumbers.get(countOfImages)).getDateInMillis());
                		Calendar c = Calendar.getInstance();
                	    c.setTimeInMillis(timeInMillis);
                	    int mYear = c.get(Calendar.YEAR); 
                	    int mMonth = c.get(Calendar.MONTH)+1;
                	    int mDay = c.get(Calendar.DAY_OF_MONTH);
                	    String date = mYear+"." + mMonth+"." + mDay;
            			tv_date.setText(date);
            			}
                slideImage.setImageBitmap(decodeFile(new File(listOfPhoto.get(arrayOfRandomNumbers.get(countOfImages)).getPath())));
                countOfImages++;
                }else{
                	if(settings.getBoolean(VAR.CHECK_BOX_LOOP_MISIC, VAR.CHECK_BOX_LOOP_MISIC_DEFAULT_VALUE)){
                		if (mMediaPlayer.isPlaying()) {
                			//mMediaPlayer.reset();
                		}
            	}
                    task.cancel();
                }
            	}
            	
                
                break;
        }
    }

    private void initOrderToShow() {
        switch (VAR.TEMP_slideShowOrder) {
            case 0://forward
                countOfImages = 0;
                break;
            case 1://back
                countOfImages = listOfPhoto.size() - 1;
                break;
            case 2://random
                arrayOfRandomNumbers.clear();
                for (int i = 0; i < listOfPhoto.size(); i++) {
                    arrayOfRandomNumbers.add(i);
                    Log.i(LOG_TAG,""+arrayOfRandomNumbers.get(i));
                }
                Collections.shuffle(arrayOfRandomNumbers);
                countOfImages = 0;
                break;
        }
    }

    private Bitmap decodeFile(File f) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            final int REQUIRED_SIZE = 250;

            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            return null;
        }
    }
}
