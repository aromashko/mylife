package selfie.madscale.shmelfie.activities;

/**
 * Andrew Bejlyk
 * User: beilik.andry@mail.ru
 * Date: 19.08.14
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;

import selfie.madscale.shmelfie.MyLifeApplication;
import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.adapters.DayAdapter;
import selfie.madscale.shmelfie.adapters.ImageAdapter;
import selfie.madscale.shmelfie.adapters.MonthAdapter;
import selfie.madscale.shmelfie.database.DataManager;
import selfie.madscale.shmelfie.models.DayObject;
import selfie.madscale.shmelfie.models.MonthObject;
import selfie.madscale.shmelfie.models.PhotoObject;
import selfie.madscale.shmelfie.receivers.Widget;
import selfie.madscale.shmelfie.service.CameraService;
import selfie.madscale.shmelfie.utils.DBBackup;
import selfie.madscale.shmelfie.utils.PreferenceUtil;
import selfie.madscale.shmelfie.utils.Supporting;
import selfie.madscale.shmelfie.utils.ZoomOutPageTransformer;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.Camera.PictureCallback;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsense.trace.BugSenseHandler;


public class MainActivity extends ActivityWithMenu {


    public static final String LOG_TAG = "logMain";
    public static final int TYPE_FULL_IMAGE = 3;
    ImageButton btnFavorites, btnSlideShow, btnSettings;
    ImageView btnActivate, btnMoveLeft, btnMoveRight;
    private static TextView txtLocation;
    public static SharedPreferences settings;
    private static GridView gridView;
    private static Context applicationContext;

    public static ImageAdapter imageAdapter;
    public static MonthAdapter monthAdapter;
    public static DayAdapter dayAdapter;
    public static ArrayList<PhotoObject> listOfPhoto;
    public static ArrayList<MonthObject> listOfMonthes;
    public static ArrayList<DayObject> listOfDays;
    public static ArrayList<PhotoObject> tempPhotoObjects;
    public static ArrayList<PhotoObject> bestListOfPhoto;
    public static android.os.Handler adapterHandler;
    public static PhotoObject photoObjectFullImage;

    public static int whichAdapter = 0; // 0 - monthAdapter, 1 - dayAdapter, 2
    // - imageAdapter, 3 - fullImage, 4
    // - favorite, 5 - fullImageFavorite
    public static int tempYear;
    public static int tempMonth;
    public static int tempDay;
    private static int positionOfImage;
    private View fullImage;
    /*private LinearLayout myLayout;*/
    private ImageView fullScreenImage;
    private ImageView textViewAppName, img_flash, img_on, img_off;
    private static Bitmap bitmap;
    private RelativeLayout location, rel_lay_on_off;
    private LinearLayout mainLayout;
    private RelativeLayout headerLayout;
    protected PictureCallback mCall;

    public static final int DELETE_MONTH = 101;
    public static final int DELETE_YEAR = 102;
    public static final int DELETE_DAY = 103;
    public static final int DELETE_PHOTO = 104;
    public static final int DELETE_FAVORITE_PHOTO = 105;
    
    public static final int HANDLER_STOP=1;

    // ��� ��������������
    private static final int DISTANCE = 100;
    private static final int VELOCITY = 200;
    private GestureDetector mGestureDetector;
    int currentPositionOfDayList=0, currentPositionOfMonthList=0, currentPositionOfImageList =0;
    Boolean fullImageShowed=true, fullImageShowedFavorite=true;
    
    private boolean SELECTION_FLAG = false;
    ViewPager viewPager;
    ViewPagerFullImageAdapter viewPagerFullImageAdapter;
    ViewPagerFavoriteImageAdapter viewPagerFavoriteImageAdapter;
    int currentPositionViewPager;
    MyLifeApplication app = (MyLifeApplication) getApplication();
    Boolean firstLaunch= false;
    int offset=0;
    int sdk;
    int prev_year_without_photo=0, next_year_without_photo;
    int photoPositionInYear = 0;
    int currentViewPagerPosition;
    Intent targetedShare;
    BackUpTask backUpTask;
    ProgressDialog pd;
    Handler h;
    ActionBar mActionBar;

    

    public static int getCurrentAdapter() {
        return whichAdapter;
    }


    @Override
    protected void onDestroy() {
    	 BugSenseHandler.closeSession(MainActivity.this);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(receiver);
        CameraService.makeBackup(getApplicationContext());
        super.onDestroy();
    }

    @Override
    protected void onPause() {
    	if (h != null){
    		h.sendEmptyMessage(HANDLER_STOP);
    	}
    	if (pd != null){
    	pd.cancel();
    	}
    	
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    void checkFirstRun() {
        try {
            PreferenceUtil.getString(getApplicationContext(), VAR.IS_FIRST_LAUNCH);
        } catch (IllegalArgumentException e) {
            createDialogBackUp();
        } finally {
            PreferenceUtil.putString(getApplicationContext(), VAR.IS_FIRST_LAUNCH, "no");
        }
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (VAR.ACTION_PHOTO_INSETRED_IN_DB.equals(intent.getAction())) {
                listOfPhoto = DataManager.getPhotoList(context);
                if (adapterHandler != null) {
                    adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
                }
            } else if (VAR.ACTION_IMPORT_UPDATE.equals(intent.getAction())) {
                listOfPhoto = DataManager.getPhotoList(getApplicationContext());
                bestListOfPhoto = DataManager.getBestPhotoList(getApplicationContext());
                Collections.sort(listOfPhoto);
            }
        }
    };

    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        BugSenseHandler.initAndStartSession(MainActivity.this, "d9f53567");
        app = (MyLifeApplication) getApplication();
        
        
        actionBarInstallation();
		 
        sdk = android.os.Build.VERSION.SDK_INT;

        mGestureDetector = new GestureDetector(getBaseContext(), new GestureListener());
        mGestureDetector.setIsLongpressEnabled(true);
        
        
        settings = getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE);
        loadDefaultSettings();
        findViewsById();
        
        applicationContext = getApplicationContext();

        checkFirstRun();
        
        if (!isBackUpEndedSuccessfully()){
        	createDialogBackUp();
        }

        headerButtonsInit();
        createDefaultFolder();
        navigationBarInit();
        makeNavigationText(VAR.currentYear, 0, 0);
        arrayListsInit();
        Collections.sort(listOfPhoto);
        
        initHandlers();
        openMonthAdapter();
        
       
        viewPagerFullImageAdapter = new ViewPagerFullImageAdapter();
        viewPagerFavoriteImageAdapter = new ViewPagerFavoriteImageAdapter();
        viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int position) {
				Log.e("position", position+"");
				if (viewPager.getAdapter() instanceof ViewPagerFullImageAdapter){
					photoObjectFullImage = listOfPhoto.get(position);
					Log.e("Adapter", "photo full");
				}
				else{
					photoObjectFullImage=  bestListOfPhoto.get(position);
					Log.e("Adapter", "photo full favorite");
				}
				Log.e("List size", listOfPhoto.size()+"");
				currentViewPagerPosition = position;
				
				
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        checkBtnState();

        if (settings.getBoolean(VAR.ACTIVE_APP, VAR.ACTIVE_APP_DEFAULT_VALUE)) {
        	Log.e("Servise intent", "go");
        	startFlashAnimation();
            final Intent service = new Intent(MainActivity.this,
                    CameraService.class);

            startService(service);
           
        }
        else{
        	
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(VAR.ACTION_IMPORT_UPDATE);
        intentFilter.addAction(VAR.ACTION_PHOTO_INSETRED_IN_DB);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(receiver, intentFilter);
    }
    
    public void createDialogBackUp(){
    	if (DBBackup.getBackupCount() > 0) {
    		
    		LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
    		View layoutDoAsk = inflater.inflate(
    				R.layout.dlg_backup,
    				(ViewGroup) findViewById(R.id.rl_dlgBackUp_root));
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            //builder.setMessage(R.string.apply_backup);
            
            builder.setView(layoutDoAsk);
            builder.setInverseBackgroundForced(true);
            Button btnAskYes = (Button) layoutDoAsk
    				.findViewById(R.id.btnDlgBackUp_yes);
            Button btnAskNo = (Button) layoutDoAsk
    				.findViewById(R.id.btnDlgBackUp_no);
            final AlertDialog dialog = builder.create();;
            btnAskYes.setOnClickListener(new OnClickListener() {

    			@Override
    			public void onClick(View v) {
    				dialog.dismiss();
                	 restoreFromBackUp();
                }
            });
            btnAskNo.setOnClickListener(new OnClickListener() {

    			@Override
    			public void onClick(View v) {
    				dialog.dismiss();
                	saveSharedPreferences(VAR.BACK_UP_ENDED, true);
                }
            });
            dialog.show();
    }
    }
    
    
    public void calculateOnOffButtonMargin(){
    	final int flashLeftMargin = 8;
    	final int flashTopMargin = 15;
    	final int OnLeftMargin = 7;
    	final int OffLeftMargin = 30;
    	
    	float dens = getResources().getDisplayMetrics().density;
    	float flashLeft = flashLeftMargin*dens;
    	float flashTop = flashTopMargin*dens;
    	float OnLeft = OnLeftMargin*dens;
    	float OffLeft = OffLeftMargin*dens;
    	android.widget.RelativeLayout.LayoutParams params = new android.widget.RelativeLayout.LayoutParams(android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT,
    			android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT );
    	
    	params.setMargins((int)flashLeft, (int) flashTop, 0, 0);
    	img_flash.setLayoutParams(params);
    	android.widget.RelativeLayout.LayoutParams params1 = new android.widget.RelativeLayout.LayoutParams(android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT,
    			android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT );
    	params1.setMargins((int)OnLeft, 0, 0, 0);
    	img_on.setLayoutParams(params1);
    	android.widget.RelativeLayout.LayoutParams params2 = new android.widget.RelativeLayout.LayoutParams(android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT,
    			android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT );
    	params2.setMargins((int)OffLeft, 0, 0, 0);
    	img_off.setLayoutParams(params2);
    }
    
    
    
    public void actionBarInstallation(){
    	ActionBar mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);
 
		View mCustomView = mInflater.inflate(R.layout.action_bar, null);
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
    }
    
    public void startFlashAnimation(){
    		img_flash.setVisibility(View.VISIBLE);
    	 final Animation animation = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
		    animation.setDuration(500); // duration - half a second
		    animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
		    animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
		    animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
		    img_flash.startAnimation(animation);
    }
    
    public void stopFlashAnimation(){
		    img_flash.clearAnimation();
		    img_flash.setVisibility(View.GONE);
   }
    
    public void restoreFromBackUp(){
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    	saveSharedPreferences(VAR.BACK_UP_ENDED, false);
    	pd = new ProgressDialog(MainActivity.this);
        pd.setTitle(getString(R.string.backUp));
        pd.setMessage(getString(R.string.please_wait));
        
        // ������ ����� �� ���������
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // �������� �������� ��������
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.show();
   	 	backUpTask = new BackUpTask();
   	 	backUpTask.execute();
    }


    @Override
    protected void onResume() {
        super.onResume();
        createDefaultFolder();
        checkBtnState();
        //monthAdapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
    	if(SELECTION_FLAG){
    		SELECTION_FLAG = false;
    		Toast.makeText(MainActivity.this, getString(R.string.click_again_to_exit), Toast.LENGTH_SHORT).show();
    		return;
    	}
        /*previousAdapter = whichAdapter;*/
        switch (whichAdapter) {
            case 0:
                finish();
                break;
            case 1:
            	currentPositionOfDayList = 0;
                clearStateTempPhotoList();
                openMonthAdapter();
                makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                break;
            case 2:
            	currentPositionOfImageList = 0;
                clearStateTempPhotoList();
                openDayAdapter(tempMonth);
                makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                gridView.setOnItemLongClickListener(null);
                break;
            case TYPE_FULL_IMAGE:
            	
            	mainLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                clearStateTempPhotoList();
                openImageAdapter(tempDay);
                makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                break;
            case 4:
            	clearStateBestPhotoList();
                openMonthAdapter();
                makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                break;
            case 5:
            	clearStateBestPhotoList();
            	mainLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                openFavoriteAdapter();
                break;
        }


    }

    

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        int selectedPhotos = 0;
        menu.clear();
        
        MenuInflater inflater = getMenuInflater();
        

        switch (whichAdapter) {
            case 0:
                inflater.inflate(R.menu.menu_item, menu);
                break;
            case 1:
                inflater.inflate(R.menu.menu_item, menu);
                break;
            case 2:
                for (PhotoObject photo : tempPhotoObjects) {
                    if (photo.getState())
                        selectedPhotos++;
                }
                if (selectedPhotos > 0) {
                    inflater.inflate(R.menu.menu_selected_photo, menu);
                    if (selectedPhotos > 1) {
                        menu.getItem(3).setEnabled(false);
                        menu.getItem(4).setEnabled(false);
                    }
                } else {
                    inflater.inflate(R.menu.menu_item, menu);
                }
                break;
            case 3:
                inflater.inflate(R.menu.menu_one_photo, menu);
                break;
            case 4:
                for (PhotoObject photo : bestListOfPhoto) {
                    if (photo.getState())
                        selectedPhotos++;
                }
                if (selectedPhotos > 0) {
                    inflater.inflate(R.menu.selected_photo_favorite, menu);
                    if (selectedPhotos > 1) {
                        menu.getItem(2).setEnabled(false);
                        menu.getItem(3).setEnabled(false);
                    }
                } else {
                    inflater.inflate(R.menu.menu_item, menu);
                }
                break;
            case 5:
                inflater.inflate(R.menu.menu_one_photo_favorite, menu);
                break;
        }

        return true;
    }

    private void clearStateTempPhotoList() {
        for (PhotoObject photo : tempPhotoObjects) {
            photo.setState(false);
        }
    }
    
    private void clearStateBestPhotoList() {
        for (PhotoObject photo : bestListOfPhoto) {
            photo.setState(false);
        }
    }

    private void loadDefaultSettings() {
        VAR.TEMP_fromTime = (int) settings.getInt(VAR.FROM_TIME, VAR.FROM_TIME_DEFAULT_VALUE);
        VAR.TEMP_toTime = (int) settings.getInt(VAR.TO_TIME, VAR.TO_TIME_DEFAULT_VALUE);
        VAR.whenMakePhoto = settings.getInt(VAR.WHEN_MAKE_PHOTO, VAR.WHEN_MAKE_PHOTO_DEFAULT_VALUE);
        VAR.TEMP_angleCamera = VAR.angleCamera[settings.getInt(VAR.ANGLE_CAMERA,
        		VAR.ANGLE_DEFAULT_VALUE)];
        VAR.TEMP_slideShowFrequency = VAR.slideShowFrequency[settings.getInt(
                VAR.SLIDE_SHOW_FREQUENCY, 1)];
        VAR.TEMP_slideShowOrder = VAR.slideShowOrder[settings.getInt(
                VAR.SLIDE_SHOW_ORDER, VAR.SLIDE_SHOW_ORDER_DEFAULT_VALUE)];
        
    }

    private void checkBtnState() {
        if (settings.getBoolean(VAR.ACTIVE_APP, VAR.ACTIVE_APP_DEFAULT_VALUE)) {
        	img_on.setImageResource(R.drawable.on_selected);
        	img_off.setImageResource(R.drawable.off_normal);
        	img_flash.setVisibility(View.VISIBLE);
        	 /*final Intent service = new Intent(MainActivity.this,
                     CameraService.class);
             startService(service);*/

        } else {
        	img_on.setImageResource(R.drawable.on_normal);
        	img_off.setImageResource(R.drawable.off_selected);
        	img_flash.setVisibility(View.GONE);
        
        }
    }

    public void createDefaultFolder() {

        if (!new File(VAR.PATH).exists()) {
            new File(VAR.PATH).mkdir();
            Supporting.showToast(
                    applicationContext,
                    getString(R.string.create_default_dir)
                            + settings.getString("path", getString(R.string.app_default_folder)));
        }
    }

    private void loadDataToLists() {
        listOfPhoto = DataManager.getPhotoList(applicationContext);
        bestListOfPhoto = DataManager.getBestPhotoList(applicationContext);
    }

    private void arrayListsInit() {
        listOfPhoto = new ArrayList<PhotoObject>();
        listOfMonthes = new ArrayList<MonthObject>();
        listOfDays = new ArrayList<DayObject>();
        tempPhotoObjects = new ArrayList<PhotoObject>();
        bestListOfPhoto = new ArrayList<PhotoObject>();
        loadDataToLists();
    }

    private void initHandlers() {
        adapterHandler = new android.os.Handler() {

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case VAR.UPDATE_ADAPTER:
                        switch (whichAdapter) {
                            case 0:
                                openMonthAdapter();
                                break;
                            case 1:
                                openDayAdapter(tempMonth);
                                break;
                            case 2:
                            	currentPositionOfImageList = gridView.getFirstVisiblePosition();
                            	imageAdapter.notifyDataSetChanged();
                                break;
                            case TYPE_FULL_IMAGE:
                            	mainLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                            	 openImageAdapter(tempDay);
                            	 break;
                            case 4:
                            	mainLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                                openFavoriteAdapter();
                                break;
                            case 5:
                            	mainLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                            	openFavoriteAdapter();
                            	break;
                        }
                        break;
                }
            }
        };
    }

    public void alertDialogConfirm(int title, int msg, final int whatDelete,
                                   final int month, final int day) {
    	LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
    	View layoutDelete = inflater.inflate(
				R.layout.dlg_delete,
				(ViewGroup) findViewById(R.id.rl_dlgDelete_root));
    	
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        //alertDialogBuilder.setTitle(title);
        
        //alertDialogBuilder.set
        alertDialogBuilder.setTheme(AlertDialog.THEME_HOLO_LIGHT);
        alertDialogBuilder.setView(layoutDelete);
        
        TextView tv_title_delete = (TextView) layoutDelete
		.findViewById(R.id.tv_title_delete);
        tv_title_delete.setText(msg);
        Button btnAskYes = (Button) layoutDelete
				.findViewById(R.id.btnDlgDelete_yes);
        Button btnAskNo = (Button) layoutDelete
				.findViewById(R.id.btnDlgDelete_no);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        btnAskYes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				alertDialog.cancel();
				switch (whatDelete) {
                case DELETE_YEAR:
                    Supporting.deleteYear(applicationContext,
                            VAR.currentYear);
                    break;
                case DELETE_MONTH:
                    Supporting.deleteMonth(applicationContext,
                            VAR.currentYear, month);
                    break;
                case DELETE_DAY:
                    Supporting.deleteDay(applicationContext,
                            VAR.currentYear, month, day);
                    break;
                case DELETE_PHOTO:
                    listOfPhoto.remove(photoObjectFullImage);
                    DataManager.deletePhoto(applicationContext,
                            photoObjectFullImage.getId());
                    File forDelete = new File(photoObjectFullImage.getPath());
                    forDelete.delete();
                    openImageAdapter(tempDay);
                    break;
                case DELETE_FAVORITE_PHOTO:
                    bestListOfPhoto.remove(photoObjectFullImage);
                    DataManager.deleteBestPhoto(applicationContext,
                            photoObjectFullImage.getId());
                    File forDeleteFavorite = new File(photoObjectFullImage.getPath());
                    forDeleteFavorite.delete();
                    openFavoriteAdapter();
                    break;
            }
				
			}
		});
        btnAskNo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				alertDialog.cancel();
				
			}
		});
        /*alertDialogBuilder
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        switch (whatDelete) {
                            case DELETE_YEAR:
                                Supporting.deleteYear(applicationContext,
                                        VAR.currentYear);
                                break;
                            case DELETE_MONTH:
                                Supporting.deleteMonth(applicationContext,
                                        VAR.currentYear, month);
                                break;
                            case DELETE_DAY:
                                Supporting.deleteDay(applicationContext,
                                        VAR.currentYear, month, day);
                                break;
                            case DELETE_PHOTO:
                                listOfPhoto.remove(photoObjectFullImage);
                                DataManager.deletePhoto(applicationContext,
                                        photoObjectFullImage.getId());
                                File forDelete = new File(photoObjectFullImage.getPath());
                                forDelete.delete();
                                openImageAdapter(tempDay);
                                break;
                            case DELETE_FAVORITE_PHOTO:
                                bestListOfPhoto.remove(photoObjectFullImage);
                                DataManager.deleteBestPhoto(applicationContext,
                                        photoObjectFullImage.getId());
                                File forDeleteFavorite = new File(photoObjectFullImage.getPath());
                                forDeleteFavorite.delete();
                                openFavoriteAdapter();
                                break;
                        }
                    }
                })
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });*/
        
        alertDialog.show();
    }
    
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
    		ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo cmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
       // Log.e("Context Menu", whichAdapter +" selected");
        switch (whichAdapter) {
            case 0:
                menu.add(1, cmi.position, 0, R.string.delete);
                menu.add(2, cmi.position, 0, R.string.delete_all_in_year);
                break;
            case 1:
                menu.add(1, cmi.position, 0, R.string.delete);
                menu.add(2, cmi.position, 0, R.string.delete_all_in_month);
                break;
            case 2:
                menu.add(1, cmi.position, 0, R.string.allocate);
                menu.add(2, cmi.position, 0, R.string.allocate_all);
                break;
            case 3:
                menu.add(1, Menu.NONE, Menu.NONE,
                        R.string.selected_photo_menu_delete);
                menu.add(2, Menu.NONE, Menu.NONE,
                        R.string.selected_photo_menu_favorite);
                menu.add(3, Menu.NONE, Menu.NONE, R.string.selected_photo_menu_send);
                menu.add(4, Menu.NONE, Menu.NONE, R.string.selected_photo_menu_info);
                menu.add(5, Menu.NONE, Menu.NONE,
                        R.string.selected_photo_menu_rename);
                break;
            case 4:
                menu.add(1, cmi.position, 0, R.string.allocate);
                menu.add(2, cmi.position, 0, R.string.allocate_all);
                break;
            case 5:
                menu.add(1, Menu.NONE, 0, R.string.selected_photo_menu_delete);
                menu.add(2, Menu.NONE, 0, R.string.selected_photo_menu_send);
                menu.add(3, Menu.NONE, 0, R.string.selected_photo_menu_info);
                menu.add(4, Menu.NONE, 0, R.string.selected_photo_menu_rename);
                break;
        }
    }

 

	@Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (whichAdapter) {
            case 0:
                int month = listOfMonthes.get(item.getItemId()).getMonthInYear();
                int day = 0;
                if (listOfDays.size() != 0)
                    day = listOfDays.get(item.getItemId()).getDayInMonth();
                switch (item.getGroupId()) {
                    case 1:
                        alertDialogConfirm(R.string.title_delete,
                                R.string.confirm_delete_month, DELETE_MONTH, month, day);
                        break;
                    case 2:
                        alertDialogConfirm(R.string.title_delete,
                                R.string.confirm_delete_year, DELETE_YEAR, month, day);
                        break;
                }
                break;
            case 1:
                day = listOfDays.get(item.getItemId()).getDayInMonth();
                switch (item.getGroupId()) {
                    case 1:
                        alertDialogConfirm(R.string.title_delete,
                                R.string.confirm_delete_day, DELETE_DAY, tempMonth, day);
                        break;
                    case 2:
                        alertDialogConfirm(R.string.title_delete,
                                R.string.confirm_delete_month, DELETE_MONTH, tempMonth,
                                day);
                        break;
                }
                break;
            case 2:
                switch (item.getGroupId()) {
                    case 1:
                    	SELECTION_FLAG = true;
                        if (!tempPhotoObjects.get(item.getItemId()).getState()) {
                            tempPhotoObjects.get(item.getItemId()).setState(true);
                        } else {
                            tempPhotoObjects.get(item.getItemId()).setState(false);
                        }
                        adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
                        break;
                    case 2:
                    	SELECTION_FLAG = true;
                        for (PhotoObject tempPhotoObject : tempPhotoObjects) {
                            tempPhotoObject.setState(true);
                        }
                        adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
                        break;
                }
                break;
            case TYPE_FULL_IMAGE:
            	photoObjectFullImage = listOfPhoto.get(currentViewPagerPosition);
                switch (item.getGroupId()) {
                    case 1:
                        alertDialogConfirm(R.string.title_delete, R.string.delete,
                                DELETE_PHOTO, tempMonth, tempDay);
                        break;
                    case 2:
                        bestListOfPhoto.add(photoObjectFullImage);
                        DataManager.insertBestPhoto(applicationContext,
                                photoObjectFullImage);
                        DataManager.deletePhoto(applicationContext,
                                photoObjectFullImage.getId());
                        listOfPhoto.remove(photoObjectFullImage);
                        bestListOfPhoto = DataManager
                                .getBestPhotoList(applicationContext);
                        openImageAdapter(tempDay);
                        break;
                    case 3:
                        sharePhoto(MainActivity.this, photoObjectFullImage);
                        break;
                    case 4:
                        Supporting.showPhotoInfo(this, photoObjectFullImage);
                        break;
                    case 5:
                        Supporting.renamePhoto(this, photoObjectFullImage,
                                VAR.RENAME_PHOTO);
                        break;
                }
                break;
            case 4:
            	//<------
                switch (item.getGroupId()) {
                    case 1:
                    	SELECTION_FLAG = true;
                        if (!bestListOfPhoto.get(item.getItemId()).getState()) {
                            bestListOfPhoto.get(item.getItemId()).setState(true);
                        } else {
                            bestListOfPhoto.get(item.getItemId()).setState(false);
                        }
                        adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
                        break;
                    case 2:
                    	SELECTION_FLAG = true;
                        for (PhotoObject tempPhotoObject : bestListOfPhoto) {
                            tempPhotoObject.setState(true);
                        }
                        adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
                        break;
                }
                break;
            case 5:
            	photoObjectFullImage = bestListOfPhoto.get(currentViewPagerPosition);
                switch (item.getGroupId()) {
                    case 1:
                        alertDialogConfirm(R.string.title_delete, R.string.delete,
                                DELETE_FAVORITE_PHOTO, tempMonth, tempDay);
                        break;
                    case 2:
                    	 sharePhoto(MainActivity.this, photoObjectFullImage);
                        break;
                    case 3:
                        Supporting.showPhotoInfo(this, photoObjectFullImage);
                        break;
                    case 4:
                        Supporting.renamePhoto(this, photoObjectFullImage,
                                VAR.RENAME_BEST_PHOTO);
                        break;
                }
        }
        return true;
    }

    @SuppressLint("NewApi")
	private void openMonthAdapter() {
        whichAdapter = 0;
        changeLayout();
        gridView = (GridView) findViewById(R.id.GridView01);
        registerForContextMenu(gridView);
        Supporting.getPhotosInYear(VAR.currentYear);
        listOfMonthes.clear();
        for (int i = 0; i < VAR.whichMonthHavePhoto.length; i++)
            VAR.whichMonthHavePhoto[i] = false;
        Supporting.getNumberOfMonthes(VAR.currentYear);
        for (int i = 1; i < VAR.MONTHES; i++) {
            if (VAR.whichMonthHavePhoto[i]) {
                listOfMonthes.add(new MonthObject(i));
            }
        }
        monthAdapter = new MonthAdapter(applicationContext, listOfMonthes);
        gridView.setAdapter(monthAdapter);
        
       

        
        if (sdk<=10){
        		if (currentPositionOfMonthList!=0){
        			gridView.setSelection(currentPositionOfMonthList);
        		}
            //gridView.scrollBy(0, offset);
            }
            else if (currentPositionOfMonthList!=0){
            gridView.smoothScrollToPositionFromTop(currentPositionOfMonthList, 0);
            }
       
       
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int position, long l) {
            	offset = (int)(5 * getResources().getDisplayMetrics().density); 
              	 
            	currentPositionOfMonthList = gridView.getFirstVisiblePosition();
                /*final View first = gridView.getChildAt(0);
                if (null != first) {
                    offset -= first.getTop();
                }*/
                int month = listOfMonthes.get(position).getMonthInYear();
                openDayAdapter(month);
                tempMonth = month;
                makeNavigationText(VAR.currentYear, tempMonth, tempDay);
            }
        });
    }
    
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }
    
    /*public int getGridScrollY(GridView gridView)
    {
       int pos, itemY = 0;
       View view;

       pos = gridView.getFirstVisiblePosition();
       view = gridView.getChildAt(0);

       if(view != null)
          itemY = view.getTop();

       return YFromPos(pos, gridView) - itemY;
    }

    private int YFromPos(int pos, GridView gridView)
    {
       int row = pos / 3;

       if(pos - row * 3 > 0)
          ++row;

       return row * gridView.getR;
    }
    
    public void scrollGridToY(int scrollY, GridView gridView)
    {
       int row, off, oldOff, oldY, item;

       // calc old offset:
       oldY = gridView.getScrollY(); // getGridScrollY() will not work here
       row = oldY / gridView;
       oldOff = oldY - row * m_rowHeight;

       // calc new offset and item:
       row = scrollY / m_rowHeight;
       off = scrollY - row * m_rowHeight;
       item = row * m_numColumns;

       setSelection(item);
       scrollBy(0, off - oldOff);
    }*/
    
    public int dpToPx(int dp){
    	Resources r = getResources();
    	int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    	return px;
    }

    
	@SuppressLint("NewApi")
	private void openDayAdapter(int inMonth) {
        whichAdapter = 1;
        changeLayout();
        listOfDays.clear();
        Supporting.getPhotosInMonth(inMonth);
        for (int i = 0; i < VAR.whichDaysHavePhoto.length; i++){
            VAR.whichDaysHavePhoto[i] = false;}
        Supporting.getNumberOfDays(inMonth);
        for (int i = 1; i < VAR.DAYS; i++) {
            if (VAR.whichDaysHavePhoto[i]) {
                listOfDays.add(new DayObject(i));
            }
        }
        dayAdapter = new DayAdapter(applicationContext, listOfDays);
        gridView.setAdapter(dayAdapter);
        
        if (sdk<=10){
        	if (currentPositionOfDayList!=0){
        gridView.setSelection(currentPositionOfDayList);
        	}
        //gridView.scrollBy(0, dpToPx(10));
        }
        else if (currentPositionOfDayList !=0){
        	
        gridView.smoothScrollToPositionFromTop(currentPositionOfDayList, 0);
        }
        
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int position, long l) {
            	offset = (int)(5 * getResources().getDisplayMetrics().density); 
           	 
            	currentPositionOfDayList = gridView.getFirstVisiblePosition();
              /*  final View first = gridView.getChildAt(0);
                if (null != first) {
                    offset -= first.getTop();
                }*/
            	
            	Log.e("Current Position", currentPositionOfDayList+ "");
                int day = listOfDays.get(position).getDayInMonth();
                openImageAdapter(day);
                tempDay = day;

                makeNavigationText(VAR.currentYear, tempMonth, tempDay);
            }
        });
    }

    @SuppressLint("NewApi")
	private void openImageAdapter(int inDay) {
        whichAdapter = 2;
        changeLayout();
        tempPhotoObjects.clear();
        gridView.setVisibility(GridView.VISIBLE);
        Supporting.getImagesOfDay(inDay, tempMonth);
        imageAdapter = new ImageAdapter(applicationContext, tempPhotoObjects);

        gridView.setAdapter(imageAdapter);
        if (sdk<=10){
            gridView.setSelection(currentPositionOfImageList);
            //gridView.scrollBy(0, offset);
            }
            else if (currentPositionOfImageList != 0){
            gridView.smoothScrollToPositionFromTop(currentPositionOfImageList, 0);
            }
        
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int position, long l) {
            	//offset = (int)(5 * getResources().getDisplayMetrics().density); 
              	 
            	currentPositionOfImageList = gridView.getFirstVisiblePosition();
               /* final View first = gridView.getChildAt(0);
                if (null != first) {
                    offset -= first.getTop();
                }*/
                if(SELECTION_FLAG){
                	if (!imageAdapter.getItem(position).getState()) {
                		imageAdapter.getItem(position).setState(true);
                    } else {
                    	imageAdapter.getItem(position).setState(false);
                    }
                    adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
                }else{
                	/**on item clik*/
                	positionOfImage = position;
                    /*imageAdapterTop = gridView.getFirstVisiblePosition();
                    imageAdapterBottom = gridView.getLastVisiblePosition();*/
                    
                   
                    whichAdapter = TYPE_FULL_IMAGE;
                    changeLayout();
                    photoPositionInYear = 0;
                    for (PhotoObject photo : listOfPhoto) {
                        int year = photo.getYear();
                        int monthInYear = photo.getMonthInYear();
                        int dayInMonth = photo.getDayInMonth();
                        if (year == VAR.currentYear && monthInYear == tempMonth && dayInMonth == tempDay) {
                        	photoPositionInYear = photoPositionInYear + positionOfImage;
                        	break;
                        }
                        photoPositionInYear ++;
                        
                    }
                    Log.e("Photo position in", photoPositionInYear+"");
                    Log.e("List size in", listOfPhoto.size()+"");
                    viewPager.setLongClickable(true);
                    viewPager.setAdapter(viewPagerFullImageAdapter);
                    viewPager.setCurrentItem(photoPositionInYear);
                    makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                }
            	
            }
        });
      }


    private void openFavoriteAdapter() {
        whichAdapter = 4;
        changeLayout();

        imageAdapter = new ImageAdapter(applicationContext, bestListOfPhoto);
        gridView.setAdapter(imageAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int position, long l) {
            	if(SELECTION_FLAG){
            		 if (!imageAdapter.getItem(position).getState()) {
            			 imageAdapter.getItem(position).setState(true);
                     } else {
                    	 imageAdapter.getItem(position).setState(false);
                     }
                     adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
            	}else{
            		positionOfImage = position;
            		//showFullImageFavorite(position);
            		whichAdapter = 5;
                    changeLayout();
                    viewPager.setAdapter(viewPagerFavoriteImageAdapter);
                    viewPager.setCurrentItem(position);
            	}
            	
            }
        });
    }

   

    private void findViewsById() {
        gridView = (GridView) findViewById(R.id.GridView01);
        mainLayout = (LinearLayout) findViewById(R.id.LinearLayoutMain);
        headerLayout = (RelativeLayout) findViewById(R.id.Header);
        viewPager = (ViewPager)findViewById(R.id.pager);
        location = (RelativeLayout) findViewById(R.id.relativeLayoutLocation);
        img_flash = (ImageView) findViewById(R.id.img_flash);
        img_on = (ImageView) findViewById(R.id.img_on);
        img_off = (ImageView) findViewById(R.id.img_off);
        rel_lay_on_off =(RelativeLayout) findViewById(R.id.rel_lay_on_off);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
    	mGestureDetector.onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

    @SuppressLint("NewApi")
	private void changeLayout() {
        switch (whichAdapter) {
            case 0:// month
            	
                mainLayout.removeAllViews();
                mainLayout.addView(location);
                mainLayout.addView(gridView);
                break;
            case 1:// day
                mainLayout.removeAllViews();
                mainLayout.addView(location);
                mainLayout.addView(gridView);
                
               
                break;
            case 2:// image
            	mainLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                mainLayout.removeAllViews();
                mainLayout.addView(location);
                mainLayout.addView(gridView);
                break;
            case TYPE_FULL_IMAGE:// full image
            	if (sdk < 11){
                	supportInvalidateOptionsMenu();
                	}
                	else{
                	invalidateOptionsMenu();
                	}
                mainLayout.removeAllViews();
              
                mainLayout.setBackgroundColor(Color.BLACK);
                mainLayout.addView(viewPager);
            	
                break;
            case 4:// favorite
            	mainLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                mainLayout.removeAllViews();
                View favorite = LayoutInflater.from(applicationContext).inflate(
                        R.layout.favorite, null, false);
                mainLayout.addView(favorite);
                mainLayout.addView(gridView);
                break;
            case 5:// favorite full image
            	if (sdk < 11){
            	supportInvalidateOptionsMenu();
            	}
            	else{
            	invalidateOptionsMenu();
            	}
                mainLayout.removeAllViews();
                mainLayout.setBackgroundColor(Color.BLACK);
                mainLayout.addView(viewPager);
                break;
        }
    }

    public static void rotate(Bitmap src, float degree) {
        Matrix matrix = new Matrix();
        matrix.setRotate(degree);
        bitmap = Bitmap.createBitmap(src, 0, 0, src.getWidth(),
                src.getHeight(), matrix, true);
    }

    public static Bitmap decodeFile(int position, File f) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            final int REQUIRED_SIZE = 250;

            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE
                    && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            return null;
        }
    }
    
   
    private void clickPrev(){
        switch (whichAdapter) {
            case 0:
                // makeNavigationText(VAR.currentYear -= 1, tempMonth,
                // tempDay);
                Supporting.isPrevYearExist();
                makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                openMonthAdapter();
                break;
            case 1:
                while (tempMonth >= 0) {
                    tempMonth--;
                    if (tempMonth == 0){
                    	Supporting.isPrevYearExist();
                        tempMonth = VAR.MONTHES - 1;

                        listOfMonthes.clear();
                        for (int i = 0; i < VAR.whichMonthHavePhoto.length; i++)
                            VAR.whichMonthHavePhoto[i] = false;
                        Supporting.getNumberOfMonthes(VAR.currentYear);
                        for (int i = 1; i < VAR.MONTHES; i++) {
                            if (VAR.whichMonthHavePhoto[i]) {
                                listOfMonthes.add(new MonthObject(i));
                            }
                        }
                        if (listOfMonthes.size() == 0){
                        	openMonthAdapter();
                        	 makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                        	break;
                        }
                    }
                    if (VAR.whichMonthHavePhoto[tempMonth]) {
                        makeNavigationText(VAR.currentYear, tempMonth,
                                tempDay);
                        openDayAdapter(tempMonth);
                        break;
                    
                    }
                }
                break;
            case 2:
            	prev_year_without_photo=0;
                while (tempDay >= 0) {
                    tempDay--;
                    //Log.e("Temp Day", tempDay +"");
                    if (tempDay == 0){
                        tempDay = VAR.DAYS - 1;
                    	tempMonth--;
                    	 //Log.e("Temp Month", tempMonth +"");
                    	if (tempMonth == 0){
                    		tempMonth = VAR.MONTHES - 1;
                    		Supporting.isPrevYearExist();
                    		Supporting.getPhotosInYear(VAR.currentYear);
                    		if (tempPhotoObjects.size()==0){
                    		prev_year_without_photo++;
                    		}
                    		 Log.e("Current Year", VAR.currentYear +"");
                    		 if (prev_year_without_photo >= VAR.MINIMUN_LENGTH_YEAR_WITHOUT_PHOTO){
                    			 VAR.currentYear = VAR.MINIMUN_LENGTH_YEAR_WITHOUT_PHOTO-1 +VAR.currentYear;
                    			 openMonthAdapter();
                    			 makeNavigationText(VAR.currentYear, tempMonth,
                                         tempDay);
                    			 break;
                    		 }
                    	}
                    	for (int i = 1; i < VAR.MONTHES; i++) {
                    		  VAR.whichMonthHavePhoto[i] = false;
                    	}
                    	for (int i = 0; i < VAR.whichDaysHavePhoto.length; i++){
                            VAR.whichDaysHavePhoto[i] = false;}
                    	Supporting.getNumberOfMonthes(VAR.currentYear);
                    	 Supporting.getNumberOfDays(tempMonth); 
                    }
                   
                    if (!VAR.whichMonthHavePhoto[tempMonth]){
                    	//Log.e("Month haven't photo", tempMonth+"");
                    }
                    else if (VAR.whichDaysHavePhoto[tempDay]) {
                        makeNavigationText(VAR.currentYear, tempMonth,
                                tempDay);
                        openImageAdapter(tempDay);
                        
                        break;
                    }
                }
                break;
            case 3:
                /*if (positionOfImage == 0) {
                    showFullImage(tempPhotoObjects.size() - 1, true);
                    positionOfImage = tempPhotoObjects.size() - 1;
                } else {
                    showFullImage(positionOfImage -= 1, true);
                }*/
                break;
            case 5:
                /*if (positionOfImage == 0) {
                	showFullImageFavorite(bestListOfPhoto.size() - 1);
                    positionOfImage = bestListOfPhoto.size() - 1;
                } else {
                	showFullImageFavorite(positionOfImage -= 1);
                }*/
                break;
        }
    }

    private void makeNavigationText(int year, int month, int day) {
        switch (whichAdapter) {
            case 0:
                txtLocation.setText("" + year);
                break;
            case 1:
                txtLocation.setText(year + " > " + month);
                break;
            case 2:
                txtLocation.setText(year + " > " + month + " > " + day);
                break;
            case 3:
                break;
        }
    }

    private void clickNext(){
        switch (whichAdapter) {
            case 0:
                Supporting.isNextYearExist();
                makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                openMonthAdapter();
                break;
            case 1:
                while (tempMonth <= VAR.MONTHES) {
                    tempMonth++;
                    if (tempMonth == VAR.MONTHES){
                        tempMonth = 0;
                    Supporting.isNextYearExist();
                    

                    listOfMonthes.clear();
                    for (int i = 0; i < VAR.whichMonthHavePhoto.length; i++)
                        VAR.whichMonthHavePhoto[i] = false;
                    Supporting.getNumberOfMonthes(VAR.currentYear);
                    for (int i = 1; i < VAR.MONTHES; i++) {
                        if (VAR.whichMonthHavePhoto[i]) {
                            listOfMonthes.add(new MonthObject(i));
                        }
                    }
                    if (listOfMonthes.size() == 0){
                    	openMonthAdapter();
                    	 makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                    	break;
                    }
                
                }
                    if (VAR.whichMonthHavePhoto[tempMonth]) {
                        makeNavigationText(VAR.currentYear, tempMonth,
                                tempDay);
                        openDayAdapter(tempMonth);
                        break;
                    }
                }
                break;
            case 2:
               
                next_year_without_photo=0;
                while (tempDay <= VAR.DAYS) {
                    tempDay++;
                    //Log.e("Temp Day", tempDay +"");
                    if (tempDay  == VAR.DAYS){
                        tempDay = 0;
                        tempMonth++;
                    	 //Log.e("Temp Month", tempMonth +"");
                    	if (tempMonth == VAR.MONTHES){
                    		tempMonth = 0;
                    		Supporting.isNextYearExist();
                    		Supporting.getPhotosInYear(VAR.currentYear);
                    		if (tempPhotoObjects.size()==0){
                    			next_year_without_photo++;
                    		}
                    		 Log.e("Current Year", VAR.currentYear +"");
                    		 if (next_year_without_photo >= VAR.MINIMUN_LENGTH_YEAR_WITHOUT_PHOTO){
                    			 VAR.currentYear =  VAR.currentYear- VAR.MINIMUN_LENGTH_YEAR_WITHOUT_PHOTO+1;
                    			 openMonthAdapter();
                    			 makeNavigationText(VAR.currentYear, tempMonth,
                                         tempDay);
                    			 break;
                    		 }
                    	}
                    	for (int i = 1; i < VAR.MONTHES; i++) {
                    		  VAR.whichMonthHavePhoto[i] = false;
                    	}
                    	for (int i = 0; i < VAR.whichDaysHavePhoto.length; i++){
                            VAR.whichDaysHavePhoto[i] = false;}
                    	Supporting.getNumberOfMonthes(VAR.currentYear);
                    	 Supporting.getNumberOfDays(tempMonth); 
                    }
                   
                    if (!VAR.whichMonthHavePhoto[tempMonth]){
                    	//Log.e("Month haven't photo", tempMonth+"");
                    }
                    else if (VAR.whichDaysHavePhoto[tempDay]) {
                        makeNavigationText(VAR.currentYear, tempMonth,
                                tempDay);
                        openImageAdapter(tempDay);
                        
                        break;
                    }
                }
                break;
                
            case 3:
               /* if (positionOfImage == tempPhotoObjects.size() - 1) {
                    showFullImage(0, true);
                    positionOfImage = 0;
                } else {
                    showFullImage(positionOfImage += 1, true);
                }*/
                break;
            case 5:
               /* if (positionOfImage == bestListOfPhoto.size() - 1) {
                    showFullImageFavorite(0);
                    positionOfImage = 0;
                } else {
                	showFullImageFavorite(positionOfImage += 1);
                }*/
                break;
        }
    }

    private void navigationBarInit() {
        btnMoveLeft = (ImageView) findViewById(R.id.moveLeft);
        btnMoveLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               clickPrev();
            }
        });
        btnMoveRight = (ImageView) findViewById(R.id.moveRight);
        btnMoveRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               clickNext();
            }
        });
        txtLocation = (TextView) findViewById(R.id.locationn);
    }

    private void headerButtonsInit() {
        final Intent service = new Intent(MainActivity.this,
                CameraService.class);
        final Intent settingsActivity = new Intent(MainActivity.this,
                SettingsActivity.class);
        final Intent slideShowActivity = new Intent(MainActivity.this,
                SlideShowActivity.class);
        slideShowActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        btnActivate = (ImageView) findViewById(R.id.activate);

        btnActivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	
                if (settings.getBoolean(VAR.ACTIVE_APP, VAR.ACTIVE_APP_DEFAULT_VALUE)) {
                	 stopService(service);
                    img_on.setImageResource(R.drawable.on_normal);
                 	img_off.setImageResource(R.drawable.off_selected);
                 	stopFlashAnimation();
                 
                     RemoteViews remoteViews = new RemoteViews(MainActivity.this.getPackageName(), R.layout.widget_layout);
                     remoteViews.setInt(R.id.widget_swicher, "setBackgroundResource", R.drawable.widget_off);
                     Widget.pushWidgetUpdate(MainActivity.this, remoteViews);
                     saveSharedPreferences(VAR.ACTIVE_APP, false);
                    
                } else {
                	startService(service);
                    img_on.setImageResource(R.drawable.on_selected);
                	img_off.setImageResource(R.drawable.off_normal);
                	startFlashAnimation();
                    RemoteViews remoteViews = new RemoteViews(MainActivity.this.getPackageName(), R.layout.widget_layout);
                    remoteViews.setInt(R.id.widget_swicher, "setBackgroundResource", R.drawable.widget_on);
                    Widget.pushWidgetUpdate(MainActivity.this, remoteViews);
                    saveSharedPreferences(VAR.ACTIVE_APP, true);
                }
            }
        });

        btnSettings = (ImageButton) findViewById(R.id.settings);
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(settingsActivity);
            }
        });
        

        btnSlideShow = (ImageButton) findViewById(R.id.slideshow);
        btnSlideShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	if (whichAdapter == 0 || whichAdapter == 1 || whichAdapter == 2 || whichAdapter == 3){
            		if (listOfPhoto.size()>0){
            			slideShowActivity.putExtra(SlideShowActivity.FULL_LIST_OF_PHOTO, true);
            			startActivity(slideShowActivity);
            		}
            		else{
                		Toast.makeText(MainActivity.this, getString(R.string.you_have_no_photo), Toast.LENGTH_LONG).show();
                	}
            	}
            	else{
            		if (bestListOfPhoto.size()>0){
            			slideShowActivity.putExtra(SlideShowActivity.FULL_LIST_OF_PHOTO, false);
            			startActivity(slideShowActivity);
            		}
            		else{
                		Toast.makeText(MainActivity.this, getString(R.string.you_have_no_photo), Toast.LENGTH_LONG).show();
                	}
            	}
            }
        });

        btnFavorites = (ImageButton) findViewById(R.id.favorite);
        btnFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	if(SELECTION_FLAG){
            		SELECTION_FLAG = false;
            	}
                openFavoriteAdapter();
            }
        });

        textViewAppName = (ImageButton) findViewById(R.id.appName);
        textViewAppName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMonthAdapter();
                makeNavigationText(VAR.currentYear, tempMonth, tempDay);
                mainLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            }
        });
    }

    public void saveSharedPreferences(String key, Boolean value) {

        SharedPreferences settings = getSharedPreferences(VAR.SETTINGS,
                MODE_PRIVATE);
        SharedPreferences.Editor ed = settings.edit();
        ed.putBoolean(key, value);
        ed.commit();
    }

    
    public void sharePhoto(final Context context, PhotoObject photo) {
		File file = new File(photo.getPath());
		 
		   MediaScannerConnection.scanFile(context,
		           new String[] { file.toString() }, null,
		           new MediaScannerConnection.OnScanCompletedListener() {
		       public void onScanCompleted(String path, Uri uri) {
		           Log.i("ExternalStorage", "Scanned " + path + ":");
		           Log.i("ExternalStorage", "-> uri=" + uri);
		     
		     Intent intent = new Intent(Intent.ACTION_SEND);
		     intent.setType("image/*");

		     intent.putExtra(Intent.EXTRA_STREAM, uri);
		     intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
		     intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.my_random_selfie_taken_via_shmelfie));
		     intent.putExtra("sms_body", getString(R.string.my_random_selfie_taken_via_shmelfie));
		     Intent chooser = Intent.createChooser(intent, context.getString(R.string.share_image));
		     context.startActivity(chooser);
		       }
		  });
	}
    
    class BackUpTask extends AsyncTask<Void, Void, Void> {
        
        @Override
        protected void onPreExecute() {
          super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void...values) {
        	int backUpCount = DBBackup.getBackupCountAll(MainActivity.this);
        	
        	 // ������������� ��������
            pd.setMax(backUpCount);
            pd.setIndeterminate(false);
            publishProgress();
        	DBBackup.applyBackup(getApplicationContext());
        	
        	 
          return null;
        }


        @Override
        protected void onProgressUpdate(Void... values) {
          super.onProgressUpdate(values);
          h = new Handler() {
        		 Boolean stopHandler = false;
        	        public void handleMessage(Message msg) {
        	        	 switch (msg.what) {
        	        	 case HANDLER_STOP:
        	        		 stopHandler = true;
        	        		 break;
        	        	 case 0:
        	        	if (pd.getProgress() < pd.getMax()) {
        	                pd.setProgress(DBBackup.countRestoreBackUp);
        	                if (!stopHandler){
        	                h.sendEmptyMessageDelayed(0, 1500);
        	                }
        	              } else {
        	                pd.dismiss();
        	              }
        	        	break;
        	        	 }
        	        }
        	      };
        	      h.sendEmptyMessageDelayed(0, 1500);
        }

        @Override
        protected void onPostExecute(Void result) {
          super.onPostExecute(result);
          Log.e("AsyncTak","End");
          saveSharedPreferences(VAR.BACK_UP_ENDED, true);
          getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
          Log.e("Count Restore All", DBBackup.countRestoreBackUp+"");
          loadDataToLists();
          openMonthAdapter();
          Collections.sort(listOfPhoto);
        }
      }
    
    
    public Boolean isBackUpEndedSuccessfully() {
    	SharedPreferences settings = getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE);
        Boolean successfull = settings.getBoolean(VAR.BACK_UP_ENDED, true);
        return successfull;
      }
    

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
    	private static final int SWIPE_MIN_DISTANCE = 75;
    	private static final int SWIPE_MAX_OFF_PATH = 100;
    	private static final int SWIPE_THRESHOLD_VELOCITY = 50;
    	
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            if (e1.getX() - e2.getX() > DISTANCE && Math.abs(velocityX) > VELOCITY) {
                // From right to left
                clickNext();
                return false;
            } else if (e2.getX() - e1.getX() > DISTANCE && Math.abs(velocityX) > VELOCITY) {
                // from left to right
                clickPrev();
            }
            return false;
        }
        
        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

    }
    
    public void viewPagerItemRegisterForContextMenu(View view, int position){
    	registerForContextMenu(view);
    }
    
    
    
    private class ViewPagerFullImageAdapter extends PagerAdapter {

		private LayoutInflater inflater;
		ViewPagerFullImageAdapter () {
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return listOfPhoto.size();
		}
		
		 private class ViewHolder {
				
			 Button rotation;
			 ImageView fullScreenImage;
			 TextView textViewImage;
			}
		 
		 

		@Override
		public Object instantiateItem(ViewGroup view,  final int position) {
			final ViewHolder holder;
			holder = new ViewHolder();
			fullImage = inflater.inflate(R.layout.full_image, view, false);
			 if (position == 0){
			 photoObjectFullImage = listOfPhoto.get(position);}
			
           
          
           holder.fullScreenImage = (ImageView) fullImage.findViewById(R.id.imageViewFull);
           bitmap = decodeFile(position, new File(listOfPhoto.get(position).getPath()));
           holder.fullScreenImage.setImageBitmap(bitmap);

           viewPagerItemRegisterForContextMenu(holder.fullScreenImage, position);
           holder.rotation = (Button) fullImage.findViewById(R.id.rotation);
           holder.rotation.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   rotate(decodeFile(position, new File(listOfPhoto.get(position).getPath())), -90);
                   holder.fullScreenImage.setImageBitmap(bitmap);
                   try {
                       FileOutputStream out = new FileOutputStream(new File(
                    		   listOfPhoto.get(position).getPath()));
                       bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                   } catch (Exception e) {
                	   BugSenseHandler.sendException(e);
                       e.printStackTrace();
                   }
               }
           });

           holder.textViewImage = (TextView) fullImage.findViewById(R.id.textFullImage);
           long timeInMillis = Long.parseLong(listOfPhoto.get(position)
                   .getDateInMillis());
           String time = Supporting.parseDateFromLong(VAR.TEMP_dateDisplay,
                   timeInMillis);
           holder.textViewImage.setText(time);
			
			 
			view.addView(fullImage,0);
			return fullImage;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
		
	}
    
    private class ViewPagerFavoriteImageAdapter extends PagerAdapter {

		private LayoutInflater inflater;
		
		ViewPagerFavoriteImageAdapter () {
			
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return bestListOfPhoto.size();
		}
		
		 private class ViewHolder {
				
			 Button rotation;
			 ImageView fullScreenImage;
			 TextView textViewImage;
			}
		 
		 

		@Override
		public Object instantiateItem(ViewGroup view,  final int position) {
			final ViewHolder holder;
			holder = new ViewHolder();
			
			
			fullImage = inflater.inflate(R.layout.full_image, view, false);
           //showFullImage(fullImage, position, false);
			//registerForContextMenu(fullImage);
           if (position == 0){
			photoObjectFullImage = bestListOfPhoto.get(position);}
			holder.fullScreenImage = (ImageView) fullImage.findViewById(R.id.imageViewFull);
			viewPagerItemRegisterForContextMenu(holder.fullScreenImage, position);
	        bitmap = decodeFile(position, new File(bestListOfPhoto.get(position).getPath()));
	        holder.fullScreenImage.setImageBitmap(bitmap);
	        /*holder.fullScreenImage.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});*/
	       
	        holder.rotation = (Button) fullImage.findViewById(R.id.rotation);
	        holder.rotation.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View view) {
	                rotate(decodeFile(position, new File(bestListOfPhoto.get(position).getPath())), -90);
	                holder.fullScreenImage.setImageBitmap(bitmap);
	                try {
	                    FileOutputStream out = new FileOutputStream(new File(
	                            bestListOfPhoto.get(position).getPath()));
	                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	            }
	        });

	        holder.textViewImage = (TextView) fullImage.findViewById(R.id.textFullImage);
	        long timeInMillis = Long.parseLong(bestListOfPhoto.get(position)
	                .getDateInMillis());
	        String time = Supporting.parseDateFromLong(VAR.TEMP_dateDisplay,
	                timeInMillis);
	        holder.textViewImage.setText(time);			
			 
			view.addView(fullImage,0);
			return fullImage;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
		
	}
}
