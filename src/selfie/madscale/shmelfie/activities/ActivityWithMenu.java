package selfie.madscale.shmelfie.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.holoeverywhere.app.AlertDialog;


import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.models.PhotoObject;
import selfie.madscale.shmelfie.utils.Supporting;

/**
 * Sloboda Studio 2013 User: oZBo Date: 29.04.13
 */
public class ActivityWithMenu extends ActivityWithCheckData {

	public static final int DELETE_SELECTED = 201;
	public static final int DELETE_SELECTED_FAVORITE = 202;
	public static final int DELETE_ALL_PHOTOS = 203;
	public static final int DELETE_ONE_PHOTO = 204;
	public static final int DELETE_ONE_FAVORITE = 205;
	Uri uri;
	Intent share;
	ArrayList<Uri> files;
	int uriCount=0;
	 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_item, menu);
		
		return true;
	}
	
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.estimate:
			try {
				String appPackageName = getPackageName();
				Intent marketIntent = new Intent(Intent.ACTION_VIEW,
						Uri.parse("market://details?id=" + appPackageName));
				
				marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
						| Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
				startActivity(marketIntent);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return true;
		case R.id.another_app:
				Intent another_app = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Globalapps+R"));
				startActivity(another_app);
			return true;
		case R.id.share:
			String appPackageName = getPackageName();
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
			intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_description)+ " "+
					"https://play.google.com/store/apps/details?id="+ appPackageName);
			Intent chooser = Intent.createChooser(intent,
					getString(R.string.tell_about_shmelfie));
			startActivity(chooser);
			return true;
		case R.id.delete:
			alertDialogConfirm(R.string.title_delete,
					R.string.delete_all_photos, DELETE_ALL_PHOTOS);
			return true;
		case R.id.settings:
			Intent settingsActivity = new Intent(ActivityWithMenu.this,
	                SettingsActivity.class);
			startActivity(settingsActivity);
			return true;
		case R.id.favorite:
			MainActivity.whichAdapter = 4;
			MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
			return true;
		case R.id.question:
			Intent emailIntent = new Intent(Intent.ACTION_SEND);
		      emailIntent.setData(Uri.parse("mailto:"));
		      emailIntent.setType("text/plain");

		      String[] TO = {VAR.QUESTION_EMAIL};
		      emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
		      emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
		      try {
		          startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		          Log.i("Finished sending email...", "");
		       } catch (android.content.ActivityNotFoundException ex) {
		          Toast.makeText(getApplicationContext(), 
		          "There is no email client installed.", Toast.LENGTH_SHORT).show();
		       }
			return true;
		case R.id.instruction:
			Intent instruction = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com/gwt/x?wsc=fa&source=wax&u=http://androids.ru/i/shmelfie.htm&ei=woVhUei-GcSG_Ab_kYGACg&whp=3Atop"));
			startActivity(instruction);
			return true;
		case R.id.twitter:
			Intent twitter = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/apps_r"));
			startActivity(twitter);
			return true;

		case R.id.selected_delete:
			alertDialogConfirm(R.string.title_delete,
					R.string.delete_selected_photos, DELETE_SELECTED);
			return true;
		case R.id.selected_favorite:
			Supporting.moveToFavorite(getApplicationContext());
			return true;
		case R.id.selected_info:
			Supporting.showPhotoInfo(this, getSelectedPhoto());
			return true;
		case R.id.selected_rename:
			Supporting.renamePhoto(this, getSelectedPhoto(), VAR.RENAME_PHOTO);
			return true;
		case R.id.selected_send:
			shareSelected(ActivityWithMenu.this);
			return true;
		case R.id.favorite_selected_delete:
			alertDialogConfirm(R.string.title_delete,
					R.string.delete_selected_photos, DELETE_SELECTED_FAVORITE);
			return true;
		case R.id.favorite_selected_info:
			Supporting.showPhotoInfo(this, getSelectedPhotoFavorite());
			return true;
		case R.id.favorite_selected_rename:
			Supporting.renamePhoto(this, getSelectedPhotoFavorite(),
					VAR.RENAME_BEST_PHOTO);
			return true;
		case R.id.favorite_selected_send:
        	shareSelected(ActivityWithMenu.this);
			return true;
		case R.id.one_send:
			sharePhoto(ActivityWithMenu.this, MainActivity.photoObjectFullImage);
			return true;
		case R.id.one_delete:
			alertDialogConfirm(R.string.title_delete,
					R.string.delete_one_photo, DELETE_ONE_PHOTO);
			return true;
		case R.id.one_to_favorite:
			Supporting.moveToFavorite(getApplicationContext(), MainActivity.photoObjectFullImage);
			return true;
		case R.id.one_info:
			Supporting.showPhotoInfo(this, MainActivity.photoObjectFullImage);
			return true;
		case R.id.one_rename:
			Supporting.renamePhoto(this, MainActivity.photoObjectFullImage, VAR.RENAME_PHOTO);
			return true;
		case R.id.favorite_one_delete:
			alertDialogConfirm(R.string.title_delete,
					R.string.delete_selected_photos, DELETE_ONE_FAVORITE);
			return true;
		case R.id.favorite_one_info:
			Supporting.showPhotoInfo(this, MainActivity.photoObjectFullImage);
			return true;
		case R.id.favorite_one_rename:
			Supporting.renamePhoto(this, MainActivity.photoObjectFullImage,
					VAR.RENAME_BEST_PHOTO);
			return true;
		case R.id.favorite_one_send:
			sharePhoto(ActivityWithMenu.this, MainActivity.photoObjectFullImage);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	
	
	
	
	
	

	private void alertDialogConfirm(int title, int msg, final int whatDelete) {
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
    	View layoutDelete = inflater.inflate(
				R.layout.dlg_delete,
				(ViewGroup) findViewById(R.id.rl_dlgDelete_root));
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		//alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setTheme(AlertDialog.THEME_HOLO_LIGHT);
		alertDialogBuilder.setView(layoutDelete);
		 
        TextView tv_title_delete = (TextView) layoutDelete
		.findViewById(R.id.tv_title_delete);
        tv_title_delete.setText(msg);
        Button btnAskYes = (Button) layoutDelete
				.findViewById(R.id.btnDlgDelete_yes);
        Button btnAskNo = (Button) layoutDelete
				.findViewById(R.id.btnDlgDelete_no);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        btnAskYes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				alertDialog.cancel();
				switch (whatDelete) {
				case DELETE_SELECTED:
					Supporting
							.deleteSelectedPhotos(getApplicationContext());
					break;
				case DELETE_SELECTED_FAVORITE:
					Supporting
							.deleteFavoriteSelectedPhotos(getApplicationContext());
					break;
				case DELETE_ONE_FAVORITE:
					Supporting
							.deleteFavoriteSelectedPhotos(getApplicationContext(), MainActivity.photoObjectFullImage);
					break;
				case DELETE_ALL_PHOTOS:
					Supporting.deleteAll(getApplicationContext());
					break;
				case DELETE_ONE_PHOTO:
					Supporting
					.deleteSelectedPhotos(getApplicationContext(), MainActivity.photoObjectFullImage);
					break;
				}
				
			}
		});
        btnAskNo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				alertDialog.cancel();
				
			}
		});
		/*alertDialogBuilder
				.setMessage(msg)
				.setCancelable(true)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						switch (whatDelete) {
						case DELETE_SELECTED:
							Supporting
									.deleteSelectedPhotos(getApplicationContext());
							break;
						case DELETE_SELECTED_FAVORITE:
							Supporting
									.deleteFavoriteSelectedPhotos(getApplicationContext());
							break;
						case DELETE_ONE_FAVORITE:
							Supporting
									.deleteFavoriteSelectedPhotos(getApplicationContext(), MainActivity.photoObjectFullImage);
							break;
						case DELETE_ALL_PHOTOS:
							Supporting.deleteAll(getApplicationContext());
							break;
						case DELETE_ONE_PHOTO:
							Supporting
							.deleteSelectedPhotos(getApplicationContext(), MainActivity.photoObjectFullImage);
							break;
						}
					}
				})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});*/
		//AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	private PhotoObject getSelectedPhoto() {
		Iterator<PhotoObject> iter = MainActivity.tempPhotoObjects.iterator();
		while (iter.hasNext()) {
			PhotoObject photo = iter.next();
			if (photo.getState()) {
				return photo;
			}
		}
		return null;
	}

	private PhotoObject getSelectedPhotoFavorite() {
		Iterator<PhotoObject> iter = MainActivity.bestListOfPhoto.iterator();
		while (iter.hasNext()) {
			PhotoObject photo = iter.next();
			if (photo.getState()) {
				return photo;
			}
		}
		return null;
	}
	
public static ArrayList<PhotoObject> loadSelectedList(Context context) {
		
		ArrayList<PhotoObject> photoArr = new ArrayList<PhotoObject>();

		int c = MainActivity.getCurrentAdapter();

		ArrayList<PhotoObject> from;
		switch (c) {
		case 2:
			from = MainActivity.listOfPhoto;
			break;
		case 4:
			from = MainActivity.bestListOfPhoto;
			break;
		default:
			return photoArr;
		}

		for (PhotoObject t : from) {
			if (t.getState())
				photoArr.add(t);

		}
		
		
		
		return photoArr;
	}

public boolean shareSelected(Context context) {
	
	ArrayList<Uri> files = new ArrayList<Uri>();

	int c = MainActivity.getCurrentAdapter();

	ArrayList<PhotoObject> from;
	ArrayList<PhotoObject> selectedList= new ArrayList<PhotoObject>();
	switch (c) {
	case 2:
		from = MainActivity.listOfPhoto;
		break;
	case 4:
		from = MainActivity.bestListOfPhoto;
		break;
	default:
		return true;
	}

	for (PhotoObject t : from) {
		if (t.getState()){
			selectedList.add(t);
			files.add(Uri.parse("file://" + t.getPath()));
		Log.e("Selected", "yes");
		}
	}
	if (files.size()>1){
		sharePhoto(context, selectedList);
	}
	else{
		Log.e("Selected", "one");
		sharePhoto(context, selectedList.get(0));
		//shareActionSend(context, files);
	}
	
	
	return true;
}

public static void shareSelected(Context context, PhotoObject photo) {
 try
{
  List<Intent> targetedShareIntents = new ArrayList<Intent>();
  List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
  Intent share = new Intent(Intent.ACTION_SEND);
    share.setType("image/jpeg");
    Intent targetedShare  = new Intent(Intent.ACTION_SEND);
  List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(share, PackageManager.MATCH_DEFAULT_ONLY);
  if (!resInfo.isEmpty()){
    for (ResolveInfo info : resInfo) {
       Log.e("Package", info.activityInfo.packageName);
        if (info.activityInfo.packageName.toLowerCase().contains("sms") || info.activityInfo.name.toLowerCase().contains("sms")
        		|| info.activityInfo.packageName.toLowerCase().contains("mms") || info.activityInfo.name.toLowerCase().contains("mms")) {
        }
        else if (info.activityInfo.packageName.toLowerCase().contains("facebook") || info.activityInfo.name.toLowerCase().contains("facebook")){
        	Uri uri = Uri.parse("file://" + photo.getPath());
            targetedShare.putExtra(Intent.EXTRA_STREAM, uri );
        	targetedShare.setType("image/jpeg");
        	targetedShare.setPackage(info.activityInfo.packageName);
            targetedShareIntents.add(targetedShare);
            intentList.add(new LabeledIntent(targetedShare, info.activityInfo.packageName, info.loadLabel(context.getPackageManager()), info.icon));
        	
        }
        else{
        	targetedShare.setType("*/*");
        	 targetedShare.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
	         targetedShare.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.my_random_selfie_taken_via_shmelfie));
	         Uri uri = Uri.parse("file://" + photo.getPath());
	            targetedShare.putExtra(Intent.EXTRA_STREAM, uri );
	            targetedShare.setPackage(info.activityInfo.packageName);
	            targetedShareIntents.add(targetedShare);
	            intentList.add(new LabeledIntent(targetedShare, info.activityInfo.packageName, info.loadLabel(context.getPackageManager()), info.icon));
        }
    }
    LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);
    
    Intent chooserIntent = Intent.createChooser(share, context.getString(R.string.share_image));
    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
    context.startActivity(chooserIntent);
  }
  }

       catch(Exception e){
      Log.v("VM","Exception while sending image on" + " "+  e.getMessage());
    }
}

public static void shareActionSend(Context context, ArrayList<Uri> files){
try
	{
	  List<Intent> targetedShareIntents = new ArrayList<Intent>();
	  List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
	  Intent share = new Intent(Intent.ACTION_SEND);
	    share.setType("*/*");
	  List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(share, PackageManager.MATCH_DEFAULT_ONLY);
	  if (!resInfo.isEmpty()){
	    for (ResolveInfo info : resInfo) {
	        Intent targetedShare = new Intent(Intent.ACTION_SEND);
	        if (info.activityInfo.packageName.toLowerCase().contains("sms") || info.activityInfo.name.toLowerCase().contains("sms")
	        		|| info.activityInfo.packageName.toLowerCase().contains("mms") || info.activityInfo.name.toLowerCase().contains("mms")) {
	        	
	        }
	        else if (info.activityInfo.packageName.toLowerCase().contains("facebook") || info.activityInfo.name.toLowerCase().contains("facebook")){
	        	//Uri uri = Uri.parse("file://" + photo.getPath());
	            targetedShare.putExtra(Intent.EXTRA_STREAM, files.get(0) );
	        	targetedShare.setType("image/jpeg");
	        	targetedShare.setPackage(info.activityInfo.packageName);
	            targetedShareIntents.add(targetedShare);
	            intentList.add(new LabeledIntent(targetedShare, info.activityInfo.packageName, info.loadLabel(context.getPackageManager()), info.icon));
	        	
	        }
	        else{
	        	targetedShare.setType("*/*");
	        	 targetedShare.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
 	         targetedShare.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.my_random_selfie_taken_via_shmelfie));
 	         //Uri uri = Uri.parse("file://" + photo.getPath());
 	            targetedShare.putExtra(Intent.EXTRA_STREAM, files.get(0) );
 	            targetedShare.setPackage(info.activityInfo.packageName);
 	            targetedShareIntents.add(targetedShare);
 	           intentList.add(new LabeledIntent(targetedShare, info.activityInfo.packageName, info.loadLabel(context.getPackageManager()), info.icon));
	        }
	    }
	    LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);
	    
	    Intent chooserIntent = Intent.createChooser(share, context.getString(R.string.share_image));
	    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
	    context.startActivity(chooserIntent);
	  }
	  }

	       catch(Exception e){
	      Log.v("VM","Exception while sending image on" + " "+  e.getMessage());
	    }

}

public static void shareActionMultiple(Context context, ArrayList<Uri> files){
try
	{
	  List<Intent> targetedShareIntents = new ArrayList<Intent>();
	  Intent share = new Intent(Intent.ACTION_SEND_MULTIPLE);
	    share.setType("*/*");
	    List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
	  List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(share, PackageManager.MATCH_DEFAULT_ONLY);
	  if (!resInfo.isEmpty()){
	    for (ResolveInfo info : resInfo) {
	        Intent targetedShare = new Intent(Intent.ACTION_SEND_MULTIPLE);
	       
	        if (info.activityInfo.packageName.toLowerCase().contains("sms") || info.activityInfo.name.toLowerCase().contains("sms")
	        		|| info.activityInfo.packageName.toLowerCase().contains("mms") || info.activityInfo.name.toLowerCase().contains("mms")) {
	        	
	        }
	        else if (info.activityInfo.packageName.toLowerCase().contains("facebook") || info.activityInfo.name.toLowerCase().contains("facebook")){
	        	//Uri uri = Uri.parse("file://" + photo.getPath());
	        	targetedShare.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
	        	targetedShare.setType("image/jpeg");
	        	targetedShare.setPackage(info.activityInfo.packageName);
	            targetedShareIntents.add(targetedShare);
	            intentList.add(new LabeledIntent(targetedShare, info.activityInfo.packageName, info.loadLabel(context.getPackageManager()), info.icon));
	        	
	        }
	        else{
	        	targetedShare.setType("*/*");
	        	 targetedShare.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
 	         targetedShare.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.my_random_selfie_taken_via_shmelfie));
 	         //Uri uri = Uri.parse("file://" + photo.getPath());
 	        targetedShare.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
 	            targetedShare.setPackage(info.activityInfo.packageName);
 	            targetedShareIntents.add(targetedShare);
 	           intentList.add(new LabeledIntent(targetedShare, info.activityInfo.packageName, info.loadLabel(context.getPackageManager()), info.icon));
	        }
	    }
	    	LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);
	    
	    Intent chooserIntent = Intent.createChooser(share, context.getString(R.string.share_image));
	    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
	    context.startActivity(chooserIntent);
	  }
	  }

	       catch(Exception e){
	      Log.v("VM","Exception while sending image on" + " "+  e.getMessage());
	    }

}

	public void sharePhoto(final Context context, PhotoObject photo) {
		File file = new File(photo.getPath());
		   MediaScannerConnection.scanFile(context,
		           new String[] { file.toString() }, null,
		           new MediaScannerConnection.OnScanCompletedListener() {
		       public void onScanCompleted(String path, Uri uri) {
		           Log.i("ExternalStorage", "Scanned " + path + ":");
		           Log.i("ExternalStorage", "-> uri=" + uri);
		     
		     Intent intent = new Intent(Intent.ACTION_SEND);
		     intent.setType("image/*");

		     intent.putExtra(Intent.EXTRA_STREAM, uri);
		     intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
		     intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.my_random_selfie_taken_via_shmelfie));
		     intent.putExtra("sms_body", getString(R.string.my_random_selfie_taken_via_shmelfie));
		     Intent chooser = Intent.createChooser(intent, getString(R.string.share_image));
		     startActivity(chooser);
		       }
		  });
	}
	
	public void sharePhoto(final Context context, final ArrayList<PhotoObject> photoArr) {
		 String[] arrFile =new String[photoArr.size()];
		  files = new ArrayList<Uri>();
		for (int i=0;i<photoArr.size();i++){
			File file = new File(photoArr.get(i).getPath());
			arrFile[i]= file.toString();
		}
		uriCount=0;
		   MediaScannerConnection.scanFile(context,
				   arrFile, null,
		           new MediaScannerConnection.OnScanCompletedListener() {
		       public void onScanCompleted(String path, Uri uri) {
		           Log.i("ExternalStorage", "Scanned " + path + ":");
		           Log.i("ExternalStorage", "-> uri=" + uri);
		           files.add(uri);
		           uriCount++;
		           if (uriCount == photoArr.size()){
		           Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
				     intent.setType("image/*");

				     intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
				     intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
				     intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.my_random_selfie_taken_via_shmelfie));
				     intent.putExtra("sms_body", getString(R.string.my_random_selfie_taken_via_shmelfie));
				     Intent chooser = Intent.createChooser(intent, getString(R.string.share_image));
				     startActivity(chooser);
		           }
		       }
			  });
		   
		     
	}
}
