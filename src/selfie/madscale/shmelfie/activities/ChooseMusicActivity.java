package selfie.madscale.shmelfie.activities;

import java.io.File;
import java.util.ArrayList;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.adapters.MusicExplorerAdapter;
import selfie.madscale.shmelfie.models.FileObject;
import selfie.madscale.shmelfie.snippets.MediaSnippet;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class ChooseMusicActivity extends Activity {
	
	private ArrayList<FileObject> mFileObject;
    private String LAST_PATH;
    private ListView musiclist;
    private String MEDIA_PATH = Environment.getExternalStorageDirectory().getPath() + "/";
    private int FOLDER_LEANTH = Environment.getExternalStorageDirectory().getName().length();
    private String END_PATH;
    private MediaSnippet mSnippet;
    private MusicExplorerAdapter adapter;
    
    private String LAST_SONG_NAME = "none";
    
    public static SharedPreferences settings;
    String lastFolder;
    
    Cursor musiccursor;
    int music_column_index;
    int count;
    MediaPlayer mMediaPlayer;

    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	//BugSenseHandler.closeSession(ChooseMusicActivity.this);
    }
    
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //BugSenseHandler.initAndStartSession(ChooseMusicActivity.this, "d5a0b647");
        setContentView(R.layout.music_chooser);
        END_PATH = MEDIA_PATH.substring(0, MEDIA_PATH.length()-(FOLDER_LEANTH+1));
        Log.d("Lenth",END_PATH);
        Log.d("Lenth",Environment.getExternalStorageDirectory().getName());
        //init_phone_music_grid();
        settings = getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE);
        mSnippet = new MediaSnippet();
        mFileObject = mSnippet.getFileList(MEDIA_PATH);
        LAST_PATH = MEDIA_PATH;
        adapter = new MusicExplorerAdapter(this, mFileObject);
        mMediaPlayer = new MediaPlayer();
        ListView lvMusicList = (ListView)findViewById(R.id.PhoneMusicList);
        lvMusicList.setAdapter(adapter);
        lastFolder = loadLastFolder();
        if (!lastFolder.equals("")){
        	Log.d("Loaded Folder", lastFolder);
        	adapter.setNewCatalog(mSnippet.getFileList(lastFolder));
        }
        lvMusicList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				 if(((MusicExplorerAdapter) parent.getAdapter()).getItem(position).isDirectory){
	                    LAST_PATH = ((MusicExplorerAdapter) parent.getAdapter()).getItem(position).getParent();
	                    Log.d("Last Path",LAST_PATH);
	                    saveSharedPreferences(VAR.LAST_FOLDER, LAST_PATH);
	                    adapter.setNewCatalog(mSnippet.getFileList(
	                            ((MusicExplorerAdapter) parent.getAdapter()).getItem(position).getPath()
	                    ));
	                }else{
	                	String filename =  ((MusicExplorerAdapter) parent.getAdapter()).getItem(position).getPath();
	                	if(((MusicExplorerAdapter) parent.getAdapter()).getItem(position).getName().equals(LAST_SONG_NAME)){
	                		saveSharedPreferences(VAR.MUSIC_PATH, filename);
	                		if (mMediaPlayer.isPlaying()) {
	                            mMediaPlayer.reset();
	                        }
	                		ChooseMusicActivity.this.finish();
	                	}else{
	                		LAST_SONG_NAME = ((MusicExplorerAdapter) parent.getAdapter()).getItem(position).getName();
	                		try {
		                        if (mMediaPlayer.isPlaying()) {
		                            mMediaPlayer.reset();
		                        }
		                        mMediaPlayer.setDataSource(filename);
		                        mMediaPlayer.prepare();
		                        mMediaPlayer.start();
		                    } catch (Exception e) {
		                    	Toast.makeText(ChooseMusicActivity.this, e.toString(), Toast.LENGTH_LONG).show();
		                    }
	                	}
	                    
	                }
	            }
		});
    }
    
    @Override
    public void onBackPressed() {
    	if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.reset();
        }
    	if(LAST_PATH.concat("/").equals(END_PATH)){
            super.onBackPressed();
        }else{
        	if (!lastFolder.equals("")){
        		File file = new File(lastFolder);
        		lastFolder= file.getParent();
        	}
            ArrayList<FileObject> mBack = mSnippet.getFileList(LAST_PATH);
            adapter.setNewCatalog(mBack);
            String tmp = mSnippet.getParentOfFile(mBack.get(0).getParent());
            LAST_PATH = tmp;
        }
    }

    private void init_phone_music_grid() {
        System.gc();
        String[] proj = { MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Video.Media.SIZE };
        musiccursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                proj, null, null, null);
        count = musiccursor.getCount();
        musiclist = (ListView) findViewById(R.id.PhoneMusicList);
        musiclist.setAdapter(new MusicAdapter(getApplicationContext()));

        musiclist.setOnItemClickListener(musicgridlistener);
        mMediaPlayer = new MediaPlayer();
    }

    private OnItemClickListener musicgridlistener = new OnItemClickListener() {
        public void onItemClick(AdapterView parent, View v, int position,
                                long id) {
            System.gc();
            music_column_index = musiccursor
                    .getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            musiccursor.moveToPosition(position);
            String filename = musiccursor.getString(music_column_index);

            try {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.reset();
                }
                mMediaPlayer.setDataSource(filename);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            } catch (Exception e) {

            }
        }
    };

    public class MusicAdapter extends BaseAdapter {
        private Context mContext;

        public MusicAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return count;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            System.gc();
            TextView tv = new TextView(mContext.getApplicationContext());
            String id = null;
            if (convertView == null) {
                music_column_index = musiccursor
                        .getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME);
                musiccursor.moveToPosition(position);
                id = musiccursor.getString(music_column_index);
                music_column_index = musiccursor
                        .getColumnIndexOrThrow(MediaStore.Audio.Media.SIZE);
                musiccursor.moveToPosition(position);
                id += " Size(KB):" + musiccursor.getString(music_column_index);
                tv.setText(id);
            } else
                tv = (TextView) convertView;
            return tv;
        }
    }
    
    public void saveSharedPreferences(String key, String value) {

        SharedPreferences settings = getSharedPreferences(VAR.SETTINGS,
                MODE_PRIVATE);
        SharedPreferences.Editor ed = settings.edit();
        ed.putString(key, value);
        ed.commit();
    }
    
    public String loadLastFolder() {
    	SharedPreferences settings = getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE);
        String lastFolder = settings.getString(VAR.LAST_FOLDER, "");
        return lastFolder;
      }
}
