package selfie.madscale.shmelfie.activities;




import org.holoeverywhere.app.Activity;

import selfie.madscale.shmelfie.R;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;


public class ActivityWithCheckData extends Activity {

    private void checksd(Context _c) {
        if (_c.getExternalCacheDir() == null || !_c.getExternalCacheDir().canWrite()) {
            Toast.makeText(_c,
					getString(R.string.data_folder_are_not_avalible_check_sdcard),
					Toast.LENGTH_LONG).show();
            finish();
        }
	}
	@Override
	protected void onStart() {
		super.onStart();
		checksd(getApplicationContext());
	}
	
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		checksd(getApplicationContext());
	}

}
