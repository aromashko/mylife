package selfie.madscale.shmelfie.snippets;

import java.io.File;
import java.util.ArrayList;

import selfie.madscale.shmelfie.models.FileObject;




public class MediaSnippet {
    private String mp3Pattern = ".mp3";

    public MediaSnippet() {

    }

    /**
     * Function to read all mp3 files and store the details in
     * ArrayList
     * */
    public ArrayList<FileObject> getFileList(String mediaPath) {
        ArrayList<FileObject> mFileObject = new ArrayList<FileObject>();
        if (mediaPath != null) {
            File home = new File(mediaPath);
            File[] listFiles = home.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File file : listFiles) {
                    System.out.println(file.getAbsolutePath());
                    FileObject mFile = new FileObject();

                        if (file.isDirectory()) {
                            mFile.setParent(file.getParent());
                            //Log.e("File Parent", mFile.getParent() );
                            mFile.setName(file.getName());
                            mFile.setPath(file.getPath());
                            mFile.setDirectory(true);
                            mFileObject.add(mFile);
                        } else {
                            if(file.getName().endsWith(mp3Pattern)){
                                mFile.setParent(file.getParent());
                                //Log.e("File Parent", mFile.getParent() );
                                mFile.setName(file.getName());
                                mFile.setPath(file.getPath());
                                mFile.setDirectory(false);
                                mFileObject.add(mFile);
                            }

                        }

                }
            }
        }
        return mFileObject;
    }

    public String getParentOfFile(String url){
        File home = null;
        if(url!=null){
            home = new File(url);
        }
        return home.getParent();
    }
}
