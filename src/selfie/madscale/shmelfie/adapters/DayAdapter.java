package selfie.madscale.shmelfie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.models.DayObject;

/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 18.04.13
 */
public class DayAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<DayObject> days;
    private static final String LOG_TAG = "DayAdapter";

    public DayAdapter(Context mContext, ArrayList<DayObject> days) {
        this.mContext = mContext;
        this.days = days;
    }

    @Override
    public int getCount() {
        return days.size();
    }

    @Override
    public Object getItem(int position) {
        return days.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        if (convertView == null) {
            grid = new View(mContext);
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.grid_view_day_month, parent, false);
        } else {
            grid = (View) convertView;
        }
        TextView whichDay = (TextView) grid.findViewById(R.id.numberOnImage);
        whichDay.setText("" + days.get(position).getDayInMonth());
        return grid;
    }
}
