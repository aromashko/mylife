package selfie.madscale.shmelfie.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;


import java.io.File;
import java.util.ArrayList;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.models.PhotoObject;
import selfie.madscale.shmelfie.utils.Supporting;

/**
 * Sloboda Studio 2013 User: oZBo Date: 05.04.13
 */
public class ImageAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<PhotoObject> objects;
	private static final String LOG_TAG = "ImageAdapter";

	public ImageAdapter(Context context, ArrayList<PhotoObject> products) {
		// super(context, R.layout.grid_view_image);
		objects = products;
		mContext = context;
	}

	public int getCount() {
		return objects.size();
	}

	public PhotoObject getItem(int position) {
		return objects.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

    public void setNewList(ArrayList<PhotoObject> products){
        objects.clear();
        objects.addAll(products);
        notifyDataSetChanged();
    }
	
/*
	public static Bitmap createScaledBitmap(Bitmap unscaledBitmap, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
		Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);
		Rect dstRect = calculateDstRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);
		Bitmap scaledBitmap = Bitmap.createBitmap(dstRect.width(), dstRect.height(), Config.ARGB_8888);
		Canvas canvas = new Canvas(scaledBitmap);
		canvas.drawBitmap(unscaledBitmap, srcRect, dstRect, new Paint(Paint.FILTER_BITMAP_FLAG));
		return scaledBitmap;
		}*/

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View grid;
		if (convertView == null) {
			grid = new View(mContext);
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			grid = inflater.inflate(R.layout.grid_view_image, parent, false);
		} else {
			grid = (View) convertView;
		}

		ImageView imageView = (ImageView) grid.findViewById(R.id.icon_image);
		TextView textView = (TextView) grid.findViewById(R.id.icon_text);
		ImageView checkBox = (ImageView) grid.findViewById(R.id.icon_check);

		long timeInMillis = Long.parseLong(objects.get(position)
				.getDateInMillis());
		String time = Supporting.parseDateFromLong(VAR.TEMP_dateDisplay,
				timeInMillis);
		Log.i(LOG_TAG, "timeInMillis long: " + timeInMillis);
		// textView.setTextColor(R.color.black);
		textView.setTextColor(parent.getResources().getColor(R.color.white));
		textView.setText(time);

		if (objects.size() != 0) {
			File imgFile = new File(objects.get(position).getPath());
			if (imgFile.exists()) {
				//Log.e("Path to file", imgFile.getPath());
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 1;
				Bitmap tBitmap = BitmapFactory.decodeFile(objects.get(position)
						.getPath(), options);
					if (tBitmap != null){
				double w = tBitmap.getWidth();
				double h = tBitmap.getHeight();
				double aspectRatio;
				if (w >= h) {
					aspectRatio = w / h;
					w = 80;
					h = 80 / aspectRatio;
				} else {
					aspectRatio = h / w;
					w = 80 / aspectRatio;
					h = 80;
				}
				Bitmap previewBitmap = Bitmap.createScaledBitmap(tBitmap,
						(int) w, (int) h, false);
				imageView.setImageBitmap(previewBitmap);
				imageView.setScaleType(ScaleType.CENTER_INSIDE);
					}
				
			}
		}

		if (objects.size() != 0) {
			if (objects.get(position).getState()) {
				checkBox.setVisibility(ImageView.VISIBLE);
			} else {
				checkBox.setVisibility(ImageView.GONE);
			}
		}

		return grid;
	}
}
