package selfie.madscale.shmelfie.adapters;

import java.util.ArrayList;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.models.MonthObject;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 15.04.13
 */
public class MonthAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<MonthObject> monthes;
    private static final String LOG_TAG = "MonthAdapter";

    public MonthAdapter(Context mContext, ArrayList<MonthObject> monthes) {
        this.mContext = mContext;
        this.monthes = monthes;
    }

    @Override
    public int getCount() {
        return monthes.size();
    }

    @Override
    public Object getItem(int position) {
        return monthes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;

        if (convertView == null) {
            grid = new View(mContext);
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.grid_view_day_month, parent, false);
        } else {
            grid = (View) convertView;
        }
        TextView whichDay = (TextView) grid.findViewById(R.id.numberOnImage);
        whichDay.setText("" + monthes.get(position).getMonthInYear());
        return grid;
    }

}
