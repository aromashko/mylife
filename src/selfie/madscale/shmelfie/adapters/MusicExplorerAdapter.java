package selfie.madscale.shmelfie.adapters;

import java.util.ArrayList;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.interfaces.MediaSnippetInterface;
import selfie.madscale.shmelfie.models.FileObject;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MusicExplorerAdapter extends BaseAdapter implements MediaSnippetInterface{
	
	private Context mContext;
	private ArrayList<FileObject> mFileList;
	private LayoutInflater mInflater;
	
	public MusicExplorerAdapter(Context context, ArrayList<FileObject> fileList){
		this.mContext = context;
		this.mFileList = fileList;
		this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mFileList.size();
	}

	@Override
	public FileObject getItem(int position) {
		return mFileList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = mInflater.inflate(R.layout.music_row, parent, false);
		((TextView)convertView.findViewById(R.id.textName)).setText(mFileList.get(position).getName());
		ImageView img = (ImageView)convertView.findViewById(R.id.imageForder);
			if(mFileList.get(position).isDirectory()){
				img.setBackgroundResource(R.drawable.explorer_folder);
			}else{
				img.setBackgroundResource(R.drawable.explorer_file);
			}
		return convertView;
	}

	@Override
	public void setNewCatalog(ArrayList<FileObject> catalog) {
		mFileList = catalog;
		notifyDataSetChanged();
		if (catalog.size()>0){
		 String lastFolder = catalog.get(0).getParent();
         Log.d("Last Folder",lastFolder);
         saveSharedPreferences(VAR.LAST_FOLDER, lastFolder);
		}
	}
	
	public void saveSharedPreferences(String key, String value) {

        SharedPreferences settings = mContext.getSharedPreferences(VAR.SETTINGS,
        		Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = settings.edit();
        ed.putString(key, value);
        ed.commit();
    }

}
