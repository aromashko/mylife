package selfie.madscale.shmelfie.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.TextView;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.activities.MainActivity;
import selfie.madscale.shmelfie.database.DataManager;
import selfie.madscale.shmelfie.models.PhotoObject;
import selfie.madscale.shmelfie.service.CameraService;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 01.04.13
 */
public class Supporting {

    private static Toast mToast;
    public static final String LOG_TAG = "supporting";
    public static SharedPreferences settings;

    /**
     * @return true, if device have front camera.
     */
    public static boolean getFrontCamera() {
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) return true;
        }
        return false;
    }

    /**
     * Set frequency for making photo in "auto per day" mode
     *
     * @param int timesPerDay
     */
    public static void frequencyForMakePhoto(int timesPerDay) {
        CameraService.periodForAutoShot = VAR.MINUTES_IN_DAY / timesPerDay * VAR.SECONDS_IN_MIN * VAR.MILLIS_IN_SEC;
    }

    /**
     * Make toast with specified message
     *
     * @param Context ctx
     * @param String  msg
     */
    public static void showToast(Context ctx, String msg) {
        if (mToast == null) {
            mToast = Toast.makeText(ctx, msg, Toast.LENGTH_SHORT);
        }
        if (!mToast.getView().isShown()) {
            mToast.setText(msg);
            mToast.show();
        }
    }

    /**
     * Get date to the specified format
     *
     * @param String form
     * @return String
     */
    public static String getCurrentDate(String form) {
        android.text.format.DateFormat df = new android.text.format.DateFormat();
        return (String) DateFormat.format(form, new java.util.Date());
    }

    /**
     * Get date from millisseconds
     *
     * @param form         string
     * @param timeInMillis long
     * @return String
     */
    public static String parseDateFromLong(String form, long timeInMillis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(form, Locale.getDefault());
        return dateFormat.format(timeInMillis);
    }

    /**
     * Get current time in milliseconds
     *
     * @return String
     */
    public static String getTimeInMillis() {
        return String.valueOf(System.currentTimeMillis());
    }

    /**
     * @param context
     * @return true if device have light sensor
     */
    public static boolean haveLightSensor(Context context) {
        SensorManager mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        return mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null;
    }

    public static void isPrevYearExist() {
        VAR.currentYear--;
        //TODO make algorithm for prev/next year open
    }

    public static void isNextYearExist() {
        VAR.currentYear++;
    }

    public static void getNumberOfMonthes(int whichYear) {
        for (PhotoObject photo : MainActivity.listOfPhoto) {
            int monthInYear = photo.getMonthInYear();
            int year = photo.getYear();
            for (int i = 1; i < VAR.MONTHES; i++) {
                if (i == monthInYear && year == whichYear) {
                    VAR.whichMonthHavePhoto[i] = true;
                }
            }
        }
    }

    public static void getNumberOfDays(int whichMonth) {
        for (PhotoObject photo : MainActivity.listOfPhoto) {
            int dayInMonth = photo.getDayInMonth();
            int monthInYear = photo.getMonthInYear();
            for (int i = 1; i < VAR.DAYS; i++) {
                if (i == dayInMonth && monthInYear == whichMonth && photo.getYear() == VAR.currentYear) {
                    VAR.whichDaysHavePhoto[i] = true;
                    //Log.e("whichDaysHavePhoto", i+"");
                }
            }
        }
    }

    public static void getImagesOfDay(int whichDay, int whichMonth) {
        for (PhotoObject photo : MainActivity.listOfPhoto) {
            int dayInMonth = photo.getDayInMonth();
            int monthInYear = photo.getMonthInYear();
            if (dayInMonth == whichDay && whichMonth == monthInYear && photo.getYear() == VAR.currentYear)
                MainActivity.tempPhotoObjects.add(photo);
        }
    }

    public static void getPhotosInYear(int whichYear) {
        MainActivity.tempPhotoObjects.clear();
        for (PhotoObject photo : MainActivity.listOfPhoto) {
            int year = photo.getYear();
            if (year == whichYear) {
                MainActivity.tempPhotoObjects.add(photo);
            }
        }
    }

    public static void getPhotosInMonth(int whichMonth) {
        MainActivity.tempPhotoObjects.clear();
        for (PhotoObject photo : MainActivity.listOfPhoto) {
            int monthInYear = photo.getMonthInYear();
            if (monthInYear == whichMonth) {
                MainActivity.tempPhotoObjects.add(photo);
            }
        }
    }

    public static void deleteMonth(Context ctx, int year, int month) {
        Iterator<PhotoObject> iter = MainActivity.listOfPhoto.iterator();
        while (iter.hasNext()) {
            PhotoObject photo = iter.next();
            if (photo.getYear() == year && photo.getMonthInYear() == month) {
                DataManager.deletePhoto(ctx, photo.getId());
                File forDelete = new File(photo.getPath());
                forDelete.delete();
                iter.remove();
            }
        }
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
    }

    public static void deleteSelectedPhotos(Context ctx) {
        Iterator<PhotoObject> iter = MainActivity.tempPhotoObjects.iterator();
        while (iter.hasNext()) {
            PhotoObject photo = iter.next();
            if (photo.getState()) {
                DataManager.deletePhoto(ctx, photo.getId());
                File forDelete = new File(photo.getPath());
                Log.d("photo path", photo.getPath());
                boolean deleted = forDelete.delete();
                Log.d("Deleted", deleted+"");
                iter.remove();
            }
        }
        settings = ctx.getSharedPreferences(VAR.SETTINGS, Context.MODE_PRIVATE);
        String folder_path = settings.getString("path", ctx.getString(R.string.app_default_folder));
        //Log.e("Path folder", "file://" +  Environment.getExternalStorageDirectory()+"/"+ folder_path);
        ctx.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
       		 Uri.parse("file://" +  Environment.getExternalStorageDirectory()+"/"+ folder_path)));
        MainActivity.listOfPhoto = DataManager.getPhotoList(ctx);
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
        Log.i(LOG_TAG, "" + MainActivity.tempPhotoObjects.size());
    }
    
    public static void deleteSelectedPhotos(Context ctx, PhotoObject photo) {
                DataManager.deletePhoto(ctx, photo.getId());
                File forDelete = new File(photo.getPath());
                Log.d("photo path", photo.getPath());
                boolean deleted = forDelete.delete();
                Log.d("Deleted", deleted+"");
                settings = ctx.getSharedPreferences(VAR.SETTINGS, Context.MODE_PRIVATE);
                String folder_path = settings.getString("path", ctx.getString(R.string.app_default_folder));
                //Log.e("Path folder", "file://" +  Environment.getExternalStorageDirectory()+"/"+ folder_path);
                ctx.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
               		 Uri.parse("file://" +  Environment.getExternalStorageDirectory()+"/"+ folder_path)));
               
        MainActivity.listOfPhoto = DataManager.getPhotoList(ctx);
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
        Log.i(LOG_TAG, "" + MainActivity.listOfPhoto.size());
    }

    public static void deleteFavoriteSelectedPhotos(Context ctx) {
        Iterator<PhotoObject> iter = MainActivity.bestListOfPhoto.iterator();
        while (iter.hasNext()) {
            PhotoObject photo = iter.next();
            if (photo.getState()) {
                DataManager.deleteBestPhoto(ctx, photo.getId());
                File forDelete = new File(photo.getPath());
                forDelete.delete();
                iter.remove();
            }
        }
        settings = ctx.getSharedPreferences(VAR.SETTINGS, Context.MODE_PRIVATE);
        String folder_path = settings.getString("path", ctx.getString(R.string.app_default_folder));
        ctx.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
       		 Uri.parse("file://" +  Environment.getExternalStorageDirectory()+"/"+ folder_path)));
        MainActivity.bestListOfPhoto = DataManager.getBestPhotoList(ctx);
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
        Log.i(LOG_TAG, "" + MainActivity.tempPhotoObjects.size());
    }
    
    public static void deleteFavoriteSelectedPhotos(Context ctx, PhotoObject photo) {
        
                DataManager.deleteBestPhoto(ctx, photo.getId());
                File forDelete = new File(photo.getPath());
                forDelete.delete();
                settings = ctx.getSharedPreferences(VAR.SETTINGS, Context.MODE_PRIVATE);
                String folder_path = settings.getString("path", ctx.getString(R.string.app_default_folder));
                ctx.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
               		 Uri.parse("file://" +  Environment.getExternalStorageDirectory()+"/"+ folder_path)));
        MainActivity.bestListOfPhoto = DataManager.getBestPhotoList(ctx);
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
        Log.i(LOG_TAG, "" + MainActivity.tempPhotoObjects.size());
    }

    public static void moveToFavorite(Context ctx) {
        Iterator<PhotoObject> iter = MainActivity.tempPhotoObjects.iterator();
        while (iter.hasNext()) {
            PhotoObject photo = iter.next();
            if (photo.getState()) {
                DataManager.insertBestPhoto(ctx, photo);
                DataManager.deletePhoto(ctx, photo.getId());
                iter.remove();
            }
        }
        MainActivity.bestListOfPhoto = DataManager.getBestPhotoList(ctx);
        MainActivity.listOfPhoto = DataManager.getPhotoList(ctx);
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
    }
    
    public static void moveToFavorite(Context ctx, PhotoObject photo) {
                DataManager.insertBestPhoto(ctx, photo);
                DataManager.deletePhoto(ctx, photo.getId());
            
        MainActivity.bestListOfPhoto = DataManager.getBestPhotoList(ctx);
        MainActivity.listOfPhoto = DataManager.getPhotoList(ctx);
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
    }
    public static void deleteYear(Context ctx, int year) {
        Iterator<PhotoObject> iter = MainActivity.listOfPhoto.iterator();
        while (iter.hasNext()) {
            PhotoObject photo = iter.next();
            if (photo.getYear() == year) {
                DataManager.deletePhoto(ctx, photo.getId());
                File forDelete = new File(photo.getPath());
                forDelete.delete();
                iter.remove();
            }
        }
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
    }

    public static void deleteDay(Context ctx, int year, int month, int day) {
        Iterator<PhotoObject> iter = MainActivity.listOfPhoto.iterator();
        while (iter.hasNext()) {
            PhotoObject photo = iter.next();
            if (photo.getYear() == year && photo.getMonthInYear() == month && photo.getDayInMonth() == day) {
                DataManager.deletePhoto(ctx, photo.getId());
                File forDelete = new File(photo.getPath());
                forDelete.delete();
                iter.remove();
            }
        }
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
    }

    public static void deleteAll(Context ctx) {
        Iterator<PhotoObject> iter = MainActivity.listOfPhoto.iterator();
        while (iter.hasNext()) {
            PhotoObject photo = iter.next();
            File forDelete = new File(photo.getPath());
            forDelete.delete();
            iter.remove();
        }
        Iterator<PhotoObject> iterFavorites = MainActivity.bestListOfPhoto.iterator();
        while (iterFavorites.hasNext()) {
            PhotoObject photo = iterFavorites.next();
            File forDelete = new File(photo.getPath());
            forDelete.delete();
            iterFavorites.remove();
        }
        DataManager.deleteAllPhotos(ctx);
        MainActivity.adapterHandler.sendEmptyMessage(VAR.UPDATE_ADAPTER);
    }

    public static void showPhotoInfo(Context ctx, PhotoObject photo) {
    	LayoutInflater inflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	View layoutPhotoInfo = inflater.inflate(
				R.layout.photo_info, null);
    	TextView tv_name = (TextView) layoutPhotoInfo
				.findViewById(R.id.tv_name);
    	TextView tv_path = (TextView) layoutPhotoInfo
				.findViewById(R.id.tv_path);
    	TextView tv_size = (TextView) layoutPhotoInfo
				.findViewById(R.id.tv_size);
        String newLine = "\n";
        File file = new File(photo.getPath());
        String name = ctx.getString(R.string.name) + newLine + photo.getName();
        String path = ctx.getString(R.string.path) + newLine + photo.getPath();
        String size = ctx.getString(R.string.size) + newLine + file.length() + ctx.getString(R.string._byte);
        tv_name.setText(name);
        tv_path.setText(path);
        tv_size.setText(size);
        

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTheme(AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle(R.string.alert_dialog_information_title)
                .setView(layoutPhotoInfo)
                .setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create().show();
    }

    public static void renamePhoto(final Context ctx, final PhotoObject photo, final int whichPhotoRename) {
        AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
        alert.setTitle(R.string.alert_dialog_rename_title);
        alert.setMessage(R.string.alert_dialog_rename_message);
        alert.setTheme(AlertDialog.THEME_HOLO_DARK);
        final EditText input = new EditText(ctx);
        alert.setView(input);
        input.setText(photo.getName());
        input.setTextColor(Color.WHITE);
        alert.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {

                String inputName = input.getText().toString();
                File oldFile = new File(photo.getPath());
                File newFile = new File(VAR.PATH + inputName + VAR.IMAGE_FORMAT);
                oldFile.renameTo(newFile);
                photo.setName(inputName);
                photo.setPath(String.valueOf(newFile));
                switch (whichPhotoRename) {
                    case VAR.RENAME_PHOTO:
                        DataManager.updatePhoto(ctx, photo);
                        break;
                    case VAR.RENAME_BEST_PHOTO:
                        DataManager.deletePhoto(ctx, photo.getId());
                        DataManager.insertBestPhoto(ctx, photo);
//                        DataManager.updateBestPhoto(ctx, photo);
                        break;
                }
            }
        });
        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        alert.show();
    }
    
    public static boolean shareSelectedMultiple(Context context) {
		
		ArrayList<Uri> files = new ArrayList<Uri>();

		int c = MainActivity.getCurrentAdapter();

		ArrayList<PhotoObject> from;
		switch (c) {
		case 2:
			from = MainActivity.listOfPhoto;
			break;
		case 4:
			from = MainActivity.bestListOfPhoto;
			break;
		default:
			return true;
		}

		for (PhotoObject t : from) {
			if (t.getState())
				files.add(Uri.parse("file://" + t.getPath()));
			/*FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
        	ArrayList<PhotoObject> arrPhoto = new ArrayList<PhotoObject>();
        	arrPhoto.add(photoObjectFullImage);
        	ShareDialog shareDialog = ShareDialog.newInstance(MainActivity.this, arrPhoto);
        	shareDialog.show(fm, "shareDialog");*/
			
		}
		if (files.size()>1){
		shareActionMultiple(context, files);
		}
		else{
			shareActionSend(context, from.get(0));
		}
		
		
		return true;
	}

public static void shareSelected(Context context, PhotoObject photo) {
	 try
	{
	  List<Intent> targetedShareIntents = new ArrayList<Intent>();
	  Intent share = new Intent(Intent.ACTION_SEND);
	    share.setType("*/*");
	  List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(share, PackageManager.MATCH_DEFAULT_ONLY);
	  if (!resInfo.isEmpty()){
	    for (ResolveInfo info : resInfo) {
	        Intent targetedShare = new Intent(Intent.ACTION_SEND);
	        //targetedShare.setType("*/*"); // put here your mime type
	        if (info.activityInfo.packageName.toLowerCase().contains("sms") || info.activityInfo.name.toLowerCase().contains("sms")
	        		|| info.activityInfo.packageName.toLowerCase().contains("mms") || info.activityInfo.name.toLowerCase().contains("mms")) {
	        	
	        	targetedShare.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.my_random_selfie_taken_via_shmelfie));
	        	targetedShare.putExtra("sms_body",context.getString(R.string.my_random_selfie_taken_via_shmelfie));
	        	Uri uri = Uri.parse("file://" + photo.getPath());
	            targetedShare.putExtra(Intent.EXTRA_STREAM, uri );
	        	targetedShare.setType("image/jpeg");
	        	
	            targetedShare.setPackage(info.activityInfo.packageName);
	            targetedShareIntents.add(targetedShare);
	        }
	        else if (info.activityInfo.packageName.toLowerCase().contains("facebook") || info.activityInfo.name.toLowerCase().contains("facebook")){
	        	Uri uri = Uri.parse("file://" + photo.getPath());
	            targetedShare.putExtra(Intent.EXTRA_STREAM, uri );
	        	targetedShare.setType("image/jpeg");
	        	targetedShare.setPackage(info.activityInfo.packageName);
	            targetedShareIntents.add(targetedShare);
	            
	        	
	        }
	        else{
	        	targetedShare.setType("*/*");
	        	 targetedShare.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
    	         targetedShare.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.my_random_selfie_taken_via_shmelfie));
    	         Uri uri = Uri.parse("file://" + photo.getPath());
    	            targetedShare.putExtra(Intent.EXTRA_STREAM, uri );
    	            targetedShare.setPackage(info.activityInfo.packageName);
    	            targetedShareIntents.add(targetedShare);
	        }
	    }
	    Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), context.getString(R.string.share_image));
	    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
	    context. startActivity(chooserIntent);
	  }
	  }

	       catch(Exception e){
	      Log.v("VM","Exception while sending image on" + " "+  e.getMessage());
	    }
	}
	
public static void shareActionSend(Context context, PhotoObject photo){
	try
 	{
 	  List<Intent> targetedShareIntents = new ArrayList<Intent>();
 	 List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
 	 Intent targetedShare = new Intent(Intent.ACTION_SEND);
 	targetedShare.setType("image/jpeg");
 	  Intent share = new Intent(Intent.ACTION_SEND);
 	    share.setType("*/*");
 	  List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(share, PackageManager.MATCH_DEFAULT_ONLY);
 	 if (!resInfo.isEmpty()){
  	    for (ResolveInfo info : resInfo) {
  	       
  	        String package_name =info.activityInfo.packageName;
  	        String activity_name= info.activityInfo.name;
  	        Log.e("Package", package_name);
  	        
  	       //фильтрация остального
  	        if (package_name.toLowerCase().contains("mms") || activity_name.toLowerCase().contains("mms")
  	        		|| package_name.toLowerCase().contains("facebook") || activity_name.toLowerCase().contains("facebook")
  	        		|| package_name.toLowerCase().contains("twitter") || activity_name.toLowerCase().contains("twitter")
  	        		|| package_name.toLowerCase().contains("instagram") || activity_name.toLowerCase().contains("instagram")
  	        		) {
  	        	continue;
  	        }

  	        else{
  	        	targetedShare.setType("*/*");
  	        	targetedShare.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
      	        targetedShare.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.my_random_selfie_taken_via_shmelfie));
      	      Uri uri = Uri.parse("file://" + photo.getPath());
      	        targetedShare.putExtra(Intent.EXTRA_STREAM, uri);
      	        targetedShare.setPackage(package_name);
      	       intentList.add(new LabeledIntent(targetedShare, package_name, info.loadLabel(context.getPackageManager()), info.icon));
  	        }
  	    }
  	   LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);
 	    
 	    Intent chooserIntent = Intent.createChooser(targetedShare, context.getString(R.string.share_image));
 	    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
 	    context.startActivity(chooserIntent);
  	  }
 	  }

 	       catch(Exception e){
 	      Log.v("VM","Exception while sending image on" + " "+  e.getMessage());
 	    }

}

public static void shareActionMultiple(Context context, ArrayList<Uri> files){
	try
 	{
 	  List<Intent> targetedShareIntents = new ArrayList<Intent>();
 	 List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
 	  Intent share = new Intent(Intent.ACTION_SEND_MULTIPLE);
 	 Intent targetedShare = new Intent(Intent.ACTION_SEND_MULTIPLE);
 	    share.setType("*/*");
 	  List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(share, PackageManager.MATCH_DEFAULT_ONLY);
 	  if (!resInfo.isEmpty()){
 	    for (ResolveInfo info : resInfo) {
 	      
 	        String package_name =info.activityInfo.packageName;
 	        String activity_name= info.activityInfo.name;
 	       //фильтрация остального
 	        if (package_name.toLowerCase().contains("mms") || activity_name.toLowerCase().contains("mms")
 	        		|| package_name.toLowerCase().contains("facebook") || activity_name.toLowerCase().contains("facebook")
 	        		|| package_name.toLowerCase().contains("twitter") || activity_name.toLowerCase().contains("twitter")
 	        		|| package_name.toLowerCase().contains("instagram") || activity_name.toLowerCase().contains("instagram")
 	        		) {
 	        	continue;
 	        }
 	        else{
 	        	targetedShare.setType("*/*");
 	        	targetedShare.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
     	        targetedShare.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.my_random_selfie_taken_via_shmelfie));
     	        targetedShare.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
     	        targetedShare.setPackage(package_name);
     	       intentList.add(new LabeledIntent(targetedShare, package_name, info.loadLabel(context.getPackageManager()), info.icon));
 	        }
 	    }
 	   LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);
	    
	    Intent chooserIntent = Intent.createChooser(targetedShare, context.getString(R.string.share_image));
	    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
	    context.startActivity(chooserIntent);
 	  }
 	  }

 	       catch(Exception e){
 	      Log.v("VM","Exception while sending image on" + " "+  e.getMessage());
 	    }

}
}

