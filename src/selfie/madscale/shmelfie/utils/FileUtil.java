package selfie.madscale.shmelfie.utils;

import android.graphics.Bitmap;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Build;
import android.util.Log;

/**
 * Created by alex on 30.08.13.
 */
public class FileUtil {

    private static final String TAG = FileUtil.class.getSimpleName();
    private static final VMRuntimeHack runtimeHack = new VMRuntimeHack();

    public static Bitmap createBitmap(int newWidth, int newHeight, Bitmap.Config config) throws OutOfMemoryError {
        Bitmap bmp = null;
        try {
            bmp = Bitmap.createBitmap(newWidth, newHeight, config);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "out memroy", e);
            if (newWidth > 3 && newHeight > 3) {
                bmp = createBitmap((int) (newWidth * 0.75), (int) (newHeight * 0.75), config);
            } else {
                throw new OutOfMemoryError("Cann't create image for save");
            }
        }
        if (bmp != null && android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB && runtimeHack.isHackAvailable()) {
            runtimeHack.trackFree(bmp.getRowBytes() * bmp.getHeight());
        }
        return bmp;
    }

    public static void recycle(Bitmap bmp) {
        bmp.recycle();
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB && runtimeHack.isHackAvailable()) {
            runtimeHack.trackAlloc(bmp.getRowBytes() * bmp.getHeight());
        }
    }

    /**
     * Read bitmap in real size. and return bitmap.
     * If Out of memory or invalid image returned null.
     */
    public static Bitmap readRealBitmap(String imageFilePath) {
        Bitmap bitmap = null;
        try {
            bitmap = readBitmap(imageFilePath, 1);
        } catch (OutOfMemoryError error) {
            Log.e(TAG, "error to read Image " + imageFilePath);
            bitmap = readBitmap(imageFilePath, 2);
        }
        if (bitmap != null && android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB && runtimeHack.isHackAvailable()) {
            runtimeHack.trackFree(bitmap.getRowBytes() * bitmap.getHeight());
        }
        return bitmap;
    }
    /**
     * Read bitmap in real size. and return bitmap.
     * If Out of memory or invalid image returned null.
     */
    public static Bitmap readBitmapFromByteArray(byte[] data, int sampleSize) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;
            opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
            opts.inDither = false;
            opts.inPurgeable = true;
            opts.inPreferQualityOverSpeed = true;
            opts.inSampleSize = sampleSize;
            opts.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeByteArray(data, 0,
                    data.length,opts);
        } catch (OutOfMemoryError error) {
            if (sampleSize < 17) {
                bitmap = readBitmapFromByteArray(data, sampleSize * 2);
            }
        }
        if (bitmap != null && android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB && runtimeHack.isHackAvailable()) {
            runtimeHack.trackFree(bitmap.getRowBytes() * bitmap.getHeight());
        }
        return bitmap;
    }
    
    



    private static Bitmap readBitmap(Context context, int resId, int sampleSize) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;
            opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
            opts.inDither = false;
            opts.inPurgeable = true;
            opts.inPreferQualityOverSpeed = true;
            opts.inSampleSize = sampleSize;
            opts.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeResource(context.getResources(), resId, opts);
            if (bitmap == null) {
                opts.inSampleSize = sampleSize * 2;
                bitmap = BitmapFactory.decodeResource(context.getResources(), resId, opts);
                if (bitmap == null) {
                    opts.inSampleSize = sampleSize * 4;
                    bitmap = BitmapFactory.decodeResource(context.getResources(), resId, opts);
                }
            }
        } catch (OutOfMemoryError error) {
            if (sampleSize < 17) {
                bitmap = readBitmap(context, resId, sampleSize * 2);
            }
        }
        return bitmap;
    }

    /**
     * Read bitmap in real size. and return bitmap.
     * If Out of memory or invalid image returned null.
     */
    public static Bitmap readBitmap(String imageFilePath, int sampleSize) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;
            opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
            opts.inDither = false;
            opts.inPurgeable = true;
            opts.inPreferQualityOverSpeed = true;
            opts.inSampleSize = sampleSize;
            opts.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(imageFilePath, opts);
            if (bitmap == null) {
                opts.inSampleSize = sampleSize * 2;
                bitmap = BitmapFactory.decodeFile(imageFilePath, opts);
                if (bitmap == null) {
                    opts.inSampleSize = sampleSize * 4;
                    bitmap = BitmapFactory.decodeFile(imageFilePath, opts);
                }
            }
        } catch (OutOfMemoryError error) {
            Log.e(TAG, "error to read Image " + imageFilePath + " \n inSampleSize =" + sampleSize);
            if (sampleSize < 17) {
                bitmap = readBitmap(imageFilePath, sampleSize * 2);
            }
        }
        return bitmap;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int rotateAngle) throws OutOfMemoryError {
        rotateAngle = rotateAngle % 360;
        if (bitmap == null) {
            return null;
        } else if (rotateAngle == 0) {
            return bitmap;
        } else {
            Bitmap targetBitmap = null;
            if (Math.abs((rotateAngle / 90) % 2) == 1) {
                targetBitmap = FileUtil.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            } else {
                targetBitmap = FileUtil.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            }
            Canvas canvas = new Canvas(targetBitmap);
            Matrix matrix = new Matrix();
            matrix.setRotate(rotateAngle, 0, 0);
            float scale = 1f;
            if (rotateAngle % 180 == 0) {
                matrix.postTranslate(bitmap.getWidth(), bitmap.getHeight());
                scale = ((float) targetBitmap.getWidth()) / bitmap.getWidth();
            } else if (rotateAngle / 90 == 1 || rotateAngle / 270 == -1) {
                matrix.postTranslate(bitmap.getHeight(), 0);
                scale = ((float) targetBitmap.getWidth()) / bitmap.getHeight();
            } else if (rotateAngle / 270 == 1 || rotateAngle / 90 == -1) {
                matrix.postTranslate(0, bitmap.getWidth());
                scale = ((float) targetBitmap.getWidth()) / bitmap.getHeight();
            }
            matrix.postScale(scale, scale);
            Bitmap bmp = bitmap;
            canvas.drawBitmap(bmp, matrix, new Paint());
            bitmap= targetBitmap;
            FileUtil.recycle(bmp);
           return bitmap;
        }
    }
}