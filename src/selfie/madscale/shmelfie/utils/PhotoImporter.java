package selfie.madscale.shmelfie.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.database.DataManager;
import selfie.madscale.shmelfie.models.PhotoList;
import selfie.madscale.shmelfie.models.PhotoObject;

import android.content.Context;
import android.net.Uri;


public class PhotoImporter extends PhotoExporter {

	public PhotoImporter(Context _context) {
		super(_context);
	}

	public boolean doImport() {
		File folder = new File(this.dataPath + TEMP_FOLDER_NAME);
		folder.delete();
		Decompress decompress = new Decompress(this.dataPath
				+ "/export_mylife.zip", this.dataPath + TEMP_FOLDER_NAME);
		decompress.unzip();

		File reader = new File(this.dataPath + TEMP_FOLDER_NAME + "export.xml");
		Serializer serializer = new Persister();
		PhotoList list = new PhotoList();
		ArrayList<PhotoObject> photos = new ArrayList<PhotoObject>();
		try {
			list = serializer.read(PhotoList.class, reader, false);
			photos = list.list;
			importPhotos(this.context, photos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	void importPhotos(Context context, ArrayList<PhotoObject> _photos) {
		ArrayList<PhotoObject> currentPhotos = DataManager
				.getPhotoList(context);
		currentPhotos.addAll(DataManager.getBestPhotoList(context));
		for (PhotoObject o : _photos) {
			boolean existFlag = false;
			String iPhotoName = Uri.parse(o.getPath()).getLastPathSegment();
			for (PhotoObject c : currentPhotos) {
				String cPhotoName = Uri.parse(c.getPath()).getLastPathSegment();
				if (iPhotoName.equals(cPhotoName)) {
					existFlag = true;
				}

			}
			if (!existFlag) {
				File src = new File(this.dataPath + this.TEMP_FOLDER_NAME
						+ Uri.parse(iPhotoName).getLastPathSegment());
				File dst = new File(VAR.PATH + "/" + iPhotoName);

				try {
					FileUtils.copyFile(src, dst);
					o.setPath(dst.getPath());
					if(o.getGroup() == 0)
						DataManager.insertPhoto(this.context, o);
					if(o.getGroup() == 1)
						DataManager.insertBestPhoto(this.context, o);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			
		}
		try {
			FileUtils.deleteDirectory(new File(this.dataPath + this.TEMP_FOLDER_NAME));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
