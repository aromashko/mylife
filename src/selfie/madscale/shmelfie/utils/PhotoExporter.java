/**
 * 
 */
package selfie.madscale.shmelfie.utils;

import java.io.File;
import java.util.ArrayList;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.Format;
import selfie.madscale.shmelfie.models.PhotoList;
import selfie.madscale.shmelfie.models.PhotoObject;

import android.content.Context;


/**
 * @author A.Kokulyuk
 * 
 */
public class PhotoExporter {
	protected ArrayList<PhotoObject> photos[];
	protected String dataPath;
	public static final String TEMP_FOLDER_NAME = "/mylife_export_temp/";
	protected Context context;
	public PhotoExporter(Context _context) {
		this.context = _context;
	}

	public PhotoExporter(ArrayList<PhotoObject>... _photos) {
		this.photos = _photos;
	}

	public void setPhotos(ArrayList<PhotoObject>... _photos) {
		this.photos = _photos;
	}

	public void setDataPath(String _path) {
		this.dataPath = _path;
	}

	public boolean doExport() {
		
		
		ArrayList<String> photoPaths = new ArrayList<String>();
		ArrayList<PhotoObject> toExport = new ArrayList<PhotoObject>();
		toExport.addAll(this.photos[0]);
		toExport.addAll(this.photos[1]);
		for (PhotoObject iPhotoObject : toExport) {
			String path = iPhotoObject.getPath();
			photoPaths.add(path);
		}

		
		PhotoList photoList = new PhotoList();
		photoList.list = toExport;
		File xml = new File(this.dataPath + "/export.xml");
		Format format = new Format("<?xml version=\"1.0\" encoding= \"UTF-8\" ?>");
		Serializer serializer = new Persister(format);
		try {
			serializer.write(photoList, xml);
		} catch (Exception e) {
			e.printStackTrace();
			xml.delete();
			return false;
		}
		photoPaths.add(xml.getPath());

		Compress compress = new Compress(photoPaths, this.dataPath
				+ "/export_mylife.zip");
		compress.zip();
		xml.delete();
		return true;
	}

	public ArrayList<PhotoObject>[] getPhotos(){
		return this.photos;
		
	}
	
	
	
}
