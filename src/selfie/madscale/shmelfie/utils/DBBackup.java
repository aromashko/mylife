package selfie.madscale.shmelfie.utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.Format;

import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.database.DataManager;
import selfie.madscale.shmelfie.models.PhotoList;
import selfie.madscale.shmelfie.models.PhotoObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import static org.apache.commons.io.FileUtils.listFiles;

public class DBBackup {
	public static int countRestoreBackUp=0;

    public static String getCurrentId(Context c){
        return PreferenceUtil.getString(c, VAR.APP_INSTALL_ID);
    }

    public static void makeBackup(Context c, ArrayList<PhotoObject>... _data){
        makeBackup(getCurrentId(c),_data);
    }

	public static void makeBackup(String id, ArrayList<PhotoObject>... _data) {
		ArrayList<String> photoPaths = new ArrayList<String>();
		ArrayList<PhotoObject> toExport = new ArrayList<PhotoObject>();
		toExport.addAll(_data[0]);
		toExport.addAll(_data[1]);

		PhotoList photoList = new PhotoList();
		photoList.list = toExport;

		File xml = new File(VAR.PATH + "/mylife_backup" + id + ".xml");
		Format format = new Format(
				"<?xml version=\"1.0\" encoding= \"UTF-8\" ?>");
		Serializer serializer = new Persister(format);
		try {
			serializer.write(photoList, xml);
		} catch (Exception e) {
			e.printStackTrace();
			xml.delete();
			return;
		}
	}

    public static int getBackupCount(){
        File directory = new File(VAR.PATH);
        Log.e("PATH", VAR.PATH);
        if (directory.exists() && directory.isDirectory()) {
            return listFiles(directory, new String[]{"xml"}, false).size();
        }
        return 0;
    }
    
    public static int getBackupCountAll(Context _context){
    	int count =0;
        Collection<File> files = listFiles(new File(VAR.PATH), new String[]{"xml"}, false);
        //Log.e("Back Up Files", "Number= "+ files.size());
        for(File f: files){
        	count++;
            if(f.getName().contains("mylife_backup")){
                String id = f.getName().replaceAll("mylife_backup", "").replaceAll(".xml", "");
                File reader = new File(VAR.PATH + "/mylife_backup" + id + ".xml");
        		Serializer serializer = new Persister();
        		PhotoList list = new PhotoList();
        		try {
        			list = serializer.read(PhotoList.class, reader, false);
        			count+=list.list.size();;
        		} catch (Exception e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        			continue;
        		}
            }
        }
       return count;
    }

    public static boolean applyBackup(Context _context) {
        boolean result = false;
        Collection<File> files = listFiles(new File(VAR.PATH), new String[]{"xml"}, false);
        for(File f: files){
        	countRestoreBackUp++;
            if(f.getName().contains("mylife_backup")){
                String id = f.getName().replaceAll("mylife_backup", "").replaceAll(".xml", "");
                result = result | applyBackup(_context, id);
            }
        }
        return result;
    }

	public static boolean applyBackup(Context _context, String id) {
		File reader = new File(VAR.PATH + "/mylife_backup" + id + ".xml");
		Serializer serializer = new Persister();
		PhotoList list = new PhotoList();
		ArrayList<PhotoObject> photos = new ArrayList<PhotoObject>();
		
		try {
			list = serializer.read(PhotoList.class, reader, false);
			photos = list.list;
			importPhotos(_context, photos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	

	public static void importPhotos(Context _context, ArrayList<PhotoObject> _photos) {

		ArrayList<PhotoObject> currentPhotos = DataManager
				.getPhotoList(_context);
		currentPhotos.addAll(DataManager.getBestPhotoList(_context));
		for (PhotoObject o : _photos) {
			countRestoreBackUp++;
			boolean existFlag = false;
			String iPhotoName = Uri.parse(o.getPath()).getLastPathSegment();
			for (PhotoObject c : currentPhotos) {
				
				String cPhotoName = Uri.parse(c.getPath()).getLastPathSegment();
				
				if (iPhotoName.equals(cPhotoName)) {
					existFlag = true;
				}
			}
			if (!existFlag) {

				if (o.getGroup() == 0)
					DataManager.insertPhoto(_context, o);
				if (o.getGroup() == 1)
					DataManager.insertBestPhoto(_context, o);
			}
		}
	}
}