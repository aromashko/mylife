package selfie.madscale.shmelfie.utils;

import java.util.Random;


public class RandomInteger {
	public static int generetaRandom(int minimum, int maximum){
		    Random random = new Random();
		    return getRandomInteger(minimum, maximum, random);
		  }
		  
		  private static int getRandomInteger(int aStart, int aEnd, Random aRandom){
		    if (aStart > aEnd) {
		      throw new IllegalArgumentException("Start cannot exceed End.");
		    }
		    //get the range, casting to long to avoid overflow problems
		    long range = (long)aEnd - (long)aStart + 1;
		    // compute a fraction of the range, 0 <= frac < range
		    long fraction = (long)(range * aRandom.nextDouble());
		    int randomNumber =  (int)(fraction + aStart);    
		    return randomNumber;
		  }
		  
}
