package selfie.madscale.shmelfie.utils;

/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 17.05.13
 */
public class PreferenceKey {

    /**
     * By the key have to save boolean value. Default value have to be {@code false}.<br>
     * <br> {@code DO_ASK_PIN = "do_ask_pin"}
     */
    public static final String DO_ASK_PIN = "do_ask_pin";
    /**
     * By the key have to save boolean value. Default value have to be {@code false}.<br>
     * <br> {@code DO_NOT_SHOW_DIALOG_AGAIN = "do_not_show_dialog_again"}
     */
    public static final String DO_NOT_SHOW_DIALOG_AGAIN = "do_not_show_dialog_again";
    
    public static final String DO_NOT_SHOW_WARNING_AGAIN = "do_not_show_warning_again";
    
    /**
     * By the key have to save pin code, String value. <br>
     * <br> {@code PIN_CODE = "pin_code"}
     */
    public static final String PIN_CODE = "pin_code";

    private PreferenceKey() {

    }
}