/*
 * Copyright 2012 sloboda-studio.com
 */
package selfie.madscale.shmelfie.database;

import selfie.madscale.shmelfie.BuildConfig;
import selfie.madscale.shmelfie.database.DatabaseContract.ArchiveRecords;
import selfie.madscale.shmelfie.database.DatabaseContract.BestRecords;
import selfie.madscale.shmelfie.database.DatabaseContract.Records;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;



/**
 * Helper for managing {@link android.database.sqlite.SQLiteDatabase} that
 * stores data for {@link BackupProvider}. This class helps open, create, and
 * upgrade the database file.
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "myLife.db";
	private static final int DATABASE_VERSION = 1;
	private static final String DEBUG_TAG = DatabaseOpenHelper.class
			.getSimpleName();

	public DatabaseOpenHelper(Context context) {

		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
	}

	/**
	 * The function remove all tables from DB!
	 * 
	 * @param db
	 *            - object of SQLiteDatabase
	 */
	public void dropTables(SQLiteDatabase db) {

		if (BuildConfig.DEBUG) {
			Log.d(DEBUG_TAG, "onDropTables called");
		}
		db.execSQL("DROP TABLE IF EXISTS " + Records.TABLE_NAME);
	}

	@Override
	public void onCreate(final SQLiteDatabase db) {

		if (BuildConfig.DEBUG) {
			Log.v(DEBUG_TAG, "onCreate()");
		}
		db.execSQL("CREATE TABLE " + Records.TABLE_NAME + " ("
				+ BaseColumns._ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
				+ Records.RecordsColumns.NAME + " TEXT NOT NULL, "
				+ Records.RecordsColumns.DURATION + " TEXT NOT NULL, "
				+ Records.RecordsColumns.PATH + " TEXT NOT NULL " + ");");
		db.execSQL("CREATE TABLE " + ArchiveRecords.TABLE_NAME + " ("
				+ BaseColumns._ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
				+ Records.RecordsColumns.NAME + " TEXT NOT NULL, "
				+ Records.RecordsColumns.DURATION + " TEXT NOT NULL, "
				+ Records.RecordsColumns.PATH + " TEXT NOT NULL " + ");");
		db.execSQL("CREATE TABLE " + BestRecords.TABLE_NAME + " ("
				+ BaseColumns._ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
				+ Records.RecordsColumns.NAME + " TEXT NOT NULL, "
				+ Records.RecordsColumns.DURATION + " TEXT NOT NULL, "
				+ Records.RecordsColumns.PATH + " TEXT NOT NULL " + ");");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		if (BuildConfig.DEBUG) {
			Log.d(DEBUG_TAG, "onUpgrade called");
		}
	}
}
