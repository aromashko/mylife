/*
 * Copyright 2012 sloboda-studio.com
 */
package selfie.madscale.shmelfie.database;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {

    /** Describes Records Table. */
    public static class Records {

        public interface RecordsColumns extends BaseColumns {

            /** String */
            public static final String NAME = "name";
            /** String */
            public static final String DURATION = "duration";
            /** String */
            public static final String PATH = "path";
        }

        public static final String TABLE_NAME = Records.class.getSimpleName().toLowerCase();
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
                + CONTENT_AUTHORITY.replace("com.", "") + "." + TABLE_NAME;
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
                + CONTENT_AUTHORITY.replace("com.", "") + "." + TABLE_NAME;
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
        /** Default "ORDER BY" clause. */
        public static final String DEFAULT_SORT = RecordsColumns.NAME + " ASC";

        public static Uri buildRecordsUri(long recId) {

            return ContentUris.withAppendedId(CONTENT_URI, recId);
        }

        public static String getRecordsId(Uri uri) {

            return uri.getPathSegments().get(1);
        }

        /** A private Constructor prevents class from instantiating. */
        private Records() {

            throw new UnsupportedOperationException("Class is prevented from instantiation");
        }
    }

    /** Describes ArchiveRecords Table. */
    public static class ArchiveRecords {

        public interface ArchiveRecordsColumns extends BaseColumns {

            /** String */
            public static final String NAME = "name";
            /** String */
            public static final String DURATION = "duration";
            /** String */
            public static final String PATH = "path";
        }

        public static final String TABLE_NAME = ArchiveRecords.class.getSimpleName().toLowerCase();
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
                + CONTENT_AUTHORITY.replace("com.", "") + "." + TABLE_NAME;
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
                + CONTENT_AUTHORITY.replace("com.", "") + "." + TABLE_NAME;
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
        /** Default "ORDER BY" clause. */
        public static final String DEFAULT_SORT = ArchiveRecordsColumns.NAME + " ASC";

        public static Uri buildArchiveRecordsUri(long aRecId) {

            return ContentUris.withAppendedId(CONTENT_URI, aRecId);
        }

        public static String getArchiveRecordsId(Uri uri) {

            return uri.getPathSegments().get(1);
        }

        /** A private Constructor prevents class from instantiating. */
        private ArchiveRecords() {

            throw new UnsupportedOperationException("Class is prevented from instantiation");
        }
    }

    /** Describes BestRecords Table. */
    public static class BestRecords {

        public interface BestRecordsColumns extends BaseColumns {

            /** String */
            public static final String NAME = "name";
            /** String */
            public static final String DURATION = "duration";
            /** String */
            public static final String PATH = "path";
        }

        public static final String TABLE_NAME = BestRecords.class.getSimpleName().toLowerCase();
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
                + CONTENT_AUTHORITY.replace("com.", "") + "." + TABLE_NAME;
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
                + CONTENT_AUTHORITY.replace("com.", "") + "." + TABLE_NAME;
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
        /** Default "ORDER BY" clause. */
        public static final String DEFAULT_SORT = BestRecordsColumns.NAME + " ASC";

        public static Uri buildBestRecordsUri(long bRecId) {

            return ContentUris.withAppendedId(CONTENT_URI, bRecId);
        }

        public static String getBestRecordsId(Uri uri) {

            return uri.getPathSegments().get(1);
        }

        /** A private Constructor prevents class from instantiating. */
        private BestRecords() {

            throw new UnsupportedOperationException("Class is prevented from instantiation");
        }
    }

    public static final String CONTENT_AUTHORITY = "selfie.madscale.shmelfie";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /** A private Constructor prevents class from instantiating. */
    private DatabaseContract() {

        throw new UnsupportedOperationException("Class is prevented from instantiation");
    }
}