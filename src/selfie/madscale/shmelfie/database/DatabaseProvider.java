/*
 * Copyright 2012 sloboda-studio.com
 */
package selfie.madscale.shmelfie.database;

import selfie.madscale.shmelfie.database.DatabaseContract.ArchiveRecords;
import selfie.madscale.shmelfie.database.DatabaseContract.BestRecords;
import selfie.madscale.shmelfie.database.DatabaseContract.Records;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;


/** The Class DatabaseProvider. Main provider for application delivery. */
public class DatabaseProvider extends ContentProvider {

	private static final UriMatcher sUriMatcher = buildUriMatcher();
	// Provide a mechanism to identify all the incoming uri patterns.
	private static final int RECORDS_DIR_ID = 100;
	private static final int RECORDS_ITEM_ID = 101;
	private static final int BEST_RECORDS_DIR_ID = 200;
	private static final int BEST_RECORDS_ITEM_ID = 201;
	private static final int ARCHIVE_RECORDS_DIR_ID = 300;
	private static final int ARCHIVE_RECORDS_ITEM_ID = 301;

	private static UriMatcher buildUriMatcher() {

		String authority = DatabaseContract.CONTENT_AUTHORITY;
		UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
		matcher.addURI(authority, Records.TABLE_NAME, RECORDS_DIR_ID);
		matcher.addURI(authority, Records.TABLE_NAME + "/#", RECORDS_ITEM_ID);
		matcher.addURI(authority, BestRecords.TABLE_NAME, BEST_RECORDS_DIR_ID);
		matcher.addURI(authority, BestRecords.TABLE_NAME + "/#",
				BEST_RECORDS_ITEM_ID);
		matcher.addURI(authority, ArchiveRecords.TABLE_NAME,
				ARCHIVE_RECORDS_DIR_ID);
		matcher.addURI(authority, ArchiveRecords.TABLE_NAME + "/#",
				ARCHIVE_RECORDS_ITEM_ID);
		return matcher;
	}

	// Deal with OnCreate call back
	private DatabaseOpenHelper mDatabaseOpenHelper;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {

		SQLiteDatabase db = mDatabaseOpenHelper.getWritableDatabase();
		int count = getSimpleSelectionBuilder(uri, selection, selectionArgs)
				.delete(db);
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	private SelectionBuilder getSimpleSelectionBuilder(Uri uri,
			String selection, String[] selectionArgs) {

		SelectionBuilder builder = new SelectionBuilder();
		switch (sUriMatcher.match(uri)) {
		case RECORDS_DIR_ID:
			builder.table(Records.TABLE_NAME);
			if (TextUtils.isEmpty(selection)) {
				builder.where(null, (String[]) null);
			} else {
				builder.where(selection, selectionArgs);
			}
			break;
		case RECORDS_ITEM_ID:
			String _Id = Records.getRecordsId(uri);
			builder.table(Records.TABLE_NAME)
					.where(BaseColumns._ID + "=?", _Id);
			break;
		case BEST_RECORDS_DIR_ID:
			builder.table(BestRecords.TABLE_NAME);
			if (TextUtils.isEmpty(selection)) {
				builder.where(null, (String[]) null);
			} else {
				builder.where(selection, selectionArgs);
			}
			break;
		case BEST_RECORDS_ITEM_ID:
			String _IdBest = BestRecords.getBestRecordsId(uri);
			builder.table(BestRecords.TABLE_NAME).where(BaseColumns._ID + "=?",
					_IdBest);
			break;
		case ARCHIVE_RECORDS_DIR_ID:
			builder.table(ArchiveRecords.TABLE_NAME);
			if (TextUtils.isEmpty(selection)) {
				builder.where(null, (String[]) null);
			} else {
				builder.where(selection, selectionArgs);
			}
			break;
		case ARCHIVE_RECORDS_ITEM_ID:
			String _IdA = ArchiveRecords.getArchiveRecordsId(uri);
			builder.table(ArchiveRecords.TABLE_NAME).where(
					BaseColumns._ID + "=?", _IdA);
			break;
		default:
			throw new IllegalArgumentException("Unknown uri: " + uri);
		}
		return builder;
	}

	@Override
	public String getType(Uri uri) {

		switch (sUriMatcher.match(uri)) {
		case RECORDS_DIR_ID:
			return DatabaseContract.Records.CONTENT_TYPE;
		case RECORDS_ITEM_ID:
			return DatabaseContract.Records.CONTENT_ITEM_TYPE;
		case BEST_RECORDS_DIR_ID:
			return DatabaseContract.BestRecords.CONTENT_TYPE;
		case BEST_RECORDS_ITEM_ID:
			return DatabaseContract.BestRecords.CONTENT_ITEM_TYPE;
		case ARCHIVE_RECORDS_DIR_ID:
			return DatabaseContract.ArchiveRecords.CONTENT_TYPE;
		case ARCHIVE_RECORDS_ITEM_ID:
			return DatabaseContract.ArchiveRecords.CONTENT_ITEM_TYPE;
		default:
			throw new IllegalArgumentException("Unknown Uri: " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) throws SQLiteException {

		SQLiteDatabase db = mDatabaseOpenHelper.getWritableDatabase();
		Uri insertedUri;
		long insertedId;
		Log.v("URI insert=", uri.toString());
		switch (sUriMatcher.match(uri)) {
		case RECORDS_DIR_ID:
			insertedId = db.insertOrThrow(Records.TABLE_NAME, null, values);
			insertedUri = Records.buildRecordsUri(insertedId);
			break;
		case BEST_RECORDS_DIR_ID:
			insertedId = db.insertOrThrow(BestRecords.TABLE_NAME, null, values);
			insertedUri = BestRecords.buildBestRecordsUri(insertedId);
			break;
		case ARCHIVE_RECORDS_DIR_ID:
			insertedId = db.insertOrThrow(ArchiveRecords.TABLE_NAME, null,
					values);
			insertedUri = ArchiveRecords.buildArchiveRecordsUri(insertedId);
			break;
		default:
			throw new IllegalArgumentException("Unknown uri: " + uri);
		}
		getContext().getContentResolver().notifyChange(insertedUri, null);
		return insertedUri;
	}

	@Override
	public boolean onCreate() {

		
		mDatabaseOpenHelper = new DatabaseOpenHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		final SQLiteDatabase db = mDatabaseOpenHelper.getWritableDatabase();
		final Cursor cur = getSimpleSelectionBuilder(uri, selection,
				selectionArgs).query(db, projection, sortOrder);
		// Tell the cursor what uri to watch,
		// so it knows when its source data changes
		cur.setNotificationUri(getContext().getContentResolver(), uri);
		return cur;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {

		switch (sUriMatcher.match(uri)) {
		case RECORDS_DIR_ID:
			if (TextUtils.isEmpty(selection)) {
				throw new IllegalArgumentException(
						"You are trying update location dir without specify selection. No sence.");
			}
			break;
		case RECORDS_ITEM_ID:
			break;
		default:
			throw new IllegalArgumentException("Unknown uri: " + uri);
		}
		SQLiteDatabase db = mDatabaseOpenHelper.getWritableDatabase();
		int count = getSimpleSelectionBuilder(uri, selection, selectionArgs)
				.update(db, values);
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}
}