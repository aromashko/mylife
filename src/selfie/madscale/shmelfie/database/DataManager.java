/** The DataManager.java...
 *
 * Made by sloboda-studio.com in 2012
 *
 * @author Alexandr Sergienko */
package selfie.madscale.shmelfie.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import selfie.madscale.shmelfie.database.DatabaseContract.ArchiveRecords;
import selfie.madscale.shmelfie.database.DatabaseContract.BestRecords;
import selfie.madscale.shmelfie.database.DatabaseContract.Records;
import selfie.madscale.shmelfie.database.DatabaseContract.ArchiveRecords.ArchiveRecordsColumns;
import selfie.madscale.shmelfie.database.DatabaseContract.BestRecords.BestRecordsColumns;
import selfie.madscale.shmelfie.database.DatabaseContract.Records.RecordsColumns;
import selfie.madscale.shmelfie.models.PhotoObject;
import selfie.madscale.shmelfie.utils.Supporting;

/**
 * @author Vitamin
 */
public class DataManager {

    private static final String TAG = null;

    public static void deleteAllPhotos(Context context) {

        ContentResolver cr = context.getContentResolver();
        cr.delete(Records.CONTENT_URI, null, null);
        cr.delete(BestRecords.CONTENT_URI, null, null);
        cr.delete(ArchiveRecords.CONTENT_URI, null, null);
    }

    /**
     * Delete record by id.
     *
     * @param context
     * @param id      - record id;
     */
    public static void deletePhoto(Context context, int id) {

        ContentResolver cr = context.getContentResolver();
        cr.delete(Records.CONTENT_URI, String.format("%s = %d", BaseColumns._ID, id), null);
    }

    public static void deleteBestPhoto(Context context, int id) {

        ContentResolver cr = context.getContentResolver();
        cr.delete(BestRecords.CONTENT_URI, String.format("%s = %d", BaseColumns._ID, id), null);
    }

    public static void deleteAtchivePhoto(Context context, int id) {

        ContentResolver cr = context.getContentResolver();
        cr.delete(ArchiveRecords.CONTENT_URI, String.format("%s = %d", BaseColumns._ID, id), null);
    }

    /**
     * Get list of all records.
     *
     * @param context
     * @return
     */
    public static ArrayList<PhotoObject> getPhotoList(Context context) {

        ArrayList<PhotoObject> list = new ArrayList<PhotoObject>();
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(Records.CONTENT_URI, null, null, null, null);
        if (cursor != null) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                try {
                    PhotoObject record = new PhotoObject();
                    record.setId(cursor.getInt(cursor.getColumnIndexOrThrow(BaseColumns._ID)));
                    record.setName(cursor.getString(cursor.getColumnIndexOrThrow(RecordsColumns.NAME)));
                    record.setDateInMillis(cursor
                            .getString(cursor.getColumnIndexOrThrow(RecordsColumns.DURATION)));
                    record.setPath(cursor.getString(cursor.getColumnIndexOrThrow(RecordsColumns.PATH)));
                    long timeInMillis = Long.parseLong(record.getDateInMillis());
                    String time = Supporting.parseDateFromLong("d", timeInMillis);
                    record.setDayInMonth(Integer.parseInt(time));
                    time = Supporting.parseDateFromLong("M", timeInMillis);
                    record.setMonthInYear(Integer.parseInt(time));
                    time = Supporting.parseDateFromLong("y", timeInMillis);
                    record.setYear(Integer.parseInt(time));
                    record.setGroup(PhotoObject.GROUP_MAIN);
                    if (new File(record.getPath()).exists()) {
                        list.add(record);
                    } else {
                        deletePhoto(context, record.getId());
                    }
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "Error to read user!", e);
                } catch (Exception e) {
                    Log.e(TAG, "Error to read user!", e);
                }
            }
            if (!cursor.isClosed()) {
                cursor.close();
            }
        }
        return list;
    }

    /**
     * Get list of all best records.
     *
     * @param context
     * @return
     */
    public static ArrayList<PhotoObject> getBestPhotoList(Context context) {

        ArrayList<PhotoObject> list = new ArrayList<PhotoObject>();
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(BestRecords.CONTENT_URI, null, null, null, null);
        if (cursor != null) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                try {
                    PhotoObject record = new PhotoObject();
                    record.setId(cursor.getInt(cursor.getColumnIndexOrThrow(BaseColumns._ID)));
                    record.setName(cursor.getString(cursor.getColumnIndexOrThrow(RecordsColumns.NAME)));
                    record.setDateInMillis(cursor
                            .getString(cursor.getColumnIndexOrThrow(RecordsColumns.DURATION)));
                    record.setPath(cursor.getString(cursor.getColumnIndexOrThrow(RecordsColumns.PATH)));
                    record.setGroup(PhotoObject.GROUP_FAVORITE);
                    if (new File(record.getPath()).exists()) {
                        list.add(record);
                    } else {
                        deleteBestPhoto(context, record.getId());
                    }
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "Error to read user!", e);
                } catch (Exception e) {
                    Log.e(TAG, "Error to read user!", e);
                }
            }
            if (!cursor.isClosed()) {
                cursor.close();
            }
        }
        return list;
    }

    /**
     * Get list of all archive records.
     *
     * @param context
     * @return
     */
    public static ArrayList<PhotoObject> getArchiveList(Context context) {

        ArrayList<PhotoObject> list = new ArrayList<PhotoObject>();
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(ArchiveRecords.CONTENT_URI, null, null, null, null);
        if (cursor != null) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                try {
                    PhotoObject record = new PhotoObject();
                    record.setId(cursor.getInt(cursor.getColumnIndexOrThrow(BaseColumns._ID)));
                    record.setName(cursor.getString(cursor.getColumnIndexOrThrow(RecordsColumns.NAME)));
                    record.setDateInMillis(cursor
                            .getString(cursor.getColumnIndexOrThrow(RecordsColumns.DURATION)));
                    record.setPath(cursor.getString(cursor.getColumnIndexOrThrow(RecordsColumns.PATH)));
                    if (new File(record.getPath()).exists()) {
                        list.add(record);
                    } else {
                        deleteAtchivePhoto(context, record.getId());
                    }
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "Error to read user!", e);
                } catch (Exception e) {
                    Log.e(TAG, "Error to read user!", e);
                }
            }
            if (!cursor.isClosed()) {
                cursor.close();
            }
        }
        return list;
    }

    /**
     * Insert new user.
     *
     * @param context
     * @param user
     * @return
     */
    public static Uri insertPhoto(Context context, PhotoObject record) {

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(RecordsColumns.NAME, record.getName());
        values.put(RecordsColumns.DURATION, record.getDateInMillis());
        values.put(RecordsColumns.PATH, record.getPath());
        return cr.insert(Records.CONTENT_URI, values);
    }

    /**
     * Insert new user.
     *
     * @param context
     * @param user
     * @return
     */
    public static Uri insertBestPhoto(Context context, PhotoObject record) {

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(BestRecordsColumns.NAME, record.getName());
        values.put(BestRecordsColumns.DURATION, record.getDateInMillis());
        values.put(BestRecordsColumns.PATH, record.getPath());
        return cr.insert(BestRecords.CONTENT_URI, values);
    }

    /**
     * Insert new user.
     *
     * @param context
     * @param user
     * @return
     */
    public static Uri insertArchivePhoto(Context context, PhotoObject record) {

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(ArchiveRecordsColumns.NAME, record.getName());
        values.put(ArchiveRecordsColumns.DURATION, record.getDateInMillis());
        values.put(ArchiveRecordsColumns.PATH, record.getPath());
        return cr.insert(ArchiveRecords.CONTENT_URI, values);
    }

    /**
     * Update record.
     *
     * @param context
     * @param record
     * @return
     */
    public static int updatePhoto(Context context, PhotoObject record) {

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(RecordsColumns.NAME, record.getName());
        values.put(RecordsColumns.DURATION, record.getDateInMillis());
        values.put(RecordsColumns.PATH, record.getPath());
        return cr.update(Records.buildRecordsUri(record.getId()), values, null, null);
    }

    /**
     * Update record.
     *
     * @param context
     * @param record
     * @return
     */
    public static int updateArchivePhoto(Context context, PhotoObject record) {

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(ArchiveRecordsColumns.NAME, record.getName());
        values.put(ArchiveRecordsColumns.DURATION, record.getDateInMillis());
        values.put(ArchiveRecordsColumns.PATH, record.getPath());
        return cr.update(ArchiveRecords.buildArchiveRecordsUri(record.getId()), values, null, null);
    }

    /**
     * Update record.
     *
     * @param context
     * @param record
     * @return
     */
    public static int updateBestPhoto(Context context, PhotoObject record) {

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(BestRecordsColumns.NAME, record.getName());
        values.put(BestRecordsColumns.DURATION, record.getDateInMillis());
        values.put(BestRecordsColumns.PATH, record.getPath());
        return cr.update(BestRecords.buildBestRecordsUri(record.getId()), values, null, null);
    }

    private DataManager() {

    }
}
