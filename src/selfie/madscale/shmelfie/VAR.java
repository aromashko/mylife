package selfie.madscale.shmelfie;

import android.os.Environment;

import java.util.Calendar;

/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 18.03.13
 */
public class VAR {

    public static final String ACTION_PHOTO_INSETRED_IN_DB = "com.mylife.photo_inserted_in_db";
    public static final String ACTION_IMPORT_UPDATE = "com.mylife.import_update";
    public static final String ACTIVE_APP = "is_active_app";
    public static String SD_CARD_PATH = Environment.getExternalStorageDirectory().getPath();
    public static String APP_DEFAULT_FOLDER = "Shmelfie";
    public static String PATH = SD_CARD_PATH + "/"+APP_DEFAULT_FOLDER+"/";
    public static final String PREFICS = "shmelfie_";
    public static String dateForamt = "yyyy.MM.dd_kkmmss";
    public static final String IMAGE_FORMAT = ".jpeg";
    public static final String SETTINGS = "settings";
    public static final String LOCATION_ARROW = " > ";

    public static final int MODE_OPEN = 1;
    public static final int MODE_CREATE = 0;
    public static final int MINUTES_IN_DAY = 1439;
    public static final int SECONDS_IN_MIN = 60;
    public static final int MILLIS_IN_SEC = 1000;
    public static final int CAMERA_FRONT = 0;
    public static final int CAMERA_BACK = 1;
    public static final int UPDATE_ADAPTER = 100;
    public static final int RENAME_PHOTO = 301;
    public static final int RENAME_BEST_PHOTO = 302;

    public static boolean[] whichDaysHavePhoto = new boolean[32];
    public static boolean[] whichMonthHavePhoto = new boolean[13];
    public static boolean havePrevNextYear;
    public static boolean havePrevNextMonth;
    public static boolean havePrevNextDay;
    public static String[] numbersString = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31",};
    public static final int DAYS = 32;
    public static final int MONTHES = 13;
    static Calendar calendar = Calendar.getInstance();
    public static int currentYear = calendar.get(Calendar.YEAR);
    public static int MINIMUN_LENGTH_YEAR_WITHOUT_PHOTO=5;

    public static final String SHUTTER_CLICK = "shutterClickBoolean";

    public static final String WHICH_CAMERA = "whichCamera";

    public static int whenMakePhoto = 0; // if 0 = unlock, if 1 = auto, if 2 = unlock not always
    public static final String WHEN_MAKE_PHOTO = "whenMakePhoto";
    
    public static final String ANGLE_CAMERA = "angleCamera";
    public static int TEMP_angleCamera = 0;
    public static final int[] angleCamera = {0, 90, 180, 270};
    
    public static final String AUTO_PER_DAY = "autoPerDay";
    public static final int[] autoPerDay = {1, 3, 5, 10, 20, 50};
    public static int TEMP_autoPerDay = 1;
    
    public static final String SLIDE_SHOW_FREQUENCY = "slideShowFrequency";
    public static final float[] slideShowFrequency = {0.1f, 0.5f, 1, 2, 3, 5, 10};
    public static float TEMP_slideShowFrequency = 0;

    public static final String SLIDE_SHOW_ORDER = "slideShowOrder";
    public static final int[] slideShowOrder = {0, 1, 2}; //0 = forward, 1 = back, 2 = random
    public static int TEMP_slideShowOrder = 0;

    public static final String DATE_DISPLAY = "dateDisplay";
    public static final String[] dateDisplay = {"dd.MM.yy-HH:mm", "yy.MM.dd-HH:mm", "MM.dd.yy-HH:mm"};
    public static String TEMP_dateDisplay = "dd.MM.yy-HH:mm";

    public static final String CHECK_BOX_INTERVAL = "checkBoxInterval";
    public static Boolean TEMP_checkBoxInterval = true;

    public static final String CHECK_BOX_GEOTAG = "checkBoxGeotag";
    public static Boolean TEMP_checkBoxGeotag = false;

    public static final String CHECK_BOX_LIGHTSENSOR = "checkBoxLightSensor";
    public static final String CHECK_BOX_LOOP_PIC = "loop_pic";
    public static final String CHECK_BOX_LOOP_MISIC = "loop_music";
    
    public static final String FROM_TIME = "fromTime";
    public static int TEMP_fromTime;

    public static final String TO_TIME = "toTime";
    public static int TEMP_toTime;

    public static final String CHECK_BOX_PINCODE = "checkBoxPincode";
    public static Boolean TEMP_checkBoxPinCode = false;

    public static final String APP_INSTALL_ID = "appInstallId";

    public static final String IS_FIRST_LAUNCH = "isFirstLaunch";
    
    public static String MUSIC_PATH = "music_path";
    
    public static String QUESTION_EMAIL= "mail@madscale.com";
    
    public static int ANGLE_DEFAULT_VALUE= 3;
    
    public static int WHEN_MAKE_PHOTO_DEFAULT_VALUE= 0;
    
    public static final String LAST_FOLDER="lastFolder";
    
    public static final String BACK_UP_ENDED="backUpEnded";
    
    public static final int FROM_TIME_DEFAULT_VALUE= 660;
    
    public static final int TO_TIME_DEFAULT_VALUE= 1260;
    
    public static final Boolean ACTIVE_APP_DEFAULT_VALUE = true;
    
    public static final Boolean CHECK_BOX_LOOP_MISIC_DEFAULT_VALUE= true;
    
    public static final Boolean CHECK_BOX_LOOP_PICTURE_DEFAULT_VALUE= true;
    
    public static final int SLIDE_SHOW_ORDER_DEFAULT_VALUE= 1;
    
    public static final String SHOW_DATE = "showDate";
    
    public static final Boolean SHOW_DATE_DEFAULT_VALUE = true;


}
