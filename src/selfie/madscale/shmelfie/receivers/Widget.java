package selfie.madscale.shmelfie.receivers;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.service.CameraService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class Widget extends AppWidgetProvider{
	
	private static String SWICHER = "selfie.madscale.shmelfie.WIDGET_BUTTON";
	public static SharedPreferences settings;
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteViews.setOnClickPendingIntent(R.id.widget_swicher, buildButtonPendingIntent(context));
		if(settings == null){
			settings = context.getSharedPreferences(VAR.SETTINGS, Context.MODE_PRIVATE);
		}
		if (settings.getBoolean(VAR.ACTIVE_APP, VAR.ACTIVE_APP_DEFAULT_VALUE)) {
			Log.e("Widget", "On in OnUpdate");
			remoteViews.setInt(R.id.widget_swicher, "setBackgroundResource", R.drawable.widget_on);
           
           
        }else {
        	Log.e("Widget", "Off in OnUpdate");
        	 remoteViews.setInt(R.id.widget_swicher, "setBackgroundResource", R.drawable.widget_off);
			
		}
		pushWidgetUpdate(context, remoteViews);
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		super.onReceive(context, intent);
		if(SWICHER.equals(intent.getAction())){
			Toast.makeText(context.getApplicationContext(), "SWICH", Toast.LENGTH_LONG).show();
			updateWidgetPictureAndButtonListener(context);
		}
		
	}
	
	public static PendingIntent buildButtonPendingIntent(Context context) {
		Intent intent = new Intent();
	    intent.setAction(SWICHER);
	    return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
	
	public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
		ComponentName myWidget = new ComponentName(context, Widget.class);
	    AppWidgetManager manager = AppWidgetManager.getInstance(context);
	    manager.updateAppWidget(myWidget, remoteViews);		
	}
	
	public static void updateWidgetPictureAndButtonListener(Context context) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		if(settings == null){
			settings = context.getSharedPreferences(VAR.SETTINGS, Context.MODE_PRIVATE);
		}
		//WidgetSetting.getWeatherInCity(mPref.getString("pickedCity", "Svitlovodsk" ));
		
		final Intent service = new Intent(context,CameraService.class);
		if (settings.getBoolean(VAR.ACTIVE_APP, VAR.ACTIVE_APP_DEFAULT_VALUE)) {
			Log.e("Widget", "Change to off");
			context.stopService(service);
			remoteViews.setInt(R.id.widget_swicher, "setBackgroundResource", R.drawable.widget_off);
			saveSharedPreferences(context, VAR.ACTIVE_APP, false);
        }else {
        	Log.e("Widget", "Change to on");
        	context.startService(service);
            remoteViews.setInt(R.id.widget_swicher, "setBackgroundResource", R.drawable.widget_on);
            saveSharedPreferences(context, VAR.ACTIVE_APP, true);
			
		}
		
		//REMEMBER TO ALWAYS REFRESH YOUR BUTTON CLICK LISTENERS!!!
		remoteViews.setOnClickPendingIntent(R.id.widget_swicher, Widget.buildButtonPendingIntent(context));
		
		Widget.pushWidgetUpdate(context.getApplicationContext(), remoteViews);
	}
	
	 public static void saveSharedPreferences(Context context,String key, Boolean value) {

	        SharedPreferences settings = context.getSharedPreferences(VAR.SETTINGS,
	                Context.MODE_PRIVATE);
	        SharedPreferences.Editor ed = settings.edit();
	        ed.putBoolean(key, value);
	        ed.commit();
	    }
	

}
