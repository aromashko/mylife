package selfie.madscale.shmelfie.receivers;

import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.service.CameraService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;


/**
 * Created by alex on 24.01.14.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences settings = context.getSharedPreferences(VAR.SETTINGS, Context.MODE_PRIVATE);
        if(settings.getBoolean(VAR.ACTIVE_APP, VAR.ACTIVE_APP_DEFAULT_VALUE)){
        	 Log.e("Boot Receiver", "Start Camera Service");
            context.startService(new Intent(context, CameraService.class));
        }
    }
}
