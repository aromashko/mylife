package selfie.madscale.shmelfie;

import android.text.format.Time;
import java.util.Random;


import org.holoeverywhere.FontLoader;
import org.holoeverywhere.FontLoader.FontCollector;
import org.holoeverywhere.FontLoader.RawFont;
import org.holoeverywhere.app.Application;

import selfie.madscale.shmelfie.utils.PreferenceUtil;
//@ReportsCrashes(formKey = "dGlEUzFoSVNKNmNIY2wzcjVrMzN3c3c6MA")
public class MyLifeApplication extends Application {
	 private static RawFont arial_regular;
	 private static RawFont open_sans_regular;
	 private static FontCollector sDefaultFont;

    private boolean isFirstLaunch(){
        try{
           PreferenceUtil.getString(getApplicationContext(), VAR.APP_INSTALL_ID).equals("");

        }catch(IllegalArgumentException e){
            return true;
        }
        return false;
    }

    private void onFirstLaunch(){
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        Random r = new Random(today.toMillis(false));
        PreferenceUtil.putString(getApplicationContext(),VAR.APP_INSTALL_ID, String.valueOf(r.nextInt()));
    }
    @Override
    public void onCreate() {
        super.onCreate();
        
        arial_regular = new RawFont(R.raw.arial_regular);
        arial_regular.setFontFamily("arial_regular");
        arial_regular.setFontStyle(FontLoader.TEXT_STYLE_NORMAL);
        
        open_sans_regular = new RawFont(R.raw.open_sans_regular);
        open_sans_regular.setFontFamily("open_sans_regular");
        open_sans_regular.setFontStyle(FontLoader.TEXT_STYLE_NORMAL);
        
        sDefaultFont = new FontCollector();
        sDefaultFont.register(arial_regular);
        sDefaultFont.register(open_sans_regular);
        
        if(isFirstLaunch())
            onFirstLaunch();

        //ACRA.init(this);
    }
}
