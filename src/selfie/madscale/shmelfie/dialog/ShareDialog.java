package selfie.madscale.shmelfie.dialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.models.PhotoObject;
import selfie.madscale.shmelfie.utils.Supporting;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class ShareDialog extends DialogFragment{
	 final String LOG_TAG = "myLogs";
	 final String ATTRIBUTE_NAME_TEXT = "text";
	  final String ATTRIBUTE_NAME_IMAGE = "image";
	  final String ATTRIBUTE_NAME_PACKAGE = "package";
	  
	  ListView lv_share_app;

	  static Context mContext;
	  ArrayList<Map<String, Object>> data= new ArrayList<Map<String,Object>>();
	  ArrayList<String> packages=  new ArrayList<String>();
	  ArrayList<String> sortedPackages=  new ArrayList<String>();
	  static PhotoObject mPhoto;
	  static ArrayList<PhotoObject>  mPhotoArr= new ArrayList<PhotoObject>();
	  static ShareDialog shareDialog;
	  public static ShareDialog newInstance(Context context,  ArrayList<PhotoObject> photoArr) {
		  mContext = context;
		  mPhotoArr.clear();
		  mPhotoArr.addAll(photoArr);
		  shareDialog = new ShareDialog();
	        return shareDialog;
	    }
	
	 

	



	


	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		      Bundle savedInstanceState) {
			setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
			getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
			int width = getResources().getDimensionPixelSize(R.dimen.dialog_fixed_width_minor);
			int height = getResources().getDimensionPixelSize(R.dimen.dialog_fixed_height_minor);        
			getDialog().getWindow().setLayout(width, height);
		    //getDialog().setTitle(getString(R.string.share_image));
		    
		    View v = inflater.inflate(R.layout.share_dialog, container, false);
		    lv_share_app = (ListView) v.findViewById(R.id.lv_share_app);
		    
		   
		    
		    data.addAll(loadDataForList(mContext));
		    
		    if(data.size() == 1){
		    	if (mPhotoArr.size() == 1){
		    		mPhoto = mPhotoArr.get(0);
		    	startDefaultShareDialog(mPhoto);
		    	}
		    	else{
		    		startDefaultShareDialogMultiple();
		    	}
		    }
		    else{

		    // ������ ���� ���������, �� ������� ����� �������� ������
		    String[] from = { ATTRIBUTE_NAME_TEXT, 
		        ATTRIBUTE_NAME_IMAGE};
		    // ������ ID View-�����������, � ������� ����� ��������� ������
		    int[] to = { R.id.tv_share_app, R.id.img_share_app_icon };
		    if (mPhotoArr.size() == 1){
	    		mPhoto = mPhotoArr.get(0);
		    }
		    ShareSimpleAdapter sAdapter = new ShareSimpleAdapter(mContext, data, R.layout.share_list_item,
			        from, to, packages, mPhotoArr) ;


		    lv_share_app.setAdapter(sAdapter);
		    }
		  
		    return v;
		  }
	
	

		 
	



		@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		 data.addAll(loadDataForList(mContext));
		    
		    if(data.size() == 1){
		    	
		    	if (mPhotoArr.size() == 1){
		    		mPhoto = mPhotoArr.get(0);
		    	startDefaultShareDialog(mPhoto);
		    	}
		    	else{
		    		startDefaultShareDialogMultiple();
		    	}
		    }
		    else{

		    // ������ ���� ���������, �� ������� ����� �������� ������
		    String[] from = { ATTRIBUTE_NAME_TEXT, 
		        ATTRIBUTE_NAME_IMAGE};
		    // ������ ID View-�����������, � ������� ����� ��������� ������
		    int[] to = { R.id.tv_share_app, R.id.img_share_app_icon };
		    ShareSimpleAdapter sAdapter = new ShareSimpleAdapter(mContext, data, R.layout.share_list_item,
			        from, to, sortedPackages, mPhotoArr) ;


		    lv_share_app.setAdapter(sAdapter);
		    }
	}



		private void startDefaultShareDialog(PhotoObject photo){
			Supporting.shareActionSend(mContext, photo);
	 }
		
		private void startDefaultShareDialogMultiple(){
			Supporting.shareSelectedMultiple(mContext);
		 }
		
		
		  
		  
		  private ArrayList<Map<String, Object>> loadDataForList(Context context){
			  Intent share = new Intent(Intent.ACTION_SEND);
			 ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	    	    share.setType("*/*");
	    	  List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(share, PackageManager.MATCH_DEFAULT_ONLY);
	    	  if (!resInfo.isEmpty()){
	    	    for (ResolveInfo info : resInfo) {
	    	    	Log.e("Package name", info.activityInfo.packageName);
	    	    	Log.e("App name", (String) info.loadLabel(context.getPackageManager()));

	    	    	if (info.activityInfo.packageName.toLowerCase().contains("facebook") || info.activityInfo.name.toLowerCase().contains("facebook")){
	    	    			    Map<String, Object> m = new HashMap<String, Object>();
	    	    			      m.put(ATTRIBUTE_NAME_TEXT, info.loadLabel(context.getPackageManager()));
	    	    			      Log.e("Info icon", info.icon + "");
	    	    			      //m.put(ATTRIBUTE_NAME_IMAGE, R.drawable.facebook_icon);
	    	    			      data.add(m);
	    	    			      packages.add(info.activityInfo.packageName);
	    	    			    
	    	    	}
	    	    	else if(info.activityInfo.packageName.toLowerCase().contains("twitter") || info.activityInfo.name.toLowerCase().contains("twitter")){
	    	    		Map<String, Object> m = new HashMap<String, Object>();
	    			      m.put(ATTRIBUTE_NAME_TEXT, info.loadLabel(context.getPackageManager()));
	    			     // m.put(ATTRIBUTE_NAME_IMAGE, R.drawable.twitter_icon);
	    			      
	    			      data.add(m);
	    			      packages.add(info.activityInfo.packageName);
	    	    	}
	    	    	else if(info.activityInfo.packageName.toLowerCase().contains("mms") || info.activityInfo.name.toLowerCase().contains("mms")){
	    	    		Map<String, Object> m = new HashMap<String, Object>();
	    			      m.put(ATTRIBUTE_NAME_TEXT, info.loadLabel(context.getPackageManager()));
	    			     // m.put(ATTRIBUTE_NAME_IMAGE, R.drawable.sms_icon);
	    			      data.add(m);
	    			      packages.add(info.activityInfo.packageName);
	    	    	}
	    	    	else if(info.activityInfo.packageName.toLowerCase().contains("instagram") || info.activityInfo.name.toLowerCase().contains("instagram")){
	    	    		Map<String, Object> m = new HashMap<String, Object>();
	    			      m.put(ATTRIBUTE_NAME_TEXT, info.loadLabel(context.getPackageManager()));
	    			      //m.put(ATTRIBUTE_NAME_IMAGE, R.drawable.instagram_icon);
	    			      data.add(m);
	    			      packages.add(info.activityInfo.packageName);
	    	    	}
	    	    }
	    	  }
	    	  Map<String, Object> m = new HashMap<String, Object>();
		      m.put(ATTRIBUTE_NAME_TEXT, "���������");
		      //m.put(ATTRIBUTE_NAME_IMAGE, R.drawable.share_icon);
		      data.add(m);
		      sortedPackages.addAll(removeDuplicates(packages));
		     
	    	  return  (ArrayList<Map<String, Object>>) removeDuplicates(data);
		  }
		  
		  public static List removeDuplicates(List list)  
		  {  
		    Set set = new HashSet();  
		    List newList = new ArrayList();  
		    for (Iterator iter = list.iterator(); iter.hasNext(); ) {  
		      Object element = iter.next();  
		      if (set.add(element))  
		        newList.add(element);  
		    }  
		    list.clear();  
		    list.addAll(newList);
		    return list;
		  }  
		  
		  class ShareSimpleAdapter extends SimpleAdapter {
			  private ArrayList<String> packages= new ArrayList<String>();
			  public PhotoObject photo;
			  private ArrayList<PhotoObject> photoArray = new ArrayList<PhotoObject>();
			  Context mContext;

			public ShareSimpleAdapter(Context context,
					List<? extends Map<String, ?>> data, int resource,
					String[] from, int[] to, ArrayList<String> arrPackages, ArrayList<PhotoObject> photoArr) {
				super(context, data, resource, from, to);
				mContext = context;
				packages.addAll(arrPackages);
				photoArray.addAll(photoArr);
			}
			
			

			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);
				final int last_item_number = getCount()-1;
				view.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						dismiss();
						if (position == last_item_number){
							if (photoArray.size()==1){
								photo = photoArray.get(0);
							startDefaultShareDialog(photo);
							}
							else{
								startDefaultShareDialogMultiple();
							}
						}
						else{
							shareOnInstanse(packages.get(position), photoArray);
						}
						
						
						
					}
				});
				return view;
			} 
		  }
		  
		  private void shareOnInstanse(String packageName, ArrayList<PhotoObject> photoArr){
			  if (packageName.toLowerCase().contains("facebook")){
				  if (photoArr.size()==1){
					  PhotoObject photo = photoArr.get(0);
					  shareOnFacebook(mContext,photo, packageName);
				  }
				  else{
					  shareOnFacebookMultiple(mContext, photoArr, packageName);
				  }
			  }
			  else if (packageName.toLowerCase().contains("twitter")){
				  if (photoArr.size()==1){
					  PhotoObject photo = photoArr.get(0);
					  shareOnTwitter(mContext,photo, packageName);
				  }
				  else{
					  shareOnTwitterMultiple(mContext, photoArr, packageName);
				  }
				  
			  }
			  else if (packageName.toLowerCase().contains("mms")){
				  if (photoArr.size()==1){
					  PhotoObject photo = photoArr.get(0);
					  shareOnMms(mContext,photo, packageName);
				  }
				  else{
					  shareOnMmsMultiple(mContext, photoArr, packageName);
				  }
				  
			  }
			  else if (packageName.toLowerCase().contains("instagram")){
				  if (photoArr.size()==1){
					  PhotoObject photo = photoArr.get(0);
					  shareOnInstagram(mContext,photo, packageName);
				  }
				  else{
					  shareOnInstagramMultiple(mContext, photoArr, packageName);
				  }
				  
			  }	
			  else{
				  if (photoArr.size() == 1){
			    		mPhoto = photoArr.get(0);
			    	startDefaultShareDialog(mPhoto);
			    	}
			    	else{
			    		startDefaultShareDialogMultiple();
			    	}
			  }
				
			 }
		  
		  public void shareOnFacebook(Context context,PhotoObject photo, String packageName){
			  Intent share = new Intent(Intent.ACTION_SEND);
			    Uri uri = Uri.parse("file://" + photo.getPath());
				share.putExtra(Intent.EXTRA_STREAM, uri );
				share.setType("image/jpeg");
				share.setPackage(packageName);
				startActivity(share);
		  }
		  public void shareOnFacebookMultiple( Context context, ArrayList<PhotoObject> arr, String packageName){
			  ArrayList<Uri> files = new ArrayList<Uri>();
			  for (PhotoObject t : arr) {
						files.add(Uri.parse("file://" + t.getPath()));
				}
			  Intent share = new Intent(Intent.ACTION_SEND_MULTIPLE);
			  share.setType("image/jpeg");
			  share.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files );
			  share.setPackage(packageName);
			  startActivity(share);
			  
		  }
		  
		  public void shareOnTwitter(Context context, PhotoObject photo, String packageName){
			  Intent share = new Intent(Intent.ACTION_SEND);
			    share.setType("*/*");
				    		Uri uri = Uri.parse("file://" + photo.getPath());
				    		share.putExtra(Intent.EXTRA_STREAM, uri );
				    		share.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.my_random_selfie_taken_via_shmelfie));
				    		share.setType("image/jpeg");
				    		share.setPackage(packageName);
				  startActivity(share);
		  }
		  
		  public void shareOnTwitterMultiple( Context context, ArrayList<PhotoObject> arr, String packageName){
			  Toast.makeText(context, "Can't send multiple photo on Twitter", Toast.LENGTH_LONG).show();
			  
		  }
		  
		  public void shareOnInstagram(Context context, PhotoObject photo, String packageName){
			  Intent share = new Intent(Intent.ACTION_SEND);
				    		Uri uri = Uri.parse("file://" + photo.getPath());
				    		share.putExtra(Intent.EXTRA_STREAM, uri );
				    		share.setType("image/jpeg");
				    		share.setPackage(packageName);
				  startActivity(share);
		  }
		  
		  public void shareOnInstagramMultiple( Context context, ArrayList<PhotoObject> arr, String packageName){
			  Toast.makeText(context, "Can't send multiple photo on Instagram", Toast.LENGTH_LONG).show();
		  }
		  
		  public void shareOnMms( Context context, PhotoObject photo, String packageName){
			  try{
			  Uri uri = Uri.parse("file:///" + photo.getPath());
			  Intent i = new Intent(Intent.ACTION_SEND);
			  //i.putExtra("sms_body",context.getString(R.string.my_random_selfie_taken_via_shmelfie));
			  //i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
			  i.setType("image/*");
			  i.putExtra(Intent.EXTRA_STREAM,uri);
			  i.setPackage(packageName);
			  startActivity(i);
			  }
			  catch(ActivityNotFoundException e){
				  e.printStackTrace();
				  try{
				  Uri uri = Uri.parse("mmsto:");
				  Intent i = new Intent(Intent.ACTION_SENDTO, uri);
				  i.setType("image/*");
				  //i.putExtra("sms_body",context.getString(R.string.my_random_selfie_taken_via_shmelfie));
				  //i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
				  Uri uri_photo = Uri.parse("file://" + photo.getPath());
				  i.putExtra(Intent.EXTRA_STREAM,uri_photo);
				  i.setPackage(packageName);
				  startActivity(i);				 
				  }
				  catch(ActivityNotFoundException e1){
					  e1.printStackTrace();
					  Uri uri = Uri.parse("file://" + photo.getPath());
					  Intent i = new Intent(Intent.ACTION_VIEW);
					  i.setType("vnd.android-dir/mms-sms");
					  //i.putExtra("sms_body",context.getString(R.string.my_random_selfie_taken_via_shmelfie));
					  //i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
					  i.putExtra(Intent.EXTRA_STREAM,uri);
					  i.setPackage(packageName);
					  startActivity(i);
				  }
			  }
          }
		  
		  public void shareOnMmsMultiple( Context context, ArrayList<PhotoObject> arr, String packageName){
			  ArrayList<Uri> files = new ArrayList<Uri>();
			  for (PhotoObject t : arr) {
						files.add(Uri.parse("file://" + t.getPath()));
				}
			  Intent i = new Intent(Intent.ACTION_SEND_MULTIPLE);
			  //i.putExtra("sms_body", context.getString(R.string.my_random_selfie_taken_via_shmelfie));
			  //i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
			  //i.putParcelableArrayListExtra(Intent.EXTRA_STREAM,files);
			  i.setType("image/jpeg");
			  i.setPackage(packageName);
			  startActivity(i);
          }
		 

	

}
