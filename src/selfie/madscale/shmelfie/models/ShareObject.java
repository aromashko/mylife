package selfie.madscale.shmelfie.models;

/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 18.04.13
 */
public class ShareObject {
   private String text;
   private String packages;
   private int image;
   
   
   
public ShareObject(String text, String packages, int image) {
	super();
	this.text = text;
	this.packages = packages;
	this.image = image;
}
public String getText() {
	return text;
}
public void setText(String text) {
	this.text = text;
}
public String getPackages() {
	return packages;
}
public void setPackages(String packages) {
	this.packages = packages;
}
public int getImage() {
	return image;
}
public void setImage(int image) {
	this.image = image;
}
   
   
}
