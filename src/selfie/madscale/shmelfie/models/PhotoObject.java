package selfie.madscale.shmelfie.models;


import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;


import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import selfie.madscale.shmelfie.utils.Supporting;

/**
 * Sloboda Studio 2013 User: oZBo Date: 05.04.13
 */

@Root(name = "Photo")
public class PhotoObject implements Serializable, Comparable<PhotoObject>{

	private String dateInMillis;
	private String path;
	private String name;
	private int dayInMonth;
	private int monthInYear;
	private int year;
	private int id;
	private boolean state;
	private int group; 
	Date photoDate;
	
	public static int GROUP_MAIN = 0;
	public static int GROUP_FAVORITE = 1;
	
	public static final String LOG_TAG = "PhotoObject";

	public PhotoObject(String name, String path, String date) {
		this.name = name;
		this.path = path;
		this.dateInMillis = date;
		long timeInMillis = Long.parseLong(getDateInMillis());
		String time = Supporting.parseDateFromLong("d", timeInMillis);
		dayInMonth = Integer.parseInt(time);
		time = Supporting.parseDateFromLong("M", timeInMillis);
		monthInYear = Integer.parseInt(time);
		time = Supporting.parseDateFromLong("y", timeInMillis);
		year = Integer.parseInt(time);
	}

	public PhotoObject() {
	}
	
	

	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	@Attribute(name = "Year")
	public int getYear() {
		return year;
	}

	@Attribute(name = "Year")
	public void setYear(int year) {
		this.year = year;
	}

	@Attribute(name = "MonthInYear")
	public void setMonthInYear(int monthInYear) {
		this.monthInYear = monthInYear;
	}

	@Attribute(name = "DayInMonth")
	public void setDayInMonth(int dayInMonth) {
		this.dayInMonth = dayInMonth;
	}

	@Attribute(name = "MonthInYear")
	public int getMonthInYear() {
		return monthInYear;
	}

	@Attribute(name = "DayInMonth")
	public int getDayInMonth() {
		return dayInMonth;
	}

	@Attribute(name = "ID")
	public int getId() {
		return id;
	}

	@Attribute(name = "ID")
	public void setId(int id) {
		this.id = id;
	}

	@Attribute(name = "Name")
	public String getName() {
		return name;
	}

	@Attribute(name = "Name")
	public void setName(String name) {
		this.name = name;
	}

	@Attribute(name = "Path")
	public String getPath() {
		return path;
	}

	@Attribute(name = "Path")
	public void setPath(String path) {
		this.path = path;
	}

	@Attribute(name = "DateInMillis")
	public String getDateInMillis() {
		return dateInMillis;
	}

	@Attribute(name = "DateInMillis")
	public void setDateInMillis(String dateInMillis) {
		this.dateInMillis = dateInMillis;
	}

	@Attribute(name = "Group")
	public int getGroup() {
		return group;
	}

	@Attribute(name = "Group")
	public void setGroup(int group) {
		this.group = group;
	}

	@Override
	public int compareTo(PhotoObject another) {
		if (getDateInMillis() == null || another.getDateInMillis() == null){
			return 0;
		}
		return getDateInMillis().compareTo(another.getDateInMillis());
	}
	

	/*@Override
	public int compareTo(PhotoObject another) {
		if (getDateTime() == null || another.getDateTime() == null){
			return 0;
		}
		return getDateTime().compareTo(another.getDateTime());
	}*/

}
