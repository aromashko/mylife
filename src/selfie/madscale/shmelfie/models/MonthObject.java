package selfie.madscale.shmelfie.models;

/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 18.04.13
 */
public class MonthObject {
    private int monthInYear;

    public MonthObject(int monthInYear) {
        this.monthInYear = monthInYear;
    }

    public int getMonthInYear() {
        return monthInYear;
    }

    public void setMonthInYear(int monthInYear) {
        this.monthInYear = monthInYear;
    }
}
