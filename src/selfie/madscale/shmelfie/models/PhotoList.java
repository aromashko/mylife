package selfie.madscale.shmelfie.models;

import java.util.ArrayList;

import org.simpleframework.xml.ElementList;

public class PhotoList{
	@ElementList(inline = true, name = "photos")
	public ArrayList<PhotoObject> list;
}
