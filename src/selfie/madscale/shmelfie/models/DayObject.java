package selfie.madscale.shmelfie.models;

/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 18.04.13
 */
public class DayObject {
    private int dayInMonth;

    public DayObject(int dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    public int getDayInMonth() {
        return dayInMonth;
    }

    public void setDayInMonth(int dayInMonth) {
        this.dayInMonth = dayInMonth;
    }
}
