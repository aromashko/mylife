package selfie.madscale.shmelfie.service;

/**
 * Sloboda Studio 2013
 * User: oZBo
 * Date: 19.03.13
 */

import android.app.Service;
import android.content.*;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;



import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import com.bugsense.trace.BugSenseHandler;

import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.database.DataManager;
import selfie.madscale.shmelfie.utils.DBBackup;
import selfie.madscale.shmelfie.utils.RandomInteger;
import selfie.madscale.shmelfie.utils.Supporting;


public class CameraService extends Service implements SensorEventListener {

    private static final String LOG_SERVICE = "logCamseraService";
    //private static Timer timerForMakeShot = new Timer();
    private static Timer timerScreenUnlock = new Timer();
    //private static TimerTask taskForAutoShot;
    private static TimerTask taskScreenUnlock;
    public static Handler handlerForMakePhoto;
    private BroadcastReceiver screenReciever;
    private SensorManager mSensorManager;
    private Sensor mLight;

    private static boolean canMakePhotoAfterUnlock = true;
    public static int whenMakePhoto=0;
    private final long periodToWaitAfterShot = 7000;
    public static long periodForAutoShot = 0;
    private static float lightLevel = 74;
    private static float lightSensorValue;
    private static int photoCounter = 0;
    public static int MININUM_PERIOD_TO_WAIT_AFTER_SHOT= 7* VAR.MILLIS_IN_SEC;
    public static int MAXIMUM_PERIOD_TO_WAIT_AFTER_SHOT= 120* VAR.SECONDS_IN_MIN* VAR.MILLIS_IN_SEC;
    public static SharedPreferences settings;

    @Override
    public void onCreate() {
    	BugSenseHandler.initAndStartSession(CameraService.this, "d9f53567");
      
        settings = getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE);
        VAR.whenMakePhoto = settings.getInt(VAR.WHEN_MAKE_PHOTO, VAR.WHEN_MAKE_PHOTO_DEFAULT_VALUE);
        recieverRegister();
        
        if (settings.getInt(VAR.CHECK_BOX_LIGHTSENSOR, 0) == 1){
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mSensorManager.registerListener(this, mLight,
                SensorManager.SENSOR_DELAY_NORMAL);
        }
        
        initHandlers();
        startTimers();
        
        
        Log.e(LOG_SERVICE, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(LOG_SERVICE, "onStartCommand");
        Log.e("Start Id", startId+"");
        return Service.START_STICKY;
    }

    /*
     * @Override public void onStart(Intent intent, int startId) {
	 * super.onStart(intent, startId); mSensorManager.registerListener(this,
	 * mLight, SensorManager.SENSOR_DELAY_NORMAL); initHandlers();
	 * startTimers(); }
	 */

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void makeBackup(Context c) {
        DBBackup.makeBackup(c, DataManager.getPhotoList(c),
                DataManager.getBestPhotoList(c));
        Log.d("Mylife_BACKUP", "Backup maked");
    }


    @Override
    public void onDestroy() {
    	BugSenseHandler.closeSession(CameraService.this);
        Log.d(LOG_SERVICE, "onDestroy");
        if (photoCounter > 0)
            makeBackup(getApplicationContext());

        super.onDestroy();
        if (mSensorManager != null){
        mSensorManager.unregisterListener(this);}
        unregisterReceiver(screenReciever);
        stopTimers();
    }

    private void recieverRegister() {
        try {
            IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_USER_PRESENT);
            screenReciever = new ScreenReceiver();
            registerReceiver(screenReciever, filter);
        } catch (Exception e) {
        	e.printStackTrace();
            BugSenseHandler.sendException(e);
            Toast.makeText(getApplicationContext(), "Some error while recieving screen state", Toast.LENGTH_LONG).show();
            Log.e(LOG_SERVICE, "some error while recieving screen state");
        }
    }


    private void initHandlers() {
        handlerForMakePhoto = new android.os.Handler() {
            @Override
            public void handleMessage(Message msg) {
            	Log.e(LOG_SERVICE, "message received");
                if (isInterval()) {
                    if (canMakePhotoAfterUnlock) {
                    	Log.e(LOG_SERVICE, "message received and can make photo");
                        if (((getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE).getInt(VAR.CHECK_BOX_LIGHTSENSOR, 0) == 1)
                                &&lightSensorValue > lightLevel)
                                ||!(getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE).getInt(VAR.CHECK_BOX_LIGHTSENSOR, 0) == 1)){
                        	
                            Intent intent = new Intent(
                                    getApplicationContext(), TakePhotoService.class);
                            startService(intent);
                            Log.e(LOG_SERVICE, "Take photo service intent");
                        }

                        if (photoCounter % 10 == 0) {
                            makeBackup(getApplicationContext());
                        }
                        photoCounter++;
                        // LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new
                        // Intent(MainActivity.class.getName()));
                    }
                }
            }
        };
    }

    public void startTimers() {
    	VAR.whenMakePhoto = settings.getInt(VAR.WHEN_MAKE_PHOTO, VAR.WHEN_MAKE_PHOTO_DEFAULT_VALUE);
        Supporting.frequencyForMakePhoto(VAR.TEMP_autoPerDay);

        /*taskForAutoShot = new TimerTask() {
            @Override
            public void run() {
                if (VAR.whenMakePhoto == 1)
                            handlerForMakePhoto.sendEmptyMessage(0);
            }
        };*/
        //
        Log.e("When make photo=",VAR.whenMakePhoto+"");
        taskScreenUnlock = new TimerTask() {
            @Override
            public void run() {
                canMakePhotoAfterUnlock = true;
                		Log.e(LOG_SERVICE, "screeen unlock timerTask");

            }
        };
        if (VAR.whenMakePhoto==2){
        	int random_time_after_unlock = RandomInteger.generetaRandom(MININUM_PERIOD_TO_WAIT_AFTER_SHOT, 
        			MAXIMUM_PERIOD_TO_WAIT_AFTER_SHOT);
        	Log.e("Random time", random_time_after_unlock/60000 + "m");
        	//Toast.makeText(getBaseContext(), random_time_after_unlock/60000 + "m", Toast.LENGTH_LONG).show();
        	timerScreenUnlock.schedule(taskScreenUnlock, random_time_after_unlock);
        }
        else{
        	Log.e("Time unlock", MININUM_PERIOD_TO_WAIT_AFTER_SHOT/60000 + "m");
        	timerScreenUnlock.schedule(taskScreenUnlock, MININUM_PERIOD_TO_WAIT_AFTER_SHOT);
        }
        //timerForMakeShot.schedule(taskForAutoShot, 0, periodForAutoShot);
    }

    private void stopTimers() {
        taskScreenUnlock.cancel();
        //taskForAutoShot.cancel();
    }
    
    public static void reStartTimers(){
    	canMakePhotoAfterUnlock = false;
    	VAR.whenMakePhoto = settings.getInt(VAR.WHEN_MAKE_PHOTO, VAR.WHEN_MAKE_PHOTO_DEFAULT_VALUE);
    	taskScreenUnlock.cancel();
    	timerScreenUnlock.cancel();
		timerScreenUnlock = new Timer();
    	taskScreenUnlock = new TimerTask() {
            @Override
            public void run() {
                canMakePhotoAfterUnlock = true;
                		Log.e(LOG_SERVICE, "screeen unlock timerTask");

            }
        };
        Log.e("When make photo=",VAR.whenMakePhoto+"");
    	if (VAR.whenMakePhoto==2){
        	int random_time_after_unlock = RandomInteger.generetaRandom(MININUM_PERIOD_TO_WAIT_AFTER_SHOT, 
        			MAXIMUM_PERIOD_TO_WAIT_AFTER_SHOT);
        	Log.e("Random time restarted", random_time_after_unlock/60000 + "m");
        	//Toast.makeText(getBaseContext(), random_time_after_unlock/60000 + "m", Toast.LENGTH_LONG).show();
        	timerScreenUnlock.schedule(taskScreenUnlock, random_time_after_unlock);
    	}
    	 else{
         	Log.e("Time unlock", MININUM_PERIOD_TO_WAIT_AFTER_SHOT/60000 + "m");
         	timerScreenUnlock.schedule(taskScreenUnlock, MININUM_PERIOD_TO_WAIT_AFTER_SHOT);
         }
    }

    private boolean isInterval() {
        boolean intervalSate = true;
        Calendar calendar = Calendar.getInstance();
        Integer currentTime = calendar.get(Calendar.HOUR_OF_DAY) * 60
                + calendar.get(Calendar.MINUTE);
        SharedPreferences settings = getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE);
        int fromTime = (int) settings.getInt(VAR.FROM_TIME, VAR.FROM_TIME_DEFAULT_VALUE);
        int toTime = (int) settings.getInt(VAR.TO_TIME, VAR.TO_TIME_DEFAULT_VALUE);
        if (settings.getInt(VAR.CHECK_BOX_INTERVAL, 1)==1) {
            if (fromTime < toTime) {
                intervalSate = (fromTime < currentTime)
                        && (toTime > currentTime);
            } else {
                intervalSate = (fromTime < currentTime)
                        && ((toTime + VAR.MINUTES_IN_DAY) > currentTime);
            }
        }
        return intervalSate;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_LIGHT) {
            //Log.i(LOG_SERVICE, "onSensor Change :" + sensorEvent.values[0]);
            lightSensorValue = sensorEvent.values[0];
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (sensor.getType() == Sensor.TYPE_LIGHT) {
            Log.i(LOG_SERVICE, "Accuracy :" + accuracy);
        }
    }
}