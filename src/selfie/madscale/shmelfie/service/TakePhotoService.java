package selfie.madscale.shmelfie.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.bugsense.trace.BugSenseHandler;

import selfie.madscale.shmelfie.R;
import selfie.madscale.shmelfie.VAR;
import selfie.madscale.shmelfie.database.DataManager;
import selfie.madscale.shmelfie.models.PhotoObject;
import selfie.madscale.shmelfie.utils.FileUtil;
import selfie.madscale.shmelfie.utils.Supporting;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;



/**
 * Created by alex on 24.01.14.
 */
public class TakePhotoService extends Service {

    private WindowManager mWindowManager;
    private View mRoot;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private Camera mCamera;
    public static final String LOG_TAG = "takePhotoService";

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(LOG_TAG, "on create");
        BugSenseHandler.initAndStartSession(TakePhotoService.this, "d9f53567");
        
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        LayoutInflater inflater = LayoutInflater.from(this);
        mRoot = inflater.inflate(R.layout.camera, null);
        mSurfaceView = (SurfaceView) mRoot.findViewById(R.id.surfaceView);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (mCamera != null) {
                    
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                try {
                	
                    final SharedPreferences settings = getSharedPreferences(VAR.SETTINGS,
                            MODE_PRIVATE);
                    final int rotation = VAR.angleCamera[settings.getInt(VAR.ANGLE_CAMERA, VAR.ANGLE_DEFAULT_VALUE)];
                    final SoundPool soundPool = new SoundPool(1,
                            AudioManager.STREAM_NOTIFICATION, 0);
                    final int shutterSound = soundPool.load(getApplicationContext(),
                            R.raw.camera_click, 0);

                    Camera.PictureCallback mCall = new Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(final byte[] data, Camera camera) {
                        	
                        	new SavePhotoTask().execute(data);
                        	camera.startPreview();
                        	

                            Thread thr = new Thread() {
                                @Override
                                public void run() {
                                    FileOutputStream out = null;
                                    try {
                                        String photoPath = makePhotoPath();
                                        out = new FileOutputStream(new File(photoPath));
                                        if (rotation != 0) {
                                            Bitmap img = FileUtil.readBitmapFromByteArray(data, 1);
                                            img = FileUtil.rotateBitmap(img, rotation);
                                            img.compress(Bitmap.CompressFormat.JPEG, 100, out);
                                            FileUtil.recycle(img);
                                            img = null;
                                        } else {
                                            out.write(data);
                                        }
                                        out.close();
                                        savePhotoInDBAndUpdateAdapter(photoPath);
                                    } catch (FileNotFoundException e) {
                                    	 stopSelf();
                                        e.printStackTrace();
                                        BugSenseHandler.sendException(e);
                                    } catch (IOException e) {
                                    	 stopSelf();
                                        e.printStackTrace();
                                        BugSenseHandler.sendException(e);
                                    } finally {
                                        if (out != null) {
                                            try {
                                                out.close();
                                            } catch (IOException e) {
                                            	 stopSelf();
                                                e.printStackTrace();
                                            }
                                        }
                                        stopSelf();
                                    }
                                }
                            };
                            thr.start();
                            mute(false);
                            if (settings.getBoolean(VAR.SHUTTER_CLICK, false)) {
                                soundPool.play(shutterSound, 1f, 1f, 0, 0, 1);
                            }
                        }
                    };
                    mute(true);
                    
                    initPreview(width, height);
    				startPreview();
                    mCamera.takePicture(null, null, mCall);
                    
                } catch (Exception e) {
                	 stopSelf();
                    e.printStackTrace();
                    BugSenseHandler.sendException(e);
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mCamera != null) {
                    releaseCamera();
                }
            }
        });
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams(
                1, // Width
                1, // Height
                WindowManager.LayoutParams.TYPE_PHONE, // Type
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, // Flag
                PixelFormat.TRANSLUCENT // Format
        );

        parameters.x = 0;
        parameters.y = 0;
        parameters.gravity = Gravity.TOP | Gravity.RIGHT;

        mWindowManager.addView(mRoot, parameters);
        
    }
    
    
    
    private void mute(boolean mute) {
        AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mgr.setStreamMute(AudioManager.STREAM_SYSTEM, mute);
    }

    private void savePhotoInDBAndUpdateAdapter(String photoPathString) {
        PhotoObject tempObject = new PhotoObject(makePhotoName(),
                photoPathString, Supporting.getTimeInMillis());
        DataManager.insertPhoto(getApplicationContext(), tempObject);
        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(new Intent(VAR.ACTION_PHOTO_INSETRED_IN_DB));
    }

    private String makePhotoPath() {
        StringBuilder photoPath = new StringBuilder();
        photoPath.append(VAR.PATH);
        photoPath.append(VAR.PREFICS);
        photoPath.append(Supporting.getCurrentDate(VAR.dateForamt));
        photoPath.append(VAR.IMAGE_FORMAT);
        return String.valueOf(photoPath);
    }

    private String makePhotoName() {
        StringBuilder photoName = new StringBuilder();
        photoName.append(VAR.PREFICS);
        photoName.append(Supporting.getCurrentDate(VAR.dateForamt));
        return String.valueOf(photoName);
    }

    public void releaseCamera() {
        try {
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.release();
            }
        } catch (Exception e) {
        	 stopSelf();
            Log.e("TakePhoto", "error stop camera", e);
        }

        mCamera = null;
    }

    private void prepareViewAndTakePhoto() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                Camera.CameraInfo info = new Camera.CameraInfo();
                for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                    Camera.getCameraInfo(i, info);
                    if (Camera.getNumberOfCameras() >= 2) {
                        switch (getSharedPreferences(VAR.SETTINGS, MODE_PRIVATE).getInt(VAR.WHICH_CAMERA, Supporting.getFrontCamera() ? VAR.CAMERA_FRONT : VAR.CAMERA_BACK)) {
                            case VAR.CAMERA_BACK:
                                if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                                    mCamera = Camera.open(i);
                                break;
                            case VAR.CAMERA_FRONT:
                                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
                                    mCamera = Camera.open(i);
                                break;
                        }
                    } else {
                        if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                            mCamera = Camera.open(i);
                        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
                            mCamera = Camera.open(i);
                    }
                }
                
            } else {
                stopSelf();
            }
        } catch (RuntimeException e) {
        	 stopSelf();
        	BugSenseHandler.sendException(e);
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Camera error", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flag, int startId) {
    	 Log.e(LOG_TAG, "onStartCommand");
    	prepareViewAndTakePhoto();
     	startPreview();

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        
        Log.e(LOG_TAG, "OnDestroy");
        CameraService.reStartTimers();
        releaseCamera();
        mCamera = null;
        if (mRoot != null){
            mWindowManager.removeView(mRoot);}
        BugSenseHandler.closeSession(getBaseContext());
    }
    
    class SavePhotoTask extends AsyncTask<byte[], String, String> {
		@Override
		protected String doInBackground(byte[]... jpeg) {
			File photo = new File(Environment.getExternalStorageDirectory(),
					"photo.jpg");

			if (photo.exists()) {
				photo.delete();
			}

			try {
				FileOutputStream fos = new FileOutputStream(photo.getPath());

				fos.write(jpeg[0]);
				fos.close();
			} catch (java.io.IOException e) {
				 stopSelf();
				Log.e("PictureDemo", "Exception in photoCallback", e);
			}
			//stopSelf();
			return (null);
		}
	}
    
    private void initPreview(int width, int height) {
		if (mCamera != null && mSurfaceHolder.getSurface() != null) {
			try {
				mCamera.setPreviewDisplay(mSurfaceHolder);
			} catch (Throwable t) {
				Log.e("PreviewDemo-surfaceCallback",
						"Exception in setPreviewDisplay()", t);
				Toast.makeText(TakePhotoService.this, t.getMessage(),
						Toast.LENGTH_LONG).show();
			}

			Camera.Parameters parameters = mCamera.getParameters();
			Camera.Size size = getBestPreviewSize(width, height, parameters);
			Camera.Size pictureSize = getSmallestPictureSize(parameters);

			if (size != null && pictureSize != null) {
				//parameters.setPreviewSize(size.width, size.height);
				parameters
						.setPictureSize(pictureSize.width, pictureSize.height);
				parameters.setPictureFormat(ImageFormat.JPEG);
				mCamera.setParameters(parameters);
			}

		}
	}

	private void startPreview() {
		if (mCamera != null) {
			mCamera.startPreview();
		}
	}

	private Camera.Size getBestPreviewSize(int width, int height,
			Camera.Parameters parameters) {
		Camera.Size result = null;

		for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
			if (size.width <= width && size.height <= height) {
				if (result == null) {
					result = size;
				} else {
					int resultArea = result.width * result.height;
					int newArea = size.width * size.height;

					if (newArea > resultArea) {
						result = size;
					}
				}
			} else
				result = size;
		}

		return (result);
	}

	private Camera.Size getSmallestPictureSize(Camera.Parameters parameters) {
		Camera.Size result = null;

		for (Camera.Size size : parameters.getSupportedPictureSizes()) {
			if (result == null) {
				result = size;
			} else {
				int resultArea = result.width * result.height;
				int newArea = size.width * size.height;

				if (newArea < resultArea) {
					result = size;
				}
			}
		}

		return (result);
	}

}
