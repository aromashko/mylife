package selfie.madscale.shmelfie.service;

import selfie.madscale.shmelfie.VAR;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;




public class ScreenReceiver extends BroadcastReceiver {

	public static final String LOG_TAG = "reciever";
	

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			//whenMakePhoto = settings.getInt(VAR.WHEN_MAKE_PHOTO, VAR.WHEN_MAKE_PHOTO_DEFAULT_VALUE);
			Log.e(LOG_TAG,"When make photo="+ VAR.whenMakePhoto);
            if (VAR.whenMakePhoto == 0 || VAR.whenMakePhoto == 2) {
                try {
                	Log.e(LOG_TAG, "send message");
                    CameraService.handlerForMakePhoto.sendEmptyMessage(0);
                } catch (RuntimeException e) {
                    //BugSenseHandler.sendException(e);
                }
            }
            Log.e(LOG_TAG, "screen on!");
        }
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			Log.e(LOG_TAG, "screen off!");
		}


        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {

            Log.d(LOG_TAG, "user action!");
        }
    }
}
